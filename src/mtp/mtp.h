/****************************************************
 * File: mtp.h
 *
 * Author: Fred Engler
 *
 * Description: Contains the data stuctures for storing
 * Minimal Tiling Path pairs and picked clones.
 ****************************************************/

#ifndef INC_MTP_H
#define INC_MTP_H

/*#define MTP_DEBUG        // general mtp algorithm traces */
/*#define DIJK_DEBUG  // Dijkstra algorithm printouts */


#include "fpp.h"
#include <gtk/gtk.h>
#include <glib.h>
#include "bss.h"
#include "glib_port.h"

#define FP_OFFSET 50000      /* Weight to add to fingerprint-based pairs as opposed to
                              * BSS-draft based ones.*/
#define FSCANLINE(fp, x) fscanf(fp,"%1023[^\n]\n",x)
#define GETCURDIR (strcmp(dirName,"") ? dirName : ".")

/* Bit masks for identifying the type of failure for a candidate pair.*/
#define MAX_BAND_MASK 1                   /* Too many bands shared*/
#define MIN_BAND_MASK 2                   /* Not enough bands shared*/
#define SPAN_FAILURE_MASK 4               /* Spanner's bands aren't accounted for enough*/
#define LFLANK_FAILURE_MASK 8             /* Left flanker didn't verify enough bands*/
#define RFLANK_FAILURE_MASK 16            /* Right flanker didn't verify enough bands*/
#define HAVE_LFLANK_MISMATCH_MASK 32      /* The left flanker works, but isn't perfect*/
#define HAVE_RFLANK_MISMATCH_MASK 64      /* The right flanker works, but isn't perfect*/

#define MAXCTGCLONES 10000             /* Maximum number of clones per contig*/
#define MAXCTGS 20000                   /*Maximum number of contigs*/
#define BIGPENALTY 500000


/* Sources for finding pairs*/
enum pairTypes {FINGERPRINT, BSS_DRAFT, MANDATORY, EXP, SNG};

/* Used for storing the band bin values for a pair*/
struct pair_bin{
  int span_L_share;    /* Bands shared by spanner and left clone*/
  int span_LR_share;   /* Bands shared by spanner and both clones*/
  int span_R_share;    /* Bands shared by spanner and right clone*/
  int flank_L_share;   /* Bands shared by left flanker and left clone*/
  int flank_R_share;   /* Bands shared by right flanker and right clone*/
  int span_mis;        /* Bands in spanner not accounted for*/
  int flank_L_mis;     /* Bands in left clone not accounted for*/
  int flank_R_mis;     /* Bands in right clone not accounted for*/
};

/* Holds the information for an overlapping clone pair meeting all criteria*/
struct pair{
  enum pairTypes pair_type;    /* Is this a fingerprint or bss-draft based pair?*/
  char c1[CLONE_SZ+1];         /* The left clone name of the pair*/
  char c2[CLONE_SZ+1];         /* The right clone name of the pair*/
  char draft_clone[CLONE_SZ+1];         /* bridging draft clone, if any */
  char cspan[CLONE_SZ+1];      /* The clone name of the spanner*/
  char clflank[CLONE_SZ+1];    /* The clone name of the left flanker*/
  char crflank[CLONE_SZ+1];    /* The clone name of the right flanker*/
  int x1, y1, x2, y2;          /* The coordinates in CB units of the pair*/
  int index1;                  /* acedata index of left clone*/
  int index2;                  /* acedata index of right clone*/
  int indexspan;               /* acedata index of spanner*/
  int indexlflank;             /* acedata index of left flanker*/
  int indexrflank;             /* acedata index of right flanker*/
  int olap;                    /* Overlap in base pairs of pair
                                * (estimated on shared bands for FP based pairs)*/
  int c1size;                  /* Size of left clone in base pairs*/
  int c1size_from_file;        /* True if c1size retrieved from Sizes directory*/
  int c2size;                  /* Size of right clone in base pairs*/
  int c2size_from_file;        /* True if c2size retrieved from Sizes directory*/

  int weight_score;
  /* Only for fingerprint pairs*/
  struct pair_bin bin_values;  /* How bands are shared*/
};

/* For dynamically allocating an expandable 'pair' array for up to
 * max_contig contigs.*/
struct pairlist{
  struct pair *p;   /* Expandable array -- size based on number of pairs for contig*/
};

/* Holds the information for an overlapping clone pair that was picked to be
 * in the Minimal Tiling Path*/
struct pick{
  struct pair *picked; /* Pointer to the picked pair*/
  int exp_pos1;        /* Position in expressway of left clone*/
  int exp_pos2;        /* Posiiton in expressway of right clone*/
  int exp_cnt;         /* Number of clones in the expressway*/
  int ljunction;       /* The left clone doesn't connect with another clone*/
  int rjunction;       /* The right clone doesn't connect with another clone*/
};

/* For dynamically allocating an expandable 'pick' array for up to
 * max_contig contigs.*/
struct picklist{
  struct pick *p;   /* Expandable array -- size based on number of MTP clones for contig*/
};

/* Used for storing all spanner candidates for a pair so they can be
 * sorted based on distance from center of pair overlap point. */
struct spanner{
  struct contig *cptrspan;   /* Pointer to the spanner in the 'root' structure*/
  int dist;                  /*Distance from pair midpoint*/
};

extern void find_pairs(gboolean use_fp, gboolean use_bss,
                       gint min_olap, gint max_olap,
                       guint min_shared, guint min_unique);
extern void find_mtp(gint shared_weight, gint unacc_weight,
                     gint spanmis_weight);
extern void output_mtp(void);
extern guint num_shared_bands(int index1, int index2);
extern void add_to_pairs(int ctg, enum pairTypes pair_type,
                         char *c1, char *c2, char* draft_clone,
                         char *cspan, char *clflank, char *crflank,
                         int x1, int y1, int x2, int y2,
                         int index1, int index2, int indexspan,
                         int indexlflank, int indexrflank,
                         int olap, int weight_score, int c1size, int c2size,
                         int c1size_from_file, int c2size_from_file,
                         struct pair_bin *bin_values);
extern void mtp_update_fp_class_pars(void);
extern void mtp_load_configuration(const gchar *config);
extern void mtp_save_configuration(FILE *stream);
extern gboolean mtp_is_mandatory(CLONE *clone);
extern gboolean mtp_is_prohibited(CLONE *clone);
extern void ZevaluateClone(void);

extern int minOlap;     /* minimum overlap of two potential MTPs */
extern int maxOlap;    /* maximum overlap between two potential MTPs */



typedef GArray* HitList;
HitList hits_seqctg_ctg(BSS_INST* pbsi,int ctg, int sctg);
HitList hits_bes(BSS_INST* pbsi, gchar* bes_name);

#endif /* INC_MTP_H */
