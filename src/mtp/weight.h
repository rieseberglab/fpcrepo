#ifndef WEIGHT_H_
#define WEIGHT_H_

#include "mtp.h"
#include <glib.h>

struct EdgeWeightS
{
    gulong bss; /* not really used anymore - WN */
    gulong fp;
};

typedef struct EdgeWeightS EdgeWeight;

EdgeWeight* weight_new(void);
EdgeWeight* weight_max(void);
void weight_make_max(EdgeWeight* weight);
void weight_make_min(EdgeWeight* weight);
void weight_init(EdgeWeight* weight, glong olap, enum pairTypes pair_type,
                 gboolean prefer_large, guint sizeL, guint sizeR);
void weight_copy(EdgeWeight* dst, const EdgeWeight* src);
void weight_add(EdgeWeight* acc, const EdgeWeight* addend);
gint weight_compare(const EdgeWeight* weight1, const EdgeWeight* weight2);
void weight_destroy(EdgeWeight** weight);

#endif /* WEIGHT_H_ */
