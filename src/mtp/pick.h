/****************************************************
 * File: pick.h
 *
 * Author: Fred Engler
 *
 * Description: Contains the data stuctures for picking
 * a Minimal Tiling Path based on the shortest-path algorithm.
 ****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <glib.h>
#include "mtp.h"
#include "weight.h"

#define MAXCTGLEN 10000                /*Maximum number of cb units per contig*/
#define MAXEXPRESSWAYS MAXCTGCLONES/2  /*Maximum number of expressways per contig*/
#define MAXBASEOLAP 15000000             /*Maximum amount of base-pair overlap for clones in exp*/
#define MAXNEGBASEOLAP 300000             /*Maximum gap for reporting purposes*/
#define REPORTBIN 10000             /*Maximum amount of base-pair overlap for clones in exp*/
#define NONMANDATORY_PENALTY 10000000   /* Term added to edge weight if not mandatory */
#define FINGERPRINT_PENALTY 150000    /*This factor added to edge weight if fingerprint based*/
#define CBMAP_PENALTY 600000 /* Term added to edge weight if cb map based */

/* Weighting options for picking mtps from overlapping expressways*/
#define MAXWEIGHT_OLAP 500
#define MAXWEIGHT_SPAN 500
#define BESTOLAP 5
#define MINOLAP 5
#define MAXOLAP 100
#define BESTSPAN 500
#define P1 .4     /*Proportion of total weight given to olap*/
#define P2 .6     /*Proportion of total weight given to span*/

#define MAXEXPOLAP 20   /*Maximum percent of expressway overlap to be included in mtp*/

#define MINPAIRSIZE 200000 /*Minimum sum of clone pair sizes if Proj.avginsertsize not set*/
#define MAXPAIRSIZE 300000 /*Maximum sum of clone pair sizes if Proj.avginsertsize not set*/

struct edge{
   int num;
   long bssOlap;
   long overlap_score;
   int fpcOlap;
   enum pairTypes pair_type;
   int sizeL, sizeR;
   struct edge *next;
};

struct vertex{
   int count;
   int l;
   int r;
   struct edge *link;
};

struct subgraph{
   int l_clone,r_clone;         /*Left and Right end clones (most extreme CB unit values)*/
   int n;                       /*Number of clones in subgraph*/
   int clones[MAXCTGCLONES];    /*List of clone IDs in subgraph -- stores mapping
                                  from shortest path clone ID to sparse matrix clone ID*/
};

/* Results structs */
struct cloneinfo{
   char name[CLONE_SZ+1];
   int l,r;
};

struct expdata{
   char name[CLONE_SZ+1];
   int l, r;
   int n;         /*Number of clones*/
   struct cloneinfo *clones;
};

struct ctgdata{
   int ctg, start, end;
   //int n;         /*Number of expressways*/
   //struct expdata *expressways;
   GArray *expressways;
};

struct ctgdata *exp_results[MAXCTGS]; /*Stores expressways and their clones for each ctg*/

/* MTP structs */
struct pickedcloneinfo{
   int l,r;
   int index;           /* acedata index */
   struct expdata *exp; /*Expressway of clone*/
   int pos_in_exp;    /*E.g. clone 4... */
   int total_pos;     /*...out of 7 in expressway.*/
   int excess;        /*1 if clone not required for contiguity in mtp*/
   gboolean mandatory;
};

struct mtpdata{
   int *covered;    /*1 if cbunit covered by a mtp. 0 otherwise.*/
   int span;        /*Number of cbunits in this contig.*/
   int n;           /*Number of clones in mtp (sum of clones in expressways)*/
   struct pickedcloneinfo* picked_clones;
};

extern struct mtpdata *mtp[MAXCTGS];  /*Stores the clones in the MTP for each ctg*/

/* CAS =  uses too much memory
char clonekey[MAXCTGS][MAXCTGSCLONE][16]
*/
extern int MaxCtgClones;           /* calc in pick_mtp_clones */
extern int *clonekey[MAXCTGS];     /*Stores mapping from clone ID to clone index */
extern int num_clones[MAXCTGS];

extern int ctgkey[MAXCTGS][3];           /*Stores mapping from ctg ID to ctg number, and left and right end values*/
                                         /* Needed because the contig number sequence can have large gaps */
extern int num_ctgs;

extern struct vertex *SMctg[MAXCTGS];        /*The sparse matrices.  One for each ctg.*/

extern int exp_num;                    /*Used to uniquely label expressways.*/

extern int total_map_size;             /*Size in cb units of all ctgs*/

/* Stats structs*/
struct ctgstatdata{
   int ctg;
   int ctg_len;
   int gap_len;
   int num_gaps;
   int overlap;
   int percent_covered;
};

#define BANDLEN 5000
struct statdata{
   int total_gap;
   int total_exp_kb;
   int total_exp_cln;
   int exp_pos_cln_olap[MAXBASEOLAP/REPORTBIN + 1];  /*Positive base pair overlap of clones in expressways*/
   long total_pos_cln_olap;

   int exp_neg_cln_olap[MAXNEGBASEOLAP/REPORTBIN + 1];  /*Negative base pair overlap of clones in expressways*/
   long total_neg_cln_olap;

   int percent_covered;               /*Percent of map covered by expressways*/
   struct ctgstatdata *ctgstats[MAXCTGS];

   int num_fp_picked;                 /*Number of pairs picked from fingerprints*/
   int num_bss_picked;                /*Number of pairs picked from BSS results*/
    int num_mandatory_picked;
    int num_exp_jct;
   int cnt_mandatory;
};
struct statdata stats;

extern int get_size_penalty(int sizeL, int sizeR);
