#ifndef DRAFT_H
#define DRAFT_H
#include "bss.h"

#define BUFFER_SIZE 300  /*Buffer size                            */
#define LINE_SIZE 80   /*Width of output of alignment             */
#define MAX_SEQCTG 500 /*Maximum number of SeqCtgs in a sequence  */
#define MAX_NEIGH 500   /*Maximum number of neighbor SeqCtgs or 
                        *distant SeqCtgs of a given SeqCtg        */
#define MAX_SEQCLONE 100 /*Maximum number of clones in the sequence*/

#define ORDER

/*Left or right MTP clones   */
typedef enum {LEFT, RIGHT} direction;

/*Clone hit with x end in or y end in or both ends in 
 *the range between sequenced clone's two ends  */
typedef enum {XIN, YIN, XYIN, XUNDET, YUNDET} position;

/*Orientation of SeqCtg
 *RCN: right direction; RCY: reverse complemented*/
typedef enum {RCN, RCY, UNKNOWN} orientation;

/*BES Information*/
typedef struct {
  char name[50];         /*BES name */
  int seqctg_assigned;   /*SeqCtg assigned to this BES */
  GPtrArray *seqctg_list;/*List of SeqCtgs that the BES hits*/

  /*1:the BES confirms a neighbor; otherwise 0
   *hits confirming neighbors should be assigned to both SeqCtgs*/
  int in_neigh; 
} BESInfo;

/*Hit Information */ 
typedef struct {
  BSS_HIT* hit;  /*Pointer to hit info from bss file */
  float pct_hitlen; /*percentage of hit length*/
 
  CLONE* clp;/*BES hit's clone */ 
  int cb_olap; /*CB overlap between hit's clone and sequenced clone*/
  
  int end;  /*1: hit SeqCtg end; otherwise: 0 */
  position end_in; /*XIN:x end hit; YIN:y end hit; XYIN:both end hit*/
  int parent_hit; /*1: parent bes hit; otherwise 0 */
  int count;/*Number of hits that a single BES hits across SeqCtgs*/

  GPtrArray *neigh_hits; /*list of correponding neighbor hits */
  GPtrArray *dist_hits; /*list of corresponding distant SeqCtg hits*/

  position contained_in;/*x end hit or y end hit of contained clones;
                          X: x end hit; Y: y end hit; */
  position best_in; /*Record the end info of the best order */
  int bes_index; /*the index of the BES info */

  int checked; /*1: checked; 0: unchecked */
} HitClone;

typedef struct seq_ctg {
  int seqctg; /*Original SeqCtg number */
  int new_seqctg; /*Ordered SeqCtg number*/
  int size;   /* SeqCtg size */
  GArray *hits;/*ALL hit list*/

  /* Neighbor info*/
  int neigh_count;  /*Number of neighbors */
  struct seq_ctg *neigh[MAX_NEIGH];/*list of neighbors*/ 
  float neigh_score[MAX_NEIGH];  /*score of each neighbor */
  GPtrArray *neigh_hits[MAX_NEIGH]; /*list of hits confirming each neighbor*/ 
 
  /* Distant SeqCtg info */
  int dist_count;  /*Number of distant SeqCtgs */
  struct seq_ctg *distseqctg[MAX_NEIGH]; /*list of distant SeqCtgs */
  float dist_score[MAX_NEIGH];   /*score of each distant SeqCtg    */
  GPtrArray *dist_hits[MAX_NEIGH]; /*list of hits confirming each distant SeqCtg*/

  /*Score of parent bes hit if containing any; otherwise, 0*/ 
  float parent_score;

  /*RCN: right direction; RCY: reverse complemented */
  orientation oret; 
  float oret_score; /*score of the orientation */
  int rc_y; /*Number of hits confirming SeqCtg is reverse complemented*/ 
  int rc_n; /*Number of hits confirming SeqCtg is in right direction   */ 
  float rc_y_score;/*Score of right direction */  
  float rc_n_score;/*Score of reverse complemented */  

  float median; /*Medians of coordinates of hits*/

  /*1: SeqCtg whose orientation can be determined; 
   *2: SeqCtg whose orienation can be determined by its neighbors or distant SeqCtgs;
   *3: SeqCtg whose orientation can not be determined and who has neighbors
   *   or distant SeqCtgs whose orienation also can not be determined.  
   *0: SeqCtg whose orienation can not be determined and which has no neighbors 
   *   or distant SeqCtgs
   *SeqCtgs with included as 0 can not be ordered 
   *SeqCtgs with all other values can be ordered  */
  int included; 

  HitClone **hits_assigned; /*Assinged hit list*/
  int count_assigned; /*Number of assigned hits */
} SeqCtg;

/*Scores of an aligment */
typedef struct {
  int score;
  char direction;/*U: Up; L: Left; D: Diagonal */
} Scores;

void ZevaluateClone();
void quitEvaluateClone();
void order_seqctgs();
extern void ZselectMTP();
#endif
