enum bss_datatype { 
                    BSS_BES, 
                    BSS_SEQCTG, 
                    BSS_CTG,
                    BSS_QSTART, 
                    BSS_QEND, 
                    BSS_TLEN,
                    BSS_TSTART, 
                    BSS_TEND, 
                    BSS_MARKER,
                    BSS_RC,
                    BSS_CLONE,
                    BSS_SCORE,
                    BSS_EVALUE,
                    BSS_IDENTITY, 
                    BSS_NUMTYPES
                };

char bss_headers[][20] = {
                        "BES",
                        "SeqCtg",
                        "Contig",
                        "Start",
                        "End",  
                        "BES_Len",
                        "BES_Start",
                        "BES_End",
                        "Marker",
                        "RC",
                        "Clone",
                        "Score",
                        "EValue",
                        "Identity"
                        };



enum  bss_datatype bss_column_order[BSS_NUMTYPES];
int bss_ncols = -1;

union bss_data_elt
{
    char* str;
    int i;
};

struct bss_hit
{
    union bss_data_elt data[BSS_NUMTYPES];
};

char bss_fields[30][20];

