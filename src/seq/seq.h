#ifndef SEQ_H
#define SEQ_H

#include "tree.h"

void init_seq_tree();
void insert_sequence(FPC_SEQ* pseq);
FPC_SEQ* find_sequence(char* name);
void destroy_seq_tree();
void free_seq_data(void** pdata);
void displaysequence(FPC_SEQ* pseq);

extern FPC_TREE seq_tree;

struct seq_props 
{
    int min_clones; 
    int min_ctgs;
    int window_size;
    int top_n;
    int use_unique;
    int search_placed;
    int search_multiplaced;
    int search_ends;
    int endmatch_check;
    char search_name[31];
};
extern struct seq_props SeqProps;
extern int seq_search_refresh;

typedef enum    { 
                FT_SEQ,         // sequence info file
                FT_BES,         // BES map file
                FT_BSS,         // BSS file
                } file_type;
struct file_list
{
    char path[MAXPATHLEN];
    int n1;
    int n2;
    file_type type;
    struct file_list* next;
}; 

struct hitdata
{
    char besname[256];
    char seqname[256];
    int seqctg;
    int qstart,qend,tstart,tend;
};

extern struct file_list *seq_info_files;

extern void fpc_load_info_file(char* line);

#endif /*SEQ_H*/
