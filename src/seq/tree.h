#ifndef TREE_H
#define TREE_H

/* WMN general purpose tree and stack functions used by the draft code.
*
* The tree allows to quickly insert and retrieve by name, with
* no need for sorting. 
* It might have been better to do this with sorted arrays as for the clone
* list, although this would make it more difficult to keep track of the
* seqctg->supercontig relationships within the array because 
* the array indexes would change under sorting. 
*/

typedef struct fpc_tree
{
    char key;
    void* pdata;                              
    struct fpc_tree* child;
    struct fpc_tree* sibling;
}   FPC_TREE;

typedef struct fpc_stack
{
    FPC_TREE* node;    /* this should be non-zero except for the stack top */
    struct fpc_stack* next;   /* next higher item on stack;
                           in top element it points to stack bottom */
    struct fpc_stack* prev;   /* next lower item on the stack */    
} FPC_STACK;

void clear_fpc_tree(FPC_TREE* ptree, void (*data_free)(void** pdata));
void dump_fpc_tree(FPC_TREE* ptree,int level);
void fpc_tree_insert_node(FPC_TREE* ptree, void* pdata, char* label);
void* fpc_tree_find_data(FPC_TREE* ptree, char* label);
int fpc_tree_remove_data(FPC_TREE* ptree, char* label, void (*data_free)(void** pdata));
void init_fpc_tree(FPC_TREE* ptree);
int fpc_tree_count_filled_nodes(FPC_TREE* ptree);
int fpc_tree_has_content(FPC_TREE* ptree);

FPC_TREE* fpc_tree_next_node(FPC_TREE* pnode,FPC_STACK *pstack);

void fpc_stack_init(FPC_STACK* pstack);
void fpc_stack_destroy(FPC_STACK* pstack);
void fpc_stack_push(FPC_STACK* pstack, FPC_TREE* node);
FPC_TREE* fpc_stack_pop(FPC_STACK* pstack);

#endif /*TREE_H*/
