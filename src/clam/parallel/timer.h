
#ifndef TIMER_H
#define TIMER_H

/* defines timeval */
#include <sys/time.h>

/* Used to manage timeval structure */
struct timeval normalizeTimeval(struct timeval a);
struct timeval addTimeval      (struct timeval a, struct timeval b);
struct timeval subtractTimeval (struct timeval a, struct timeval b);
const char*    formatTimeval   (struct timeval a);

/* In the future I will allow concurrent timers to run, this is a dummy handle till then */
typedef struct Timer_t
{
	/* currently accumulated time - not from running clock */
	struct timeval real;
	struct timeval profile;
	struct timeval virtual;
	
	int running;                /* is the timer running, 1 = yes, 0 = no */
	struct Timer_t* next;       /* used for the linked list in timer.c */
} fpc_timer_t;

/* The timer functions */
void initTimer (fpc_timer_t* x); /* Prepares a timer */
void zeroTimer (fpc_timer_t* x); /* zeros even a running timer */
int  startTimer(fpc_timer_t* x); /* 0 = ok, -1 = already running */
int  stopTimer (fpc_timer_t* x); /* 0 = ok, -1 = not started */

/* Read the total time measured by a timer */
struct timeval getRealTime   (const fpc_timer_t* x);
struct timeval getProfileTime(const fpc_timer_t* x);
struct timeval getVirtualTime(const fpc_timer_t* x);

#endif

