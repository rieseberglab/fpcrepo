

#ifndef SERIAL_H
#define SERIAL_H

union chunk1
{
  float   f[2];
  int32_t i[2];
  char    c[8];
  /* Add Gaurav */

  double d;
  
};

union chunk
{
  float   f;
  int32_t i;
  char    c[4];

};




int writePz(const struct tmpPz* wpz, int socket);
int readPz(struct tmpPz* wpz, int socket);

int writeProj(const struct project* proj, int socket);
int readProj(struct project* proj, int socket);

int writeCpM(const struct CpMvar* cpm, int socket);
int readCpM(struct CpMvar* cpm, int socket);

int releaseCzFastCache();
int writeLoadCzFastCache(int socket);
int readLoadCzFastCache(int socket);

int readAcedata(int socket);
int writeAcedata(int socket);

int readMarkerdata(int socket);
int writeMarkerdata(int socket);

int readMatrix(int socket);
int writeMatrix(int socket);

int readLoop(int socket, void* data, int len);

#endif












