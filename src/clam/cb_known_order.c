/***************************************************************
                             cb_known_order.c
This was zorder.c, and some routines from zap.c were moved in
That is, Build, Fp_order and Gel_order
***************************************************************/
#include <stdio.h>
#include <malloc.h>
#include <math.h>
#include <clam.h>

extern int Zcreate_node();
extern int make_this_Zset();
extern void Zprepare_for_display();
extern int get_Zset(), Zbuild_matrix();
extern void cleanCB(), drawCBmap(), showClam(), little_wrap_Up();
extern void little_wrap_up();
static void Zctg_order(), Zfp(), Zgel();
static int max_csort=0;

/************************************************************/
static void do_display(int type)
{
int i;
    if (type==0) {
       if (Zset[ZS].n_clone>=max_csort-1) { 
          max_csort+=20;
          i = max_csort * sizeof(struct Zset_csort);
          if (Zset[ZS].csort==NULL) Zset[ZS].csort = (struct Zset_csort *) malloc(i);
          else Zset[ZS].csort = (struct Zset_csort *) realloc(Zset[ZS].csort,i);
          NOMEM2a(Zset[ZS].csort, "struct Zset_csort",Zset[ZS].n_clone+1,i);
        }
    }
    else {
       i = (Zset[ZS].n_clone+1) * sizeof(struct Zset_csort); 
       Zset[ZS].csort = (struct Zset_csort *) malloc(i);
       NOMEM2a(Zset[ZS].csort, "struct Zset_csort",Zset[ZS].n_clone+1,i);
    }
    Zprepare_for_display();
}
/************************************************************/
static void no_buried()
{
int i;
   for (i=0; i<ZZ.size; i++) {
      ZZ.matrix[i].mtype = ZZ.matrix[i].save_mtype = IAM_NOTHING;
      ZZ.matrix[i].high_match = ZZ.matrix[i].save_high_match = -1;
      ZZ.matrix[i].cbmap = ZIGNORED;
   }
}
/****************************************************************
                       Def: Zbuild_zap
The matrix keeps build and the CBmap window appears.
Then, when a clone is picked, its added to the CBmap.
****************************************************************/
void Zbuild_zap()
{
   cleanCB();
   strcpy(Fn,"Add");
   LastCB=Add;
   Zadd=1; Zorder=0; max_csort=0;
   if (!Zbuild_matrix(currentctg)) return;
   no_buried();
   if ((ZS = get_Zset()) == -1) return;
   drawCBmap();
   if (graphActivate(g99)) graphPop();
   printf("Build: select clones from contig to add to CBmap\n");
}
/****************************************************************
                       Def: Zfp_zap
The order is based on the order of clones in the Fingerprint window
****************************************************************/
void Zfp_zap()
{
   cleanCB();
   strcpy(Fn,"Fp");
   LastCB=Fp_order;
   Zadd=0; Zorder=1; 
   if (!Zbuild_matrix(currentctg)) return;
   no_buried();

   if (cloneindex[0]==-1) {
       sprintf(ZBuf,"No fingerprint window. Using contig order."); showClam(); 
       Zctg_order();
   }
   else {
       sprintf(ZBuf,"Fingerprint order for CBmap."); showClam(); 
       Zfp();
   }
   little_wrap_up();
}
/****************************************************************
                       Def: Zgel_zap
The order is based on the order of clones in the Gel window
****************************************************************/
void Zgel_zap()
{
   cleanCB();
   strcpy(Fn,"Gel");
   LastCB=Gel_order;
   if (gelcloneindex[0]==-1) {
       sprintf(ZBuf,"** No gel trace."); showClam(); 
       return;
   }
   Zadd=0; Zorder=1;
   if (!Zbuild_matrix(currentctg)) return;
   no_buried();

   Zgel();
   little_wrap_up();
}
/***************************************************************
                DEF: Zadd_clhigh 
this is called when a clone is picked on the map
****************************************************************/
struct contig *ZZp(int i)
{
struct contig *p;

  for (p=root; p!=NULL && ZZ.matrix[i].cin != p->next; p = p->new);
  return p;
}
/***************************************************************/
void Zadd_clhigh()
{
int i;
ZPQZ pq;
void Zclamdump();

   if (clhigh==NULL) return;
   for (i=0; i<ZZ.size; i++) 
     if (ZZp(i) == clhigh) {
        if (ZZ.matrix[i].cbmap <= -1 && ZZ.matrix[i].nbands > 0) {
            Zcreate_node(i);
            pq.C=i; 
            pq.score = -1; 
            ZZ.matrix[i].cbmap = ZS;
            Zset[ZS].n_clone++;
            make_this_Zset(&pq);
            do_display(0);
         }
         return;
      }
}
/***************************************************************
                DEF: Zctg_order
****************************************************************/
static void Zctg_order()
{
int j;
ZPQZ pq;
struct contig *p;
CLONE clp;

   printf("Building CBmap of all visable clones in contig.\n");
   if ((ZS = get_Zset()) == -1) return;

   for(p=root; p!=NULL; p = p->new)
   {
      clp = arr(acedata, p->next, CLONE);

      if (showburied==1 && clp.match[0]!=' ') continue;
      if (showburied==2 && clp.mattype == PSEUDO) continue;

      for (j=0; j< ZZ.size && p->next != ZZ.matrix[j].cin; j++);
      if (j==ZZ.size) {
        printf("FPC internal error: index not found\n");
        break;
      }
      Zcreate_node(j);
      pq.C=j; 
      pq.score = -1; 
      ZZ.matrix[j].cbmap = ZS;
      Zset[ZS].n_clone++;
      make_this_Zset(&pq); 
   }
   do_display(1);
}
/***************************************************************
                DEF: Zfp - fp order
****************************************************************/
extern int cloneindex[FPMAX];
static void Zfp()
{
int i,j;
ZPQZ pq;

   if ((ZS = get_Zset()) == -1) return;
   for (i=0; i<FPMAX && cloneindex[i]!=-1; i++) 
   {
      for (j=0; j< ZZ.size && cloneindex[i]!=ZZ.matrix[j].cin; j++);
      if (ZZ.matrix[j].cbmap>=0) {
           printf("Duplicate %s\n",arr(acedata, ZZ.matrix[j].cin, CLONE).clone);
           continue;
      }
      Zcreate_node(j);
      pq.C=j; 
      pq.score = -1; 
      ZZ.matrix[j].cbmap = ZS;
      Zset[ZS].n_clone++;
      make_this_Zset(&pq);
   }
   do_display(1);
}
/***************************************************************
                DEF: Zgel  - gel order
****************************************************************/
extern int gelcount, gelclone[GELMAX];
static void Zgel()
{
int i,j;
ZPQZ pq;

   if ((ZS = get_Zset()) == -1) return;
   for (i=0; i<GELMAX && i < gelcount && gelclone[i]!=-1; i++) 
   {
      for (j=0; j< ZZ.size && gelclone[i]!=ZZ.matrix[j].cin; j++);
      if (ZZ.matrix[j].cbmap>=0) {
          printf("Duplicate %s\n",arr(acedata, ZZ.matrix[j].cin, CLONE).clone);
          continue;
      }
      Zcreate_node(j);
      pq.C=j; 
      pq.score = -1; 
      ZZ.matrix[j].cbmap = ZS;
      Zset[ZS].n_clone++;
      make_this_Zset(&pq);
   }
   do_display(1);
}
