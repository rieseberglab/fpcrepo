/***************************************************************
                       editproj.c
written by CAS
This is the Project Menu off the Project window
26oct04 - remove check for disconnected contigs
***************************************************************/
#include "clam.h"
#include <fcntl.h>
#include <pwd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <float.h>
#include <gtk/gtkwidget.h>

#define graphQuery messQuery

void Zcheck_sim(), Zcheck_map(), Zcheck_seq_clones();
void Zcheck_tol(), Zcheck_tol2(), Zcheck_tol_range();
void file_check_fp();  
void Zdosize(), Zdoband(), Zcleanup();
extern void getdatehead();
extern char projectname[80];
extern void updateproj(), settime(), move_selected();
extern void Zproj_results(), clone_marker_ctg();
extern GtkWidget *ctg_window;
extern int Get_chr_prefix();
extern int Get_ctg_chr();
extern int Zbatch_flag;
extern void merge_from_list();

Graph gproj, gclean;

static char toText[14], fromText[14], ctgText[14], distText[14];
static int  fromBox, toBox, fromCtg, toCtg, editCtg;
static void projpick(), scanPz(), mv_ctg_all();
static void insert_ctg(), user_msg_results(), chr_msg_results(), trace_msg_results();
static void setlefttozero(), sweepchild();
static char userText[100], chrText[100], traceText[100];
static int userBox, chrBox,traceBox, distBox;
static int cMdistance=50;

static void trace_results(), chr_results(), msg_results(), anchor_results(), Q_results();

#define XR 200

static void zhelp()
{
void show_help();
  show_help("Project/Search","proj-search");
}
/**************************************************************
                  DEF: AutoCtgMsg
**************************************************************/
void AutoCtgMsg(int i, char *msg)
{
char tmp[500];
    sprintf(tmp,"%s %s",msg, contigs[i].trace_msg);
    strncpy(contigs[i].trace_msg, tmp, CTGMSG_SZ-1);
}
/**************************************************************
                  DEF: ConCtgMsg
Merges two remarks when two contigs are being merged.
CAS 2-2-3: add flag parameter so works for trace or user message.
  plus, I had some bizaar code here.
**************************************************************/
void ConCtgMsg(int i,char *msg2, int flag)
{
char tmp[CTGMSG_SZ*2], tmp2[CTGMSG_SZ*2];
char msg1[CTGMSG_SZ];
int k, j;
int spaceOnly();

    if (spaceOnly(msg2)) return;

    if (flag==1) strcpy(msg1, contigs[i].trace_msg);
    else strcpy(msg1, contigs[i].user_msg);
    
       /* remove extra spaces */
    sprintf(tmp,"%s %s", msg1, msg2);
    for (k=0; tmp[k]==' ' && k<CTGMSG_SZ*2; k++);
    for (j=0;tmp[k]!='\0' && k<(CTGMSG_SZ*2)-1; k++) 
      if (!(tmp[k]==' ' && tmp[k+1]==' ')) tmp2[j++] = tmp[k];
    tmp2[j]='\0';

    if (strlen(tmp2) > CTGMSG_SZ) {
       printf("Warning: contig remark for ctg%d is over limit %d - truncated\n",
             i, CTGMSG_SZ);
       printf("Full messages is: %s\n", tmp2);
    }
    if (flag==1) strncpy(contigs[i].trace_msg, tmp2, CTGMSG_SZ-1);
    else strncpy(contigs[i].user_msg, tmp2, CTGMSG_SZ-1);
}
/*********************************************************
                  DEF: ZupdateProj
*********************************************************/
void ZupdateProj(int who)
{
    if (graphActivate(gproj)) {
         sprintf(ctgText,"%d",who);
         editCtg=who;
         graphRedraw();
    }
}
/********************************************************
               CLEANUP 
*********************************************************/
void Zstatus_ok() {int i;  
   for (i=0; i<=max_contig; i++) {
       contigs[i].ctgstat=ND; 
   }
   updateproj();
   if (!Zbatch_flag) printf("Complete set all contig status to OK\n");
}
/* cari add 1Mar07 */
void Zstatus_nocb() {int i, cnt=0;  
   for (i=0; i<=max_contig; i++) {
       if (contigs[i].seq>0) {
          contigs[i].ctgstat=NOCB; 
          cnt++;
       }
   }
   updateproj();
   if (!Zbatch_flag) printf("Complete set %d contig status to NoCB\n",cnt);
}
void Zqs2_() {int i;  
   for (i=0; i<=max_contig; i++) {
       contigs[i].ctgQs= -1; 
   }
   updateproj();
   printf("Complete set all Qs to -1\n");
}
void Zclear_chrmsg() {int i;    
   for (i=0; i<=max_contig; i++) {
       contigs[i].chr_msg[0]='\0'; 
   }
   updateproj();
   if (!Zbatch_flag) /* cari 2/4/4 */
      printf("Complete clear chromosome message\n");
}
void Zclear_usermsg() {int i;  
   for (i=0; i<=max_contig; i++) {
       contigs[i].user_msg[0]='\0'; 
   }
   updateproj();
   printf("Complete clear user message\n");
}
void Zclear_tracemsg() {int i; 
   for (i=0; i<=max_contig; i++) {
       contigs[i].trace_msg[0]='\0'; 
   }
   updateproj();
   printf("Complete clear trace message\n");
}

void Zno_auto_chr() {
int i, cnt=0;
   for (i=0; i<=max_contig; i++) {
       if (contigs[i].noedit_chr==1) {
          contigs[i].noedit_chr=0;
          AutoCtgMsg(i, contigs[i].chr_msg);
          contigs[i].chr_msg[0] = '\0';
          cnt++;
       }
   }
   updateproj();
   printf("Complete clear No Auto Chr for %d contigs\n", cnt);
}
void Zno_auto_pos() {
char msg[50];
int i, cnt=0;
   for (i=0; i<=max_contig; i++) {
       if (contigs[i].noedit_pos==1) {
          contigs[i].noedit_pos=0;
          sprintf(msg,"Pos %4.1f",contigs[i].chr_pos); 
          contigs[i].chr_pos=0.0;
          AutoCtgMsg(i, msg);
          cnt++;
       }
   }
   updateproj();
   printf("Complete clear No Auto Pos for %d contigs\n", cnt);
}
static void Zclear_clpRemarks() {
int i;
   for (i=0; i<arrayMax(acedata); i++) 
   {
      arrp(acedata,i,CLONE)->remark = NULL;
      arrp(acedata,i,CLONE)->fp_remark = NULL;
   }
   printf("Clear remarks\n");
}
static void Zreset_oldctg() {int i; 
  for (i=0; i<arrayMax(acedata); i++) 
    arrp(acedata,i,CLONE)->oldctg = arrp(acedata,i,CLONE)->ctg;
   printf("Clear Oldctg\n");
}
static void Zreset_newmarker() {
  int i; struct markertop *top; CLONE *clp;
  for (i=0; i<arrayMax(acedata); i++) {
    clp = arrp(acedata,i,CLONE);
    for (top = clp->marker; top!=NULL; top = top->nextmarker)
       top->new=FALSE;
  }
   printf("Clear New marker\n");
}
static void setlefttozero() 
{
CLONE *clpk;
register int i, k, c;

PRTMESS=0;
   for (c=i=0; i<=max_contig; i++) {
       if (contigs[i].count== 0 || contigs[i].left==0) continue;
       for (k= contigs[i].start; k != -1; k = clpk->next) {
            clpk = arrp(acedata, k, CLONE);
            clpk->selected = TRUE;
       }
       move_selected(i, i, -contigs[i].left); 
       c++;
   }
PRTMESS=1;
   printf("Set Left coordinate to zero for %d contigs\n",c);
}
static void sweepchild() 
{
int i; 
CLONE *clp, *parent;
int cnt=0, unb=0;
void unbury();

  for (i=0; i<arrayMax(acedata); i++) {
      clp = arrp(acedata,i,CLONE);
      if (clp->fp==NULL) continue;
      if (clp->match[0]==' ') continue;
      if (clp->ctg==0) {
          unb++;
          unbury(i);
          continue;
      }
      parent = arrp(acedata,clp->parent,CLONE);
  
      if (clp->x < parent->y && parent->x < clp->y) continue;
  
      printf("Child %s %d %d  Parent %s %d %d    (%d)\n",
            clp->clone,clp->x, clp->y,
               parent->clone, parent->x, parent->y, clp->x-parent->x);


      clp->x = parent->x;
      clp->y = parent->x + clp->fp->b2 - 1;
      cnt++;
  }
  printf("Unbury %d from ctg0. Sweep %d children.\n",unb,cnt);
}
/******************************************************************
                    DEF: set_user_Qs
*****************************************************************/
static void set_user_Qs()
{
register int i;

   for (i=0; i<=max_contig; i++) {
       if (contigs[i].count== 0) continue;
       if (contigs[i].ctgQs!=-1)
          sprintf(contigs[i].user_msg,"Qs %d",contigs[i].ctgQs);
       else
          sprintf(contigs[i].user_msg,"Qs -");
   }
PRTMESS=1;
   printf("Set all Contig User Remarks to number of Qs\n");
}
/*****************************************************************
               DEF: Zmove_ctg
used in ibc
******************************************************************/
int Zmove_ctg(int start, int end, int seq)
{
CLONE *clpk;
int i,j,k,c;
BOOL found;

if(ctg_window!=NULL) gtk_widget_destroy(ctg_window);

PRTMESS=0;
   for (c=0, i=start; i<end; i++) {
       if (contigs[i].count!= 0) continue;
       found=FALSE;
       for (j=i+1; j<=end && !found; j++) {
          if (contigs[j].count== 0) continue;

          for (k= contigs[j].start; k != -1; k = clpk->next) {
               clpk = arrp(acedata, k, CLONE);
               clpk->selected = TRUE;
               if (clpk->ctg==clpk->oldctg) clpk->oldctg=i;
          }
          move_selected(j, i, 0); 
          c++;
          found=TRUE;
        }
   }
PRTMESS=1;
updateproj();
return c;
}
/*****************************************************************
               DEF: move_ctg_all
******************************************************************/
static void mv_ctg_all()
{
CLONE *clpk;
int i,j,k,c;
BOOL found;

if(ctg_window!=NULL) gtk_widget_destroy(ctg_window);
PRTMESS=0;
   for (c=0, i=0; i<=max_contig; i++) {
       if (contigs[i].count!= 0) continue;
       found=FALSE;
       for (j=i+1; j<=max_contig && !found; j++) {
          if (contigs[j].count== 0) continue;

          for (k= contigs[j].start; k != -1; k = clpk->next) {
               clpk = arrp(acedata, k, CLONE);
               clpk->selected = TRUE;
               if (clpk->ctg==clpk->oldctg) clpk->oldctg=i;
          }
          move_selected(j, i, 0); 
          found=TRUE;
          c++;
        }
   }
PRTMESS=1;
updateproj();
printf("Move %d contigs.\n",c);
}
/*****************************************************************
               DEF: doswap
******************************************************************/
static void doswap(int fromCtg, int toCtg)
{
CLONE *clpk;
register int i,k,c;
void contigsAlloc();

  c = max_contig+1;
  contigsAlloc(c); 
PRTMESS=0;
  i = toCtg;
  if (contigs[i].count!= 0) {
    for (k= contigs[i].start; k != -1; k = clpk->next) {
       clpk = arrp(acedata, k, CLONE);
       clpk->selected = TRUE;
       if (clpk->ctg==clpk->oldctg) clpk->oldctg=i;
     }
     move_selected(i, c, 0); 
  }
  i = fromCtg;
  if (contigs[i].count!= 0) {
    for (k= contigs[i].start; k != -1; k = clpk->next) {
       clpk = arrp(acedata, k, CLONE);
       clpk->selected = TRUE;
       if (clpk->ctg==clpk->oldctg) clpk->oldctg=i;
     }
     move_selected(i, toCtg, 0); 
  }
  i = c;
  if (contigs[i].count!= 0) {
    for (k= contigs[i].start; k != -1; k = clpk->next) {
       clpk = arrp(acedata, k, CLONE);
       clpk->selected = TRUE;
       if (clpk->ctg==clpk->oldctg) clpk->oldctg=i;
     }
     move_selected(c, fromCtg, 0); 
  }
PRTMESS=1;
}

/*****************************************************************
               DEF: domove
 fred 6/25/03
******************************************************************/
static void domove(int fromCtg, int toCtg){
  CLONE *clpk;
  int i, j, k, c;
  extern void contigsAlloc(); 

  c = max_contig+1;
  contigsAlloc(c); 
  for(i=toCtg; (contigs[i].count>0) && (i!=fromCtg); i++);
  if(i==fromCtg){
    /*There were no empty contigs between toCtg, fromCtg*/
    doswap(fromCtg, c);
    fromCtg=c;
  }
PRTMESS=0;
  for(j=i; j>toCtg; j--){
    /*Shuffle contigs down*/
    for (k=contigs[j-1].start; k!= -1; k=clpk->next) {
      clpk = arrp(acedata, k, CLONE);
      clpk->selected = TRUE;
      if (clpk->ctg==clpk->oldctg) clpk->oldctg=j-1;
    }
    move_selected(j-1, j, 0); 
  }
PRTMESS=1;

  doswap(fromCtg, toCtg);
}

/*****************************************************************
               DEF: insert_ctg
******************************************************************/
static void insert_ctg()
{
extern void  contigsAlloc();
  if(ctg_window!=NULL) gtk_widget_destroy(ctg_window);
  scanPz();
  if (fromCtg<=0 || fromCtg>max_contig) {
    printf("Error: incorrect contig %d\n",fromCtg);
    return;
  }
  if (toCtg<=0) {
    printf("Error: incorrect contig %d\n",toCtg);
    return;
  }
  if (contigs[fromCtg].count<=0) {
    printf("Error: incorrect contig %d\n",fromCtg);
    return;
  }
  if (toCtg>max_contig) {
    contigsAlloc(toCtg);
  }
  printf("Move ctg%d to ctg%d\n", fromCtg, toCtg);
  domove(fromCtg, toCtg);
  updateproj();
}

/************************************************************
                  DEF: show_markers
*************************************************************/
static void show_markers(type)
{
struct {
  char name[CLONE_SZ];
  int pos;
  int index;
  int cnt;
  char chr[10];
  float fpos;
} results[XR], temp;
CLONE *clp;
struct markertop *mkptr;
struct marker *mkp;
struct markerctgpos *pos;
int i, num, j, xr, found;
struct list *lp;
int cntchr, cnt=0;
char tmp[200], tmp2[2], prtout[2000];
float fdiff;
extern int get_char_string(); /* see ctg2chr */
char chr[10];
FILE *file=NULL;
extern FILE *graphQueryOpen();
char tempdirname[DIR_BUFFER_SIZE];
char tempfilename[FIL_BUFFER_SIZE];

tempdirname[0] = 0;
tempfilename[0] = 0;

  if (type==1 || type==3) {
       file=graphQueryOpen(tempdirname,tempfilename,
                    "txt","w","Enter file name for output");
       if (file==NULL) printf("Cannot open output file \n");
       return;
  }
  scanPz();
  for (num=0; num<=max_contig; num++)
  {
      if (contigs[num].count==0) continue;
      contigs[num].projmsg[0]='\0';
      cnt=0;

      for (xr=0, i=contigs[num].start; i!= -1 && xr<XR; i = clp->next)
      {
         clp = arrp(acedata, i, CLONE);
         for (mkptr = clp->marker; mkptr != NULL && xr<XR; mkptr=mkptr->nextmarker)
         {
             mkp = arrp(markerdata, mkptr->markerindex, MARKER);
                            /* only anchors - for distance or list all  */
             if (type<=1 && !mkp->anchor) continue;

             if (type>1) {  /* if in keyset -- number only, or list */
                for (found=0, lp = listroot; lp!=NULL && !found; lp = lp->next)
                {
                     if (mkptr->markerindex == lp->index) found = 1;

                }
                if (!found) continue;
             }
             for (found=i=0; i<xr && !found; i++) 
                if (results[i].index == mkptr->markerindex) found=1;
             if (found) continue;

             if (type<=1) {  /* anchors */
                 results[xr].index = mkptr->markerindex;
                 Get_chr_prefix(mkp->anchor_bin,results[xr].chr);
                 results[xr].fpos = mkp->anchor_pos;
                 results[xr].cnt = 0;
                 strcpy(results[xr].name, mkp->marker);
                 if (type==0) xr++;
             }
             else if (type==2) { /* keyset, number only */
                 cnt++;
                 results[xr].index = mkptr->markerindex;
                 xr++;
             }
             if (type==1 || type ==3) { /* anchor list or keyset list */
                cnt++;
                for (pos = mkp->pos; pos != NULL; pos = pos->next)
                {
                   if (pos->ctg == num) {
                      results[xr].index = mkptr->markerindex;
                      results[xr].pos = pos->pos;
                      strcpy(results[xr].name, mkp->marker);
                      xr++;
                      break;
                   }
                }
            }
         }
      } /* end marker loop */

      if (cnt > 0) 
            sprintf(contigs[num].projmsg,"%3d ",cnt);
      if (xr==0) continue;

      if (type==0) {  /* distance */
         cnt=cntchr=0;
         for (j=0; j<xr; j++) /* let it do the last one for cntchr */
         {
           for (i=j+1; i<xr; i++)
           {
               if (strcmp(results[j].chr, results[i].chr)!=0) continue;
               if (results[j].fpos>results[i].fpos)
                    fdiff = results[j].fpos-results[i].fpos;
               else fdiff = results[i].fpos-results[j].fpos;

               if (fdiff > (float) cMdistance) {
                   cnt++;
                   results[j].cnt++;
                   results[i].cnt++;
               }
            }
            if (results[j].cnt>results[cntchr].cnt) cntchr=j;
         }
         if (cnt>0) {
                sprintf(tmp,"%s%s/%s/%d", Proj.label_abbrev,
                                  results[cntchr].chr,results[cntchr].name, cnt);
                printf("Ctg%d %s\n",num, tmp);
                if (strlen(contigs[num].projmsg) + strlen(tmp) < MSGSIZE) 
                       strcat(contigs[num].projmsg, tmp);
         }
      }
      if (type==3) { /* all in keyset */
         for (j=0; j<xr-1; j++)
           for (i=j+1; i<xr; i++)
             if (results[j].pos > results[i].pos) {
                temp= results[j];
                results[j] = results[i];
                results[i] = temp;
             }
          for (j=0; j<xr; j++) {
              sprintf(tmp,"%s ",results[j].name);
              if (strlen(contigs[num].projmsg) + strlen(tmp) < MSGSIZE) {
                 strcat(contigs[num].projmsg, tmp);
                 strcat(prtout, tmp);
              }
              else {
                 if (strlen(contigs[num].projmsg) + 1 < MSGSIZE) 
                      strcat(contigs[num].projmsg, "*");
                 strcat(prtout, tmp);
              }
          }
          fprintf(file, "%s\n",prtout);
          prtout[0] = '\0';
      } 
      if (type==1) { /* anchors */
         tmp[0]='\0';
         if (Get_ctg_chr(contigs[num].chr_msg, tmp) ) {
             Get_chr_prefix(tmp, chr);
         }
         else chr[0]='?';
         if (contigs[num].noedit_chr) tmp[0]='C'; else tmp[0]=' ';
         if (contigs[num].noedit_pos) tmp2[0]='P'; else tmp2[0]=' ';
         sprintf(contigs[num].projmsg,"%2s %c %c", chr, tmp[0], tmp2[0]);
         sprintf(prtout,"Ctg%-4d %c %s%s ", num, tmp[0], Proj.label_abbrev, chr);

         for (j=0; j<xr-1; j++)
           for (i=j+1; i<xr; i++)
             if ((strcmp(results[i].chr,chr)==0 && strcmp(results[j].chr,chr)!=0) ||
                 (strcmp(results[j].chr,chr)==0 && strcmp(results[i].chr,chr)==0 && 
                  results[i].fpos < results[j].fpos)) {
                     temp= results[j];
                     results[j] = results[i];
                     results[i] = temp;
             }
          for (j=0; j<xr; j++) {
             if (strcmp(chr, results[j].chr)==0)  
                   sprintf(tmp,"%s:%3.1f ",results[j].name, results[j].fpos);
             else  sprintf(tmp,"%s#%s:%3.1f ",results[j].name, results[j].chr,results[j].fpos);

             if (strlen(contigs[num].projmsg) + strlen(tmp) < MSGSIZE) {
                 strcat(contigs[num].projmsg, tmp);
                 strcat(prtout, tmp);
             }
             else {
                 if (strlen(contigs[num].projmsg) + 1 < MSGSIZE) 
                      strcat(contigs[num].projmsg, "*");
                 strcat(prtout, tmp);
             }
         }
         fprintf(file,"%s\n",prtout);
         prtout[0] = '\0';
      } 
  } 
  Zproj_results(3);
}
static void anchor_results()
{
  show_markers(1);
}
static void distance_results()
{
  show_markers(0);
}
/* called by the Project window for Show Keyset */
void keyset_results()
{
  show_markers(2);
}
void list_keyset_results()
{
  show_markers(3);
}
/************************************************************
                  DEF: Q_results
*************************************************************/
static void Q_results()
{
int i, num, numqs=0, qs=0, cnt=0, five=0, lt5=0;
char msg[50];

for (i=1; i<2; i++) 
  for (num=1; num<=max_contig; num++)
  {
      contigs[num].projmsg[0] = '\0';
      if (contigs[num].count==0) continue;
      cnt++; 
      if (contigs[num].ctgQs==0) continue;
      if (contigs[num].ctgQs<=5 && i==1) continue;
      if (contigs[num].ctgQs>5 && i==2) continue;
   
      if (contigs[num].ctgQs==-1) continue;
      sprintf(msg,"%d",contigs[num].ctgQs);
      strcpy(contigs[num].projmsg,msg);
      if (contigs[num].ctgQs>5) five++;
      else lt5++;
      qs++;
      numqs += contigs[num].ctgQs;
  } 
  Zproj_results(3);
  printf("Contigs %d  Qs %d(%d) <=5Qs %d (%5.3f) >5Qs %d (%5.3f)\n",
    cnt, numqs, qs, lt5, 100.0 * ((float)lt5/(float)cnt), five, 
                    100.0 * ((float)five/(float)cnt));
}
/************************************************************
                  DEF: msg_results
*************************************************************/
static void msg_results()
{
int num;
  for (num=0; num<=max_contig; num++)
  {
      if (contigs[num].count==0) continue;
      strcpy(contigs[num].projmsg,contigs[num].user_msg);
  } 
  Zproj_results(3);
}
/************************************************************
                  DEF: trace_results
*************************************************************/
static void trace_results()
{
int num;
  for (num=0; num<=max_contig; num++)
  {
      if (contigs[num].count==0) continue;
      strcpy(contigs[num].projmsg,contigs[num].trace_msg);
  } 
  Zproj_results(3);
}
/************************************************************
                  DEF: chr_results
*************************************************************/
static void chr_results()
{
int num;
  for (num=0; num<=max_contig; num++)
  {
      if (contigs[num].count==0) continue;
      strcpy(contigs[num].projmsg,contigs[num].chr_msg);
  } 
  Zproj_results(3);
}
/************************************************************
                  DEF: user_msg_results
*************************************************************/
static void user_msg_results()
{
int num;
char word[100];
  sscanf(userText,"%s", word);
  printf("Searching for %s in remark\n",word);
  for (num=0; num<=max_contig; num++)
  {
      contigs[num].projmsg[0] = '\0';
      if (contigs[num].count==0) continue;
      if (strstr(contigs[num].user_msg, word))
         strcpy(contigs[num].projmsg,contigs[num].user_msg);
  } 
  Zproj_results(3);
}
/************************************************************
                  DEF: chr_msg_results
*************************************************************/
static void chr_msg_results()
{
int num, cnt=0;
char word[100];
  sscanf(chrText,"%s", word);
  printf("Searching for %s in Chr Remark\n",word);
  for (num=0; num<=max_contig; num++)
  {
      contigs[num].projmsg[0] = '\0';
      if (contigs[num].count==0) continue;
      if (strstr(contigs[num].chr_msg, word)) {
         strcpy(contigs[num].projmsg,contigs[num].chr_msg);
         cnt++;
      }
  } 
  Zproj_results(3);
  printf("Found %d results\n",cnt);
}
/************************************************************
                  DEF: trace_msg_results
*************************************************************/
static void trace_msg_results()
{
int num, cnt=0;
char word[100];
  sscanf(traceText,"%s", word);
  printf("Searching for %s in Trace remark\n",word);
  for (num=0; num<=max_contig; num++)
  {
      contigs[num].projmsg[0] = '\0';
      if (contigs[num].count==0) continue;
      if (strstr(contigs[num].trace_msg, word)) {
         strcpy(contigs[num].projmsg,contigs[num].trace_msg);
         cnt++;
      }
  } 
  Zproj_results(3);
  printf("Found %d results\n",cnt);
}

/*******************************************************
                  DEF: ZquitEditProj
********************************************************/
void ZquitEditProj()
{
   if (graphActivate(gclean)) graphDestroy();
   if (graphActivate(gproj)) graphDestroy();
}

/*********************************************************
                  DEF: ZeditProj
*********************************************************/
void ZeditProj()
{
float col, col1=1.0, row=1.0;
static MENUOPT amenu[] = { {ZquitEditProj, "Close"},
    { graphPrint,"Print Screen"}, 
    { 0, 0 } };

  if(graphActivate(gproj)){   
    graphPop();
  }
  else{                                         /* x, y, w, h */
    gproj = graphCreate (TEXT_FIT,"Search and misc for Contigs",.2,.2,.39,.60);
    ctgText[0]='\0';
    editCtg = -1;
    sprintf(distText,"%d",cMdistance);
  }
  graphClear();
  graphRegister (PICK, projpick) ;
  graphMenu(amenu);

  graphText("Search by Word (enter <CR> after word):",col1,row);
  row+=1.5;
  graphText("User Remark",2,row);
  userBox = graphTextEntry(userText, 20, 17, row,user_msg_results);
  row+=1.5;
  graphText("Chr/Lg Remark",2,row);
  chrBox = graphTextEntry(chrText, 20, 17, row,chr_msg_results);
  row+=1.5;
  graphText("Trace Remark",2,row);
  traceBox = graphTextEntry(traceText, 20, 17, row,trace_msg_results);
  row+=2.0;
  graphText("Display contigs by:",col1,row);
  row+=1.5;
  graphButton("Contig Chr Remarks", chr_results,2.0,row);
  row+=1.5;
  graphButton("Contig User Remarks", msg_results,2.0,row);
  row+=1.5;
  graphButton("Contig Trace Remarks", trace_results,2.0,row);
  row+=1.5;
  graphButton("Q contigs", Q_results,2.0,row);
  row+=1.5;
  graphButton("Anchors > ", distance_results,2.0,row);
  distBox = graphTextEntry(distText, 5, 14, row,scanPz);
  graphText("distance apart",20,row);
  row+=1.5;
  graphButton("Contigs with anchors & print",anchor_results,2.0,row);
  row+=1.5;
  graphButton("Markers from keyset & print",list_keyset_results,2.0,row);
  row+=2.0;
  graphLine(0.0,row,50,row);
  row+=1.0;
  graphText("Re-number contigs:",col1,row);
  row+=1.5;
  graphButton("Move contigs up", mv_ctg_all,2.0,row);
  row+=1.5;
  col=2.0;
  graphButton("Move", insert_ctg,2.0,row);
  col+=6.0;
  graphText("Contig",col,row);
  col+=8.0;
  fromBox = graphTextEntry(fromText, 5, col, row,scanPz);
  col+=6.0;
  graphText("to Contig",col,row);
  col+=11.0;
  toBox = graphTextEntry(toText, 5, col, row,scanPz);
  row+=2.0;
  graphLine(0.0,row,50,row);
  row+=1.0;
  graphText("Misc:",col1,row);
  row+=2.0;
  graphButton("File of pairs of clones to compare",file_check_fp,2.0,row);   
  row+=2.0;
  graphButton("Find clones with mult gels < cutoff",Zcheck_tol,2.0,row);
  row+=1.5;
  graphButton("Find clones with mult gels > cutoff",Zcheck_tol2,2.0,row);
  row+=1.5;
  graphButton("Band Stats",Zdoband,2.0,row);
  graphButton("Size Stats",Zdosize,21.0,row);
  row+=2.0;
  graphButton("Set all Contig User Remarks to Qs",set_user_Qs,2.0,row);
  row+=2.0;
  graphLine(0.0,row,50,row);
  row+=1.0;
  graphButton("Close",ZquitEditProj,2.0,row);
  graphButton("Cleanup Menu",Zcleanup,9.0,row);
  graphButton("Help",zhelp,30.0,row);
#ifdef AGCoL 
  graphButton("Cl/Mk=Ctg",clone_marker_ctg,2.0,row);
  graphButton("EvalHumanmap",Zcheck_map,12.0,row);
  graphButton("Simulation",Zcheck_sim,27.0,row);
#endif
  graphRedraw();
}

void scanPz()
{
  if(graphActivate(gproj)){   
    sscanf(ctgText,"%d",&editCtg);
    sscanf(toText,"%d",&toCtg);
    sscanf(fromText,"%d",&fromCtg);
    sscanf(distText,"%d",&cMdistance);
  }
}
void projpick(int box,double x,double y)
{
  if(box == 0) return;
  if (box==toBox) toBox = graphTextEntry(toText,14,0,0,NULL);
  else if (box==fromBox) fromBox = graphTextEntry(fromText,14,0,0,NULL);
  else if (box==userBox) userBox = graphTextEntry(userText,14,0,0,NULL);
  else if (box==chrBox) chrBox = graphTextEntry(chrText,14,0,0,NULL);
  else if (box==traceBox) traceBox = graphTextEntry(traceText,14,0,0,NULL);
  else if (box==distBox) distBox = graphTextEntry(distText,14,0,0,NULL);
  graphRedraw();
}
/*********************************************************
                  DEF: Zcleanup
*********************************************************/
void Zcleanup()
{
float  col1=1.0, row=1.0;
static MENUOPT amenu[] = { {graphDestroy, "Close"},
    { graphPrint,"Print Screen"}, 
    { 0, 0 } };

  if(graphActivate(gclean)){   
    graphPop();
  }
  else{                                         /* x, y, w, h */
    gclean = graphCreate (TEXT_FIT,"Clean up",.2,.2,.39,.36);
  }
  graphClear();
  graphMenu(amenu);

  graphText("Cleanup:",col1,row);
  row+=1.5;
  graphButton("Clear all contig Chr remarks",Zclear_chrmsg,2.0,row);
  row+=1.5;
  graphButton("Clear all contig User remarks",Zclear_usermsg,2.0,row);
  row+=1.5;
  graphButton("Clear all contig Trace remarks",Zclear_tracemsg,2.0,row);
  row+=1.5;
  graphButton("Change all contig status to OK",Zstatus_ok,2.0,row);
  row+=1.5;
  graphButton("Change all seq contig status to NoCB",Zstatus_nocb,2.0,row);
  row+=1.5;
  graphButton("Change Qs to - (no value)",Zqs2_,2.0,row);
  row+=1.5;
  graphButton("Remove No Auto Update for Chr/Lg",Zno_auto_chr,2.0,row);
  row+=1.5;
  graphButton("Remove No Auto Update for Pos",Zno_auto_pos,2.0,row);
  row+=2.0;
  graphButton("Clear clone remarks",Zclear_clpRemarks,2.0,row);
  row+=1.5;
  graphButton("Clear clone Oldctg",Zreset_oldctg,2.0,row);
  row+=1.5;
  graphButton("Clear marker New",Zreset_newmarker,2.0,row);
  row+=2.0;
  graphButton("Set left end of each contig to zero", setlefttozero,2.0,row);
  row+=1.5;
  graphButton("Sweep children under parent", sweepchild,2.0,row);
  graphRedraw();
}

