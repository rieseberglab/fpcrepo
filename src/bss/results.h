#ifndef INC_results_H
#define INC_results_H

#include "bss.h"

#define MAXPRINTCMD 30
#define MAXCOLSIZE 256

extern int alphasort();
extern char *rindex();
extern int centreMarker;
extern int markerTrail;
extern void suffixremove();
extern FILE *graphQueryOpen();
extern void displaymarker();
extern void highlightnodraw();
extern void highlightmarkernodraw();
extern void freelistmem();
extern void setclassclone();
extern void show_help();

extern GtkWidget *ctg_window;

/* ADD_COLUMNS - add sort type */
typedef enum{NONE,SBJCT,RC,CLN,CONTIG,MRKR,SEQCTG,START,END,LENGTH1,LENGTH2,
             SCORE,EVAL,IDENTITY, BLATSCORE, PCTMATCH, INTRON, QUERYLEN,
            START2, END2} prevsorttype;

struct prevsortlisttype{
   prevsorttype prevsort;
   struct prevsortlisttype *next;
};

struct prevsortlisttype *prevsortlist=NULL;

int numResultFiles;
int maxResultFiles=0;

Graph results=0;    /*Window for results.*/

int r_selected_row1= -1;   /*Selected row in the marker/seqctg table.*/
int r_selected_row2= -1;   /*Selected row in the contig table.*/
int r_selected_row3= -1;   /*Selected row in the hitlist table.*/
int r_max_contig;          /*Set to max_contig everytime a result file is opened.
                             This prevents seqfaults due to a modification of 
                             max_contig outside of BSS.*/
int bss_results_deleted=0;
int bss_results_resorted=0;
GtkWidget *r_clist1;         /*The queryfilsec clist.*/
GtkWidget *r_clist2;         /*The contig clist.*/
GtkWidget *r_clist3;         /*The hitlist clist.*/
GtkWidget *r_typeOfSearch_label;
GtkWidget *r_typeOfMapping_label;
GtkWidget *r_inputFile_label;
GtkWidget *r_numDBs_label;
GtkWidget *r_dbDir_label;
GtkWidget *r_eval_label;
GtkWidget *r_xtraParm_label;
GtkWidget *r_numOfHits_label;
GtkWidget *r_queryFilSecWithHits_label;
GtkWidget *r_queryFilSec_label;
GtkWidget *r_minLetters_label;
GtkWidget * r_table;

Graph print=0;      /*Print dialog.*/
GtkWidget *p_entry[1];
char p_printCmd[MAXPRINTCMD+1];
int print_letter, print_legal;
GtkWidget *p_wrapper_box;/*Put all GTK widgets in this box.*/
GtkWidget *p_event_box;

GtkWidget *p_popup_menu;
GtkWidget *p_quit_item;
#endif /*INC_results_H*/
