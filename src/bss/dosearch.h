#ifndef INC_dosearch_H
#define INC_dosearch_H

#include <glib.h>
#include "bss.h"

extern char *rindex();
extern pid_t waitpid();

#define BES_EMPTY "NO .bes FILES"
#define SEQ_EMPTY "NO .seq FILES"
#define MRK_EMPTY "NO .mrk FILES"
#define NUMOFINDEXFILES 5
#define MAXBLASTCLONE 37
#define MAXBLASTOUTFILE 300
#define MAXENDTEXT 13
#define MAXETEXT 15
#define MAXMINLET 7
#define MAXSEQDIR 300
#define MAXPARMSTRING 50
#define SEQ_DB_FILENAME			"comp_seq_db.bs"

struct nametype{
   char name[256];
};
struct nametype *dbFiles;

/*Variables that may be set by user through graphTextEntry().*/
char blastoutfile[MAXBLASTOUTFILE+1];		/*Suffix of file to
                                                  write output to.*/
char qryDir[MAXSEQDIR+1];
char qryFile[MAXSEQDIR+1];
char qrySuffix[MAXSEQDIR+1];  /*fred 8/27/03*/
char dbDir[MAXSEQDIR+1];
GArray* g_dbs;
char dbFile[MAXSEQDIR+1];
char dbSuffix[MAXSEQDIR+1];   /*fred 8/27/03*/
char xtraParm[MAXPARMSTRING+1];         /*Any extra BLAST parameters the user
                                          may wish to add.*/
int childpid[15];		/*Array of pids for currently active children.*/
int activekids=0;		/*Number of currently active children.*/
int stopblast=0;

int clones_matched=0, clones_not_matched=0;  /* Used for mark2seq searches to inform user
                                                how many sequences have corresponding clones.*/

int batch_update_db=FALSE;         /*TRUE if updating database from command line*/

long double eVal=1e-100;
int blatscore = 100;

int blastprocesscount = 0;	/*Number of blast processes that have been started.*/
struct nametype *dbFiles;

int save_output_flag=TRUE;

int use_blast_flag=1;
int use_megablast_flag=0;
int use_blat_flag=0;

int split_bss_flag=FALSE;

#endif /*INC_dosearch_H*/
