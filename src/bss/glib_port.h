#ifndef GLIB_PORT
#define GLIB_PORT   

#include <glib.h>

// Borrowed from newer GLib versions
#if !GLIB_CHECK_VERSION(2, 0, 0)
#  define G_N_ELEMENTS(arr) (sizeof (arr) / sizeof ((arr)[0]))
#  define G_STRINGIFY(macro_or_string)	G_STRINGIFY_ARG (macro_or_string)
#  define	G_STRINGIFY_ARG(contents)	#contents

/* Provide a string identifying the current code position */
#  if defined(__GNUC__) && (__GNUC__ < 3) && !defined(__cplusplus)
#    define G_STRLOC	__FILE__ ":" G_STRINGIFY (__LINE__) ":" __PRETTY_FUNCTION__ "()"
#  else
#    define G_STRLOC	__FILE__ ":" G_STRINGIFY (__LINE__)
#  endif

gsize g_strlcpy (gchar       *dest,
                 const gchar *src,
                 gsize        dest_size);
gchar* g_strrstr (const gchar *haystack,
                  const gchar *needle);

//#include <glib/gerror.h>

/* I'm not sure I remember our proposed naming convention here. */
#define G_SPAWN_ERROR g_spawn_error_quark ()

typedef enum
{
  G_SPAWN_ERROR_FORK,   /* fork failed due to lack of memory */
  G_SPAWN_ERROR_READ,   /* read or select on pipes failed */
  G_SPAWN_ERROR_CHDIR,  /* changing to working dir failed */
  G_SPAWN_ERROR_ACCES,  /* execv() returned EACCES */
  G_SPAWN_ERROR_PERM,   /* execv() returned EPERM */
  G_SPAWN_ERROR_2BIG,   /* execv() returned E2BIG */
  G_SPAWN_ERROR_NOEXEC, /* execv() returned ENOEXEC */
  G_SPAWN_ERROR_NAMETOOLONG, /* ""  "" ENAMETOOLONG */
  G_SPAWN_ERROR_NOENT,       /* ""  "" ENOENT */
  G_SPAWN_ERROR_NOMEM,       /* ""  "" ENOMEM */
  G_SPAWN_ERROR_NOTDIR,      /* ""  "" ENOTDIR */
  G_SPAWN_ERROR_LOOP,        /* ""  "" ELOOP   */
  G_SPAWN_ERROR_TXTBUSY,     /* ""  "" ETXTBUSY */
  G_SPAWN_ERROR_IO,          /* ""  "" EIO */
  G_SPAWN_ERROR_NFILE,       /* ""  "" ENFILE */
  G_SPAWN_ERROR_MFILE,       /* ""  "" EMFLE */
  G_SPAWN_ERROR_INVAL,       /* ""  "" EINVAL */
  G_SPAWN_ERROR_ISDIR,       /* ""  "" EISDIR */
  G_SPAWN_ERROR_LIBBAD,      /* ""  "" ELIBBAD */
  G_SPAWN_ERROR_FAILED       /* other fatal failure, error->message
                              * should explain
                              */
} GSpawnError;

typedef void (* GSpawnChildSetupFunc) (gpointer user_data);

typedef enum
{
  G_SPAWN_LEAVE_DESCRIPTORS_OPEN = 1 << 0,
  G_SPAWN_DO_NOT_REAP_CHILD      = 1 << 1,
  /* look for argv[0] in the path i.e. use execvp() */
  G_SPAWN_SEARCH_PATH            = 1 << 2,
  /* Dump output to /dev/null */
  G_SPAWN_STDOUT_TO_DEV_NULL     = 1 << 3,
  G_SPAWN_STDERR_TO_DEV_NULL     = 1 << 4,
  G_SPAWN_CHILD_INHERITS_STDIN   = 1 << 5,
  G_SPAWN_FILE_AND_ARGV_ZERO     = 1 << 6
} GSpawnFlags;

GQuark g_spawn_error_quark (void);

gboolean g_spawn_async (const gchar           *working_directory,
                        gchar                **argv,
                        gchar                **envp,
                        GSpawnFlags            flags,
                        GSpawnChildSetupFunc   child_setup,
                        gpointer               user_data,
                        int                   *child_pid,
                        gchar                **error);


/* Opens pipes for non-NULL standard_output, standard_input, standard_error,
 * and returns the parent's end of the pipes.
 */
gboolean g_spawn_async_with_pipes (const gchar          *working_directory,
                                   gchar               **argv,
                                   gchar               **envp,
                                   GSpawnFlags           flags,
                                   GSpawnChildSetupFunc  child_setup,
                                   gpointer              user_data,
                                   int                 *child_pid,
                                   gint                 *standard_input,
                                   gint                 *standard_output,
                                   gint                 *standard_error,
                                   gchar               **error);


/* If standard_output or standard_error are non-NULL, the full
 * standard output or error of the command will be placed there.
 */

gboolean g_spawn_sync         (const gchar          *working_directory,
                               gchar               **argv,
                               gchar               **envp,
                               GSpawnFlags           flags,
                               GSpawnChildSetupFunc  child_setup,
                               gpointer              user_data,
                               gchar               **standard_output,
                               gchar               **standard_error,
                               gint                 *exit_status,
                               gchar               **error);

void g_spawn_close_pid (int pid);


#endif

#if !GLIB_CHECK_VERSION(2, 0, 0)
typedef gint (*GCompareDataFunc) (gconstpointer  a,
                                  gconstpointer  b,
                                  gpointer       user_data);

void g_ptr_array_foreach (GPtrArray *array,
                          GFunc      func,
                          gpointer   user_data);
void g_ptr_array_sort (GPtrArray    *array,
                       GCompareFunc  compare_func);
void g_array_sort (GArray       *array,
                   GCompareFunc  compare_func);
void g_array_sort_with_data (GArray           *array,
                             GCompareDataFunc  compare_func,
                             gpointer          user_data);
#endif


#endif
