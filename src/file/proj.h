/************************************************************************
                           proj.h

30jul02 - repmarker (fred)
************************************************************************/
#define DEFAULTFLAG 429

/* FIL_BUFFER_SIZE+DIR_BUFFER_SIZE are defined in w/wh and = MAXPATHLEN
   which is defined in /usr/include/sys/param.h as 1023 on Dec Alpha */

struct project { /* CAS 26 oct */
   BOOL  automsg;
   char uncfile[FIL_BUFFER_SIZE+DIR_BUFFER_SIZE];
   char filterfile[FIL_BUFFER_SIZE+DIR_BUFFER_SIZE];
   char mapname[50]; /* read from framework file and used for pace/gull */
   char anchor_label[50]; /* read from framework file*/ 
   char label_abbrev[50]; /* read from framework file; abbreviation of anchor_label*/
   BOOL generic_grpnames;
   int  avgbandsize; /* avg size of band, entered 1st by user */
   float  genomesize;  /* lenght of sequence, entered 1st by user */
   int  avginsertsize; /* avg insert size, entered 1st by user */
   int  maxband;
   BOOL variable;
   int eq2;
   
   char msg[4][100]; /* gaurav 01/29/04 put back; used by parallel */
   
   char msg2[4][MSGSIZE*2]; /* cari 12/12/03 removed msg[4][100] */
   int MinClip, MaxClip;
   int gel_len;      /*fred 9/30/02 -- make GEL_LEN a variable (set in Configure)*/
   int default_page_size;  /* fred 7/24/03*/
  
   int tot_bands;  /* WMN 9/20/06    added to allow comp of avg bands/clone */
    
};

#define MAX_CTG_FW 201
struct framework {
  int index;
  char anchor_bin[ANCHOR_BIN_SZ+1];
  float pos;
  char name[MARKER_SZ+1];
  int ctg[MAX_CTG_FW];
  int cnt[MAX_CTG_FW];
};

#ifdef MAIN
struct project Proj;
struct framework *FWmarks;
int FWcnt;
#else
extern struct project Proj;
extern struct framework *FWmarks;
extern int FWcnt;
#endif
