// fpc project %s<filename>
// %s<date>
// %s<time>
// Contigs %d  Clones %d  Markers %d  Bands %d
// Framework %s  Label %s Abbrev %s Genome %f AvgBand %d  AvgInsert %d
// Genome %20.0f  AvgBand %d  AvgInsert %d
// Unc %s
// Vector %s
// Configure %d[  Variable] [EQ2|EQ3] Tol %d Cut %.e Apx %3.3f \
   Gel %d Min %d End %d Kill %d Bad %d Best %d Log %d Std %d
// Clip(%d %d) MinMax(%d %d) [AutoRemark] [UseSeq]

Clone|YAC|BAC|PAC|Cosmid|Fosmid : "%s<clonename>"
Map "ctg%d<contignumber>" Ends Left %d<start>.00
Map "ctg%d<contignumber>" Ends Right %d<end>.00
Fp_number "%s<fpchar>"
Gel_number %sgelname
Bands %d<offset> %d<numberbands>
(Approximate|Exact|Pesudo)_match_to_cosmid %s<clonename>
Remark "%s<remark>"
Shotgun %s<seqtype> %s<seqstat>
Positive_(YAC|End|Probe|Locus|Clone|Fosmid|Cosmid|BAC|PAC|cDNA|\
          PCR|SNP|eMRK|eBAC|TC|SSR|RFLP|OVERGO|REP|probe|locus)\
	  [_weak] "%s<markername>" (New|CpM|null)
Fpc_remark "%s<remark>"
Creation_date %d<yearsince1900> %d<month> %d<day> %d<hour> %d<minute>
Modified_date %d %d %d %d %d  <ditto>

Markerdata

Marker_<type> : "%s<markername>"
Anchor_bin "%s<anchor bin id>"
Anchor_pos %7.1f [F | P]
Creation_date %d %d %d %d %d
Modified_date %d %d %d %d %d

Contigdata %d<maxContig>

Ctg%d %d/%d/%d %d:%d %s%c %d # %s
    1  2  3  4  5  6  7 8  9   10
1         - contig number
2/3/4 5:6 - day/month/year hour:minute
7         - contig status (Ok, NoCB, Avoid, NoAce, Dead)
8         - IBC
9         - Q's
10        - remark

new version of the remark field is to remove everything after the #
and have new lines of the form:
Chr_remark "Chr [2 Fw2]"
User_remark "other stuff"
Trace_remark "whatever"
