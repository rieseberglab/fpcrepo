/**************************************************************
                    gsc.c  

The merge file should have a .gsc suffix.
The following demo works on the Humanmap:
(a wrong contig number will be ignored)

// GSC input

Clone : "bA5N14"
Map "ctg3" Ends Left 5.000
Map "ctg3" Ends Right 49.000
Fpc_remark "GSC positioned"

Clone : "bA9O4"
Map "ctg3" Ends Left 3.000
Map "ctg3" Ends Right 39.000
Fpc_remark "GSC positioned"
Remark "Serious remark"
**************************************************************/
#include <stdio.h>
#include <../fpp/fpshr.h>

extern int update;  
extern int dequote();
extern void getcomment();
extern void settime();
extern FILE *graphQueryOpen();
extern int Zbatch_flag;

BOOL fppFind(Array a, char *s, int *ip, BOOL (* order)());
BOOL fppInsert(Array a, char *s, int *index, BOOL (*order)());
BOOL cloneOrder(void *s, void *b);
BOOL markerOrder(void *s, void *b);

/*******************************************************************
                     DEF: mergeGSC
********************************************************************/
void mergeGSChandler(FILE *infile)
{
  char *pos, line[256], tmp[256], name[256];
  register int i;
  CLONE *clp=NULL;
  struct remark *last, *prev;   /*fred 1/7/02 -- added prev stuff*/
  int index, ctg;
  int cnt=0, ctype;
  int dupFlag;
           
  if (infile==NULL) return;
  
  /**********************************************************************
           Parse clones
  *********************************************************************/

  while(fgets(line,255,infile) != NULL){
      if (line[0] == '/') { /* read and print comments at beginning */
          if (!Zbatch_flag) printf("%s",line);
          continue;
      }
      for (i=0; i<255 && (line[i] == ' ' || line[i] == '\t'); i++);
      if(line[i]=='\n' || i==255) {
         clp=NULL;
         continue;
      }

      ctype = -1;
      for (i=0; i<clonetypenum && ctype==-1; i++)
         if(strncmp(clonetype[i],line,strlen(clonetype[i]))==0) ctype = i;

      if(ctype!=-1){
	  if (dequote(line, name, CLONE_SZ-1)==0){                
	    printf("ERROR string *%s* no DATA\n",line);
            clp=NULL;
	  }
	  if(fppFind(acedata,name,&index,cloneOrder)){
	    clp = arrp(acedata, index, CLONE);    
	  }
	  else{
            printf("Clone %s does not exist\n",name);
            clp=NULL;
          } 
          continue;
      }
      if (clp==NULL) continue; /* skip over rest of record */

      if(strncmp("Map",line,3)==0)
      { 
	 if (dequote(line, name, 200)==0) continue;
	 sscanf(name,"ctg%d",&ctg);
         if (ctg != clp->ctg) 
            printf("Clone %s different contigs %d and %d\n",
                  name, ctg, clp->ctg);
	 pos = strstr(line,"Left");
	 if(pos !=NULL) sscanf(pos,"Left %d.00",&clp->x);
	 else{
	   pos = strstr(line,"Right");
	   if(pos !=NULL) sscanf(pos,"Right %d.00",&clp->y);
	 }
         continue;
      }
      if (strncmp("Fpc_re",line,6)==0){
        getcomment(line, tmp, COMMENT_SZ-1); 
        if(clp->fp_remark==NULL){
          clp->fp_remark = (struct remark *)messalloc((sizeof(struct remark)));
          last = clp->fp_remark;
          strcpy(last->message, tmp);
          last->next = NULL;
          settime(&clp->modified_date);
          cnt++;
        }
        else{
	  dupFlag = 0;
          for(last=clp->fp_remark;(last!=NULL)&&(!dupFlag);last=last->next){
              if (strcmp(last->message,tmp)==0) dupFlag = 1;
              prev=last;
          }
	  if (!dupFlag) {
            prev->next  = (struct remark *)messalloc((sizeof(struct remark)));
            last = prev->next;
            strcpy(last->message, tmp);
            last->next = NULL;
            settime(&clp->modified_date);
            cnt++;
	  }
        }
      }
      if (strncmp("Remark",line,6)==0){
        getcomment(line, tmp, COMMENT_SZ-1);
        if(clp->remark==NULL){
          clp->remark = (struct remark *)messalloc((sizeof(struct remark)));
          last = clp->remark;
          strcpy(last->message, tmp);
          last->next = NULL;
          settime(&clp->modified_date);
          cnt++;
        }
        else{
	  dupFlag = 0;
          for(last=clp->remark;(last!=NULL) && (!dupFlag);last=last->next){
              if (strcmp(last->message,tmp)==0) dupFlag = 1;
              prev=last;
          }
	  if (!dupFlag) {
            prev->next  = (struct remark *)messalloc((sizeof(struct remark)));
            last = prev->next;
            strcpy(last->message, tmp);
            last->next = NULL;
            settime(&clp->modified_date);
            cnt++;
	  }
        }
      }
  }
  printf("Merge file: Update %d clones\n", cnt);
  update=1;
}

/*******************************************************************
                     DEF: mergeMremHandler
fred 12/18/02 -- Merge marker remarks from file.
format:   Marker : "<marker_name>"
          Remark "<comment>"
********************************************************************/
void mergeMremHandler(FILE *infile)
{
  char line[256], tmp[256], name[256];
  register int i;
  MARKER *mrkPtr=NULL;
  struct remark *last, *prev;
  int index;
  int cnt=0, mtype;
  int dupFlag;
           
  if (infile==NULL) return;
  
  /********************************************************************
           Parse clones
  **********************************************************************/

  while(fgets(line,255,infile) != NULL){
      if (line[0] == '/') { /* read and print comments at beginning */
          if (!Zbatch_flag) printf("%s",line);
          continue;
      }
      for (i=0; i<255 && (line[i] == ' ' || line[i] == '\t'); i++);
      if(line[i]=='\n' || i==255) {
         mrkPtr=NULL;
         continue;
      }

      mtype = -1;
      if(strncmp("Marker",line,6)==0) mtype = 1;

      if(mtype!=-1){
	  if (dequote(line, name, MARKER_SZ-1)==0){                
	    printf("ERROR string *%s* no DATA\n",line);
            mrkPtr=NULL;
	  }
	  if(fppFind(markerdata,name,&index,markerOrder)){
	    mrkPtr = arrp(markerdata, index, MARKER);    
	  }
	  else{
            printf("Marker %s does not exist\n",name);
            mrkPtr=NULL;
          } 
          continue;
      }
      if (mrkPtr==NULL) continue; /* skip over rest of record */

      if (strncmp("Remark",line,6)==0){
        getcomment(line, tmp, COMMENT_SZ-1);
        if(mrkPtr->remark==NULL){
          mrkPtr->remark = (struct remark *)messalloc((sizeof(struct remark)));
          last = mrkPtr->remark;
          strcpy(last->message, tmp);
          last->next = NULL;
          settime(&mrkPtr->modified_date);
          cnt++;
        }
        else{
	  dupFlag = 0;
          for (last=mrkPtr->remark;(last!=NULL) && (!dupFlag);last=last->next){
              if (strcmp(last->message,tmp)==0) dupFlag = 1;
              prev=last;
          }
	  if (!dupFlag) {
            prev->next  = (struct remark *)messalloc((sizeof(struct remark)));
            last = prev->next;
            strcpy(last->message, tmp);
            last->next = NULL;
            settime(&mrkPtr->modified_date);
            cnt++;
	  }
        }
      }
  }
  printf("Merge file: Update %d Markers\n", cnt);
  update=1;
}

void mergeGSC()
{
  FILE *infile;
           
  infile = graphQueryOpen(mergedir, mergefile, "ace","r","Choose merge file");
  if (infile==NULL) return;

  mergeGSChandler(infile);

  fclose(infile);
}

void mergeMrem()
{
  FILE *infile;
           
  infile = graphQueryOpen(mergedir, mergefile, "ace","r","Choose merge file");
  if (infile==NULL) return;

  mergeMremHandler(infile);

  fclose(infile);
}

/*************************************************************
                       DEF: mergeCtgUserRem
cari 10aug04
Format:
CtgN  "remark"
*************************************************************/
void replaceCtgUserRem()
{
FILE *infile;
char line[1024], remark[CTGMSG_SZ];
int i,cnt=0, ctg;

  infile = graphQueryOpen(mergedir, mergefile, "ace","r","Choose merge file");
  if (infile==NULL) return;
           
  for (i=0; i<=max_contig; i++) contigs[i].user_msg[0] = '\0';
  while(fgets(line,1024,infile) != NULL){
      if (strncmp(line,"Ctg",3)!=0) continue;
      if (sscanf(line,"Ctg%d",&ctg)!=1) continue;
      if (contigs[ctg].count==0) {
         printf("Ctg%d has zero clones. Ignoring...\n",ctg);
         continue;
      }
      if (dequote(line, remark, CTGMSG_SZ-1)==0){                
	 printf("ERROR string *%s* no remark\n",line);
         continue;
      }
      cnt++;
      strcpy(contigs[ctg].user_msg,remark);
  }
  fclose(infile);
  printf("Add %d contig remarks\n",cnt);
}



void replaceCtgChrRem()
{
FILE *infile;
char line[1024], remark[CTGMSG_SZ];
int i,cnt=0, ctg;
float pos;
  infile = graphQueryOpen(mergedir, mergefile, "","r","Choose merge file");
  if (infile==NULL) return;
           
  for (i=0; i<=max_contig; i++) contigs[i].chr_msg[0] = '\0';
  if (fgets(line,1024,infile) == NULL) return;
  if (1 != sscanf(line,"Label %s",Proj.anchor_label)) return;
  if (fgets(line,1024,infile) == NULL) return;
  if (1 != sscanf(line,"Abbrev %s",Proj.label_abbrev)) return;

  while(fgets(line,1024,infile) != NULL)
  {
      if (sscanf(line,"Ctg%d %s %f",&ctg,remark,&pos)!=3) continue;
      cnt++;
      strcpy(contigs[ctg].chr_msg,remark);
      contigs[ctg].chr_pos = pos;
  }
  fclose(infile);
  printf("Add %d contig chromosome remarks\n",cnt);
}
