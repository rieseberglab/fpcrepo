/*  Last edited: Dec 23 12:25 1996 (il) */
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <ctype.h>
#include <limits.h>
#include "clam.h"
#include "dirent.h"
#ifdef ALPHA
#include <sex.h>
#endif

extern int Zget_tol();
/* The filtering is now a little more complex as there is now the abillity to filter
on the second letter of the clone. The second letter of the clone will be considered
as a char for determining sub types.
subtypes
* has subtypes but this is not one of them, see exclude to see which are excluded.
0 has no subtypes so use this for the whole type.
anything else is the second letter of the clone.

exclude holds the characters to be (clones second letter) ignored as they are dealt
with later.
*/
 
void read_filter(){
  FILE *in, *sopen();
  int  i=0;
  char buf[500],chtype[20];
  char bac[5],pac[5],yac[5],cos[5],fos[5];
  int nbac=0,npac=0,nyac=0,ncos=0,nfos=0;
  FILTER filter,*filt;
  int value,tol,subtype[NUM_TYPES];

  for(i=0;i<NUM_TYPES;i++)
    subtype[i]=0;

  if(filterdata)
      arrayDestroy(filterdata);
  filterdata = 0;
  if (Proj.filterfile[0] == '\0') return;
  
  in = fopen(Proj.filterfile,"r");
  if (in==NULL) {
    sprintf(buf,"*** Cannot open filter file %s", Proj.filterfile);
    displaymess(buf);
    return;
  }
  filterdata = arrayReCreate(filterdata,10,FILTER);
  value = 0;
  filter.done = FALSE;
  filter.subtype = '0';
  filter.exclude[0] = '\0';
  while (fgets(buf,500,in)!=0) {
    if((buf[0]< '0' || buf[0] > '9') && (buf[0] != '*')){
      sscanf(buf,"%s",chtype);
      filter.subtype = '0';
      if(buf[0]=='B' || buf[0]=='b')
	filter.type = TYPEBAC;
      else if(buf[0]=='P' || buf[0]=='p')
	filter.type = TYPEPAC;
      else if(buf[0]=='Y' || buf[0]=='y')
	filter.type = TYPEYAC;
      else if(buf[0]=='C' || buf[0]=='c')
	filter.type = TYPECOSMID;
      else if(buf[0]=='F' || buf[0]=='f')
	filter.type = TYPEFOSMID;
      else
	filter.type = TYPECLONE;
    }
    else if(buf[0] == '*'){
      if(!subtype[filter.type])
	 subtype[filter.type]++;
      filter.subtype = buf[1];
      if(filter.type == TYPEBAC)
	bac[nbac++] = buf[1];
      else if(filter.type == TYPEPAC)
	pac[npac++] = buf[1];
      else if (filter.type == TYPEYAC)
	yac[nyac++] = buf[1];
      else if(filter.type == TYPECOSMID)
	cos[ncos++] = buf[1];
      else if(filter.type == TYPEFOSMID)
	fos[nfos++] = buf[1];
    }
    else{
      sscanf(buf,"%d",&value);
      tol = Zgtol(value);
      filter.min=value-tol;
      filter.max=value+tol;
      array(filterdata,i++,FILTER) = filter;
   }
  }
  if(!value){
    arrayDestroy(filterdata);
    filterdata = 0;
  }
  else if (nbac || npac ||nyac || ncos ||nfos) { 
    for (i=0; i<arrayMax(filterdata); i++){
      filt = arrp(filterdata,i,FILTER);
      /* fred 8/13/03 -- don't know if this is the right fix,
       * but the existing was definitely wrong.
      if(subtype[filt->subtype]){ */
      if(subtype[filt->type]){
  
	if(filt->subtype == '0'){
	  filt->subtype = '*';
	  if(filt->type == TYPEBAC)
	    strcpy(filt->exclude,bac);
	  else if(filt->type == TYPEPAC)
	    strcpy(filt->exclude,pac);
	  else if(filt->type == TYPEYAC)
	    strcpy(filt->exclude,yac);
	  else if(filt->type == TYPECOSMID)
	    strcpy(filt->exclude,cos);
	  else if(filt->type == TYPEFOSMID)
	    strcpy(filt->exclude,fos);
	}
      }
    }
  }
  /* for DEBUGING
  printf("bac= %s, pac= %s, yac= %s, fosmid = %s, cosmid = %s\n",bac,pac,yac,fos,cos);

    for (i=0; i<arrayMax(filterdata); i++){
      filt = arrp(filterdata,i,FILTER);
      printf("%d  type = %d   subtype = %c  value= %d  exclude= %s\n",i,filt->type,filt->subtype,filt->min,filt->exclude);
    }
    */
}
