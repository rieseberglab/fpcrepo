#include "fpshr.h"
#include <fcntl.h>
#include <float.h>
#ifdef ALPHA
#include "sex.h"
#endif
#define SEEK_SET 0
#define DELETED 1
#define FPMAX 500 /* number of fp map that can be displayed (fp-> fingerprint) */
extern BOOL fpcreadflag;

void getfppdata(), displaymess();
int readfpc(), Zbury_type();
int dequote();
void SetCtgForMatchingClones3();
void sortoutmarkersman(void);
void setmarkerposman(void);
void settime(struct mytime *mytime);
void markercorrect(void);
void markersinctgcount(void);
void getfppdata2(void);
void contigsAlloc(int ctg);
void sortoutmarkersman2(void);
void addchanges(void);

BOOL fppFind(Array a, char *s, int *ip, BOOL (* order)());
BOOL fppInsert(Array a, char *s, int *index, BOOL (*order)());
BOOL cloneOrder(void *s, void *b);
BOOL markerOrder(void *s, void *b);
extern  BOOL  dataloaded;
extern BOOL merging,firstread;
extern int cloneindex[FPMAX];
extern char timestamp[255];

/*******************************************************************
                     DEF: readsecond
********************************************************************/
int readsecond()
{
  Array acecopy;
  int val,max_contig_copy,i;

  acecopy = acedata;
  max_contig_copy = max_contig;
  acedata = 0;
  projmerge = TRUE;
  val = readfpc(fpsecond);
  if(val != 1)
    return(val);
  projmerge = FALSE;
  max_contig2 = max_contig;
  max_contig = max_contig_copy;
  acedatasecond = acedata;
  acedata = acecopy;

  addchanges();

  /***************************/

  SetCtgForMatchingClones3();
  for(i=0;i<FPMAX;i++)
    cloneindex[i]= -1;
 
  projmerge = TRUE;
  getfppdata();
  projmerge = FALSE;

  dataloaded = TRUE;
  return(1); /* Successful read therefore return 1 */
}

void addchanges()
{
  int i;
  CLONE *clone;
  struct fpdata *fp;

  for(i=0;i<arrayMax(acedatasecond);i++){
    clone = arrp(acedatasecond,i,CLONE);
    if(clone->ctg != 0){
      clone->ctg += max_contig;
      sprintf(clone->chctg,"ctg%d",clone->ctg);
    }
    fp = clone->fp;
    while(fp != NULL){
      if(fp->b2 != 0)
	fp->b1 +=arrayMax(bands);
      fp=fp->next;
    }
  }
}

/**************************************************************
                   DEF: SetCtgForMatchingClones3
**************************************************************/
void SetCtgForMatchingClones3()
/* This routine searches searches for the approxiamate and exact matches.*/
/* The clones that are these, will locate there "Mothers" and load their data */
{
  int i,max,index,temp;
  CLONE *clone, *parent;

  max= arrayMax(acedatasecond); 
  contigsAlloc(max_contig2);
  
  for(i=0;i<max;i++){

    clone =  arrp(acedatasecond, i, CLONE);
    clone->next = -1;
    if(clone->clone[0] == '!') continue;


                    /** find parent index for buried clone **/
    if(clone->match[0] != ' '){
      if(!fppFind(acedatasecond,clone->match,&index, cloneOrder)) {
	printf("FPC ERROR could not find clone *%s* parent = %s\n",
		clone->match,clone->clone);
	index = -1;
      }

      if(index!=-1){  
	parent = arrp(acedatasecond,index,CLONE);
	clone->parent = index;
	temp = Zbury_type(i,index);
	if(temp != PSEUDO)
	  parent->mattype = PARENT;
	else if(!(parent->mattype & PARENT))
	  parent->mattype = PSPARENT;
	/* sanity checks */
	if(clone->ctg != parent->ctg){
	  printf("FPC ERROR: Buried %s not in same ctg as parent %s\n",
		 clone->clone, parent->clone);
	  clone->ctg = parent->ctg;
	  sprintf(clone->chctg,"ctg%d",clone->ctg);
	  displaymess("Buried's ctg changed to that of parent");
	  printf("FPC FIX: Buried's ctg changed to that of parent\n");
	}
	if(parent->match[0] != ' ')
	  printf("FPC ERROR clone %s has parent %s who has parent %s\n",
		 parent->clone ,clone->clone, 
		 arrp(acedatasecond,clone->parent, CLONE)->clone);
	
       }
    }
  }
}
