/*  Last edited: Feb 15 17:54 1995 (il) */
#include "fpp.h"

int findConIndex(int ctg)
/* This routine returns the index for the structure contigdata (contigs) */
/* corrwsponding to the contig ctg or -1 if not found*/
{
  int i;

  for(i=1;i<MAXCONTIG;i++){
    if(contigs[i].ctg == ctg) /* if found then return the index */
      return(i);
    else if(contigs[i].ctg == 0)
      return(0);
  }
  printf("DANGER too many contigs. Need to set MAXCONTIG higher and recompile\n");
  for(i=0;i<10;i++){
    printf("i= %d, ctg = %d, start = %d, fin = %d, count = %d\n",i,
	   contigs[i].ctg, contigs[i].start, contigs[i].last,contigs[i].count);
  }
  exit(1);
  return(-1);
}
