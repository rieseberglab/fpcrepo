/*******************************************************************
 *                        cEdit_callbacks.c
 *                        by Scott J. Pearson, April 06, 2004
 * This window contains the callback methods from the clone edit
 * window. They all begin with "on_", continue with the name of the
 * widget (e.g., Window, RenameButton), and end with the name of the
 * type of item (e.g., clicked, delete). Sometimes, the name of a window
 * will appear second (e.g., Rename, for the rename clone window).
 */

#include <stdio.h>
#include <gtk/gtk.h>
#include <strings.h>
#include <stdlib.h>
#include <ctype.h>
#include "fpp.h"
#include "float.h"
#include "gtkhelp.h"
#include "cEdit.h"

#define BORDER_WIDTH 7

void on_Window_delete();
void on_RenameButton_clicked(GtkWidget* CI);
void on_Rename_Accept_clicked (GtkWidget* CI, GtkWidget* CE);
void on_Dialog_delete(GtkWidget* CI);
void on_CancelButton_clicked(GtkWidget* CI, CLONE* cl);
void on_ShotgunStatusMenuItem_activate(GtkWidget* CI, GtkWidget* OOM);
void CreateDialog();
void on_AcceptButton_clicked(GtkWidget* CI, CLONE* cl);
void on_RejectButton_clicked(GtkWidget* CI);
void on_HelpButton_clicked(GtkWidget* CI);
void on_MarkersPickToggle_toggled(GtkWidget* CI, GtkWidget* Txt);
void on_BuriedClonesPickToggle_toggled(GtkWidget* CI, GtkWidget* Txt);

extern int clone_compare (const void* a, const void* b);

extern int output_data(CLONE* cl);
extern int findParentIndex(char* clonename);
extern void safe_strncpy(char* dest, char* src, size_t num);
extern int is_empty(char* str);
extern void setmarkerposctg();
extern void    recalccontigs();
extern void    markercorrect();
extern void    markersetzero();
extern void    gelquit();
extern void        fpquit();
extern void graphOut(char* str);

extern void show_help();

extern void deleteclone();
extern void refreshlist();
extern void updateproj();

extern void refresh_gtk_ctgdisplay();   
extern void gtk_ctgdisplay(int ctg);  
extern void change_clone_name(char* chaddname); 
extern void recalcindexes();

extern CLONE* clone_edit_clone;

void on_Window_delete(GtkWidget* ClickedItem)
{
   /* active -= EDITCLONE; WN 04/04 in destroy now */
   if (graphActivate(gcloneedit)) graphDestroy();
   gtk_widget_destroy(mw.Window);
   mw.Window = NULL;
   clone_edit_window = NULL;
   gtk_main_quit();
   displayclone(localmisc.cloneindex);
}

void on_RenameButton_clicked(GtkWidget* ClickedItem)
{
   GtkWidget* Box;
   GtkWidget* NewName;
   GtkWidget* NewNameLabel;
   GtkWidget* AcceptButton;
   GtkWidget* RejectButton;
   GtkWidget* EmptyLine;
   int RowNum;

   if ((dialog != NULL) && (GTK_IS_WIDGET(dialog)))
   {
      gtk_widget_destroy(dialog);   /* iff exists */
      dialog = NULL;
   }

   dialog = gtk_window_new(GTK_WINDOW_TOPLEVEL);
   Box = gtk_table_new(4, 3, FALSE);
   NewNameLabel = gtk_label_new("New Name");
   NewName = gtk_entry_new();
   AcceptButton = gtk_button_new_with_label("Accept");
   RejectButton = gtk_button_new_with_label("Reject");
   EmptyLine = gtk_label_new(" ");

   RowNum = 0;
   gtk_table_attach(GTK_TABLE(Box), NewNameLabel, 0, 2, RowNum, RowNum + 1,
                    GTK_FILL, GTK_FILL, 0, 0);
   gtk_table_attach(GTK_TABLE(Box), NewName, 3, 4, RowNum, RowNum + 1,
                    GTK_FILL, GTK_FILL, 0, 0);
   RowNum++;
   gtk_table_attach(GTK_TABLE(Box), EmptyLine, 0, 4, RowNum, RowNum + 1,
                    GTK_FILL, GTK_FILL, 0, 0);
   RowNum++;
   gtk_table_attach(GTK_TABLE(Box), AcceptButton, 0, 1, RowNum, RowNum + 1,
                    GTK_FILL, GTK_FILL, 0, 0);
   gtk_table_attach(GTK_TABLE(Box), RejectButton, 1, 2, RowNum, RowNum + 1,
                    GTK_FILL, GTK_FILL, 0, 0);

   gtk_container_add(GTK_CONTAINER(dialog), Box);
   gtk_window_set_title(GTK_WINDOW(dialog), "Rename Clone");
   gtk_container_set_border_width(GTK_CONTAINER(dialog), BORDER_WIDTH);

   gtk_signal_connect(GTK_OBJECT(dialog), "delete_event",
                      GTK_SIGNAL_FUNC(on_Dialog_delete), NULL);
   gtk_signal_connect(GTK_OBJECT(AcceptButton), "clicked",
                      GTK_SIGNAL_FUNC(on_Rename_Accept_clicked), NewName);
   gtk_signal_connect(GTK_OBJECT(RejectButton), "clicked",
                      GTK_SIGNAL_FUNC(on_Dialog_delete), NULL);

   gtk_widget_show_all(dialog);
   gtk_main();
}

void on_Dialog_delete(GtkWidget* ClickedItem)
{
   gtk_widget_destroy(dialog);
   dialog = NULL;
   gtk_main_quit();
}

void on_Rename_Accept_clicked (GtkWidget* ClickedItem, GtkWidget* CloneEntry)
{
    GtkWidget* Item;
    gchar* gstr_name;
    char* ptr;
    int idx;

    Item = GTK_WIDGET(CloneEntry);
    gstr_name = (gchar*)gtk_entry_get_text(GTK_ENTRY(Item));

    if (strcmp(clone_edit_clone->clone, gstr_name))
    {
        ptr = gstr_name;
        while (*ptr)
        {
            if (isspace(*ptr))
            {
                CreateDialog("The clone name can not contain spaces.");
                return;   
            }
            ptr++;
        }
        if (strlen(gstr_name) >= CLONE_SZ)
        {
            CreateDialog("The clone name entered is too long.");
            return;
        }
        if (fppFind(acedata, gstr_name, &idx, cloneOrder))
        {
            CreateDialog("A clone by that name already exists.");
            return;  
        }
    }
    else /* the name is the same */
    {
        gtk_widget_destroy(dialog);
        return;
    }

    /* they are changing the name */

    update = 1;

    /* on FIRST name change, we store the original name as fpname */

    if (is_empty(clone_edit_clone->fp->fpchar ))
    {
        safe_strncpy(clone_edit_clone->fp->fpchar,clone_edit_clone->clone, CLONE_SZ);
    }

    change_clone_name(gstr_name);

    if(graphActivate(g2)) 
    {
       graphDestroy();
    }
    if (currentctg > 0)
    {
        gtk_ctgdisplay(currentctg);
    }

    gtk_widget_destroy(dialog);
    dialog = NULL;
    gtk_widget_destroy(mw.Window);
    mw.Window = NULL;
    gtk_main_quit();
}

void on_CancelButton_clicked(GtkWidget* ClickedItem, CLONE* clone)
{
   GtkWidget* CloneTypeMenuItem;
   char str[100];

   sprintf(str, "Are you sure you want to cancel clone %s?", clone->clone);
   if (!messQuery(str))
   {
      printf("Clone %s not cancelled.\n", clone->clone);
      return;
   }

   CloneTypeMenuItem = gtk_menu_get_active(GTK_MENU(mw.CloneTypeMenu));
   if (CloneTypeMenuItem == mw.CloneTypeMIAry[5])
   {
      return;
   }

   output_data(clone);
   /* active -= EDITCLONE; WN 04/04 in destroy now */
   deleteclone();

   /* clean up Pick toggles */
   markersTextview = NULL;
   clonesTextview = NULL;
   editmarkerstatus = 0;
   editclonestatus = 0;

   if (graphActivate(gcloneedit)) graphDestroy();
   if(graphActivate(g2)) 
   {
      graphDestroy();
   }
   gtk_widget_destroy(mw.Window);
   mw.Window = NULL;
   clone_edit_window = NULL;
   gtk_main_quit();
}

void on_AcceptButton_clicked(GtkWidget* ClickedItem, CLONE* clone)
{

   /* clean up Pick toggles */
   markersTextview = NULL;
   clonesTextview = NULL;
   editmarkerstatus = 0;
   editclonestatus = 0;

   /* active -= EDITCLONE; WN 04/04 in destroy now */
   if(!output_data(clone))
   {
      return;
   }

   refreshlist();
   updateproj();
   if (graphActivate(gcloneedit)) graphDestroy();
   refresh_gtk_ctgdisplay();   // if alive
   gtk_widget_destroy(mw.Window);
   mw.Window = NULL;
   clone_edit_window = NULL;
   gtk_main_quit();
   displayclone(localmisc.cloneindex);
}

void CreateDialog_old(char* Str)
{
   GtkWidget* Label;
   GtkWidget* Ok_Button;
   GtkWidget* Box;
   GtkWidget* Empty;

   if ((dialog != NULL) && (GTK_IS_WIDGET(dialog)))
   {
      gtk_widget_destroy(dialog);   /* iff exists */
      dialog = NULL;
   }

   dialog = gtk_window_new(GTK_WINDOW_TOPLEVEL);
   Box = gtk_table_new(1, 3, FALSE);
   Label = gtk_label_new(Str);
   Empty = gtk_label_new(" ");
   Ok_Button = gtk_button_new_with_label("Ok");
   // gtk_widget_set_usize(Ok_Button, 15, 25);
   /* does not shrink horizontally, though it should in theory;
    * cannot find out why; web does not help; thus, comment out */

   gtk_table_attach(GTK_TABLE(Box), Label, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);
   gtk_table_attach(GTK_TABLE(Box), Empty, 0, 1, 1, 2, GTK_FILL, GTK_FILL, 0, 0);
   gtk_table_attach(GTK_TABLE(Box), Ok_Button, 0, 1, 2, 3, GTK_FILL, GTK_FILL, 0, 0);

   gtk_container_add(GTK_CONTAINER(dialog), Box);
   gtk_window_set_title(GTK_WINDOW(dialog), "Warning!");
   gtk_container_set_border_width(GTK_CONTAINER(dialog), BORDER_WIDTH);

   gtk_signal_connect(GTK_OBJECT(dialog), "delete_event",
                      GTK_SIGNAL_FUNC(on_Dialog_delete), NULL);
   gtk_signal_connect(GTK_OBJECT(Ok_Button), "clicked",
                      GTK_SIGNAL_FUNC(on_Dialog_delete), NULL);

   gtk_widget_show_all(dialog);
   gtk_main();
}

void CreateDialog(char* str)
{
    printf("%s\n",str);
}

void on_HelpButton_clicked(GtkWidget* ClickedItem)
{
   show_help("Edit Clone Record", "cEditHelp");
}

/* makes no changes to clone */
void on_RejectButton_clicked(GtkWidget* ClickedItem)
{
   /* active -= EDITCLONE; WN 04/04 in destroy now */
   if (graphActivate(gcloneedit)) graphDestroy();
   gtk_widget_destroy(mw.Window);
   mw.Window = NULL;
   gtk_main_quit();
   displayclone(localmisc.cloneindex);
}

/* Information is verified in output_data */
void on_ShotgunStatusMenuItem_activate(GtkWidget* ClickedItem,
                                       GtkWidget* OtherOptMenu)
{
   GtkWidget* OtherMenu;
   GtkWidget* OtherMenuItem;

   OtherMenu = gtk_option_menu_get_menu(GTK_OPTION_MENU(OtherOptMenu));
   OtherMenuItem = gtk_menu_get_active(GTK_MENU(OtherMenu));
   if (((ClickedItem == mw.StatusMIAry[0])
        && (OtherMenuItem != mw.ShotgunTypeMIAry[0]))
       || ((ClickedItem == mw.ShotgunTypeMIAry[0])
           && (OtherMenuItem != mw.StatusMIAry[0])))
   {
      /* Clicked is NONE => change other to NONE */
      gtk_widget_hide(OtherMenu);
      gtk_option_menu_set_history(GTK_OPTION_MENU(OtherOptMenu), 0);
      gtk_widget_show(OtherMenu);
   }
   else if (((ClickedItem != mw.ShotgunTypeMIAry[0])
             && (OtherMenuItem == mw.StatusMIAry[0]))
            || ((OtherMenuItem == mw.ShotgunTypeMIAry[0])
                && (ClickedItem != mw.StatusMIAry[0])))
   {
      /* Clicked is not NONE and other is NONE => change other to not NONE
       * (1 chosen arbitrarily) */
      gtk_widget_hide(OtherMenu);
      gtk_option_menu_set_history(GTK_OPTION_MENU(OtherOptMenu), 1);
      gtk_widget_show(OtherMenu);
   }
}

void on_MarkersPickToggle_toggled(GtkWidget* CI, GtkWidget* Txt)
{
   markersTextview = mw.MarkersTextView;
   editclonestatus = !editclonestatus;
}

void on_BuriedClonesPickToggle_toggled(GtkWidget* CI, GtkWidget* Txt)
{
   clonesTextview = mw.BuriedClonesTextView;
   editmarkerstatus = !editmarkerstatus;
}
/* WN 04/04 */
void
destroy_edit_clone_callback(GtkWidget *widget, gpointer data){
  active -= EDITCLONE;
  editclonestatus=0;
  clone_edit_window = NULL;
}
