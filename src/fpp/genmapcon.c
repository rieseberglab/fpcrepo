/*********************************************************************
                       genmapcon.c
written by IL
edited by CAS
EDIT: change zoom from *2.0 to +0.5
********************************************************************/ 
#include "fpp.h"
void highlightmarker(BOOL high);

#define ZINC 0.25

/***************************************************************
                    DEF: fpwhole
Whole button on fingerprint display
*************************************************************/
void fpwhole()
/* This routine resets the zoom and shiftfor the fp map to the original */
/* values and redisplays the graph */
{
  if(FPZOOM != 1.0){
    FPZOOM = 1.0;
    FPYSHIFT = 0;
    drawfpdata(0);
  }
  FPYSHIFT = 0;
}

/***************************************************************
                    DEF: fpzoomin
zoomin button on fingerprint display
*************************************************************/
void fpzoomin()
/* This routine double the zoom for the fp map and redisplays it*/
{
  FPZOOM = FPZOOM + ZINC;
  drawfpdata(0);
}

/***************************************************************
                    DEF: fpzoomout
zoomout button on fingerprint display
*************************************************************/
void fpzoomout()
{
  if(FPZOOM > 1.0){
    FPZOOM = FPZOOM- ZINC;
    drawfpdata(0);
  }
}

/***************************************************************
                    DEF: setupshift
*************************************************************/
BOOL setypshift(float *y)
/* fingerprint scale bar has changed by shift therefore up date it */
/* always returns TRUE */
{
  
  *y = *y - FPBUTTONHEIGHT;  /* shift = shift - the top bit of the graph */
  FPYSHIFT = (*y);           /* set global shift to this */
  return(TRUE);
}
/***************************************************************
                    DEF: banddrag
fingerprint display
*************************************************************/
void banddrag(float *x, float *y, BOOL isUP)
/* This routine enables the scroll bar to be moved. Shift routine is called */
/* when the drag has finished. x is fixed and the graph is redrawn if needed */
{
  *x = 0.5;
  if(isUP)
    {
      if(setypshift(y))      /* if the scroll bar has been moved */
	drawfpdata(0);        /* then redraw fp map */
    }
}

/***************************************************************
                    DEF: movebotscroll
*************************************************************/
static BOOL movebotscroll(float *x)
/* fingerprint scale bar has changed by shift therefore up date it */
/* always returns TRUE */
{
  
  *x = (*x - FPSCALEWIDTH);  /* shift = shift - the top bit of the graph */
  fpcolstart = (int)((*x/sizepercol)+0.5);           /* set global shift to this */
  return(TRUE);
}

/***************************************************************
                    DEF: botscrolldrag
fingerprint display
*************************************************************/
void botscrolldrag(float *x, float *y, BOOL isUP)
{

  *y = fpheight;

  if(isUP)
    {
      if(movebotscroll(x))      /* if the scroll bar has been moved */
	drawfpdata(0);        /* then redraw fp map */
    }
}
