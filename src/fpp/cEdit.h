/*************************************************************************
 *                             cEdit.h
 *                             by Scott J. Pearson
 * This file contains the definitions of the structures used in the
 * clone edit documents. Also, global variables are declared.
 */

#include <gtk/gtk.h>
#include "fpp.h"
 
#define MAX_SELECTIONS 8
#define BORDER_WIDTH 7

/* one line of the line containing gels and bands */
struct GelBandsLine
{
   GtkWidget *HBox, *GelText, *GelName, *BandsText, *BandsName1, *BandsName2;
   struct GelBandsLine *next;
};


struct MainWindowInfo
{
   GtkWidget *Window, *VBox;

   GtkWidget *HBox1, *CancelButton, *RenameButton;

   GtkWidget *HBox2, *CloneText, *CloneName;
   GtkWidget *HBox3, *ContigText, *Contig;
   GtkAdjustment *SpinAdj;
   GtkWidget *CloneContigL, *CloneContigR;
   GtkWidget *HBox4, *FpNameText, *FpName;
   struct GelBandsLine *GBLine;

   GtkWidget *Separator1;

   GtkWidget *HBox7;
   GtkWidget *VSub2Box1, *RemarksText, *RemarksScroll, *RemarksTextView;
   GtkWidget *MarkersName, *MarkersText, *MarkersScroll, *MarkersTextView;
   GtkWidget *MarkersPickToggle;

   GtkWidget *VSub2Box2, *FpRemarksText, *FpRemarksScroll, *FpRemarksTextView;
   GtkWidget *BuriedClonesText, *BuriedClonesScroll, *BuriedClonesTextView;
   GtkWidget *BuriedClonesPickToggle;

   GtkWidget *Separator2;

   GtkWidget *HBox8, *ParentText, *ParentName;

   GtkWidget *Separator3;

   GtkWidget *HBox9;
   GtkWidget *VSub3Box1, *CloneTypeText, *CloneTypeMenu, *CloneTypeOptMenu;
   char CloneTypeAry[MAX_SELECTIONS][25];
   GtkWidget *CloneTypeMIAry[MAX_SELECTIONS];
   GtkWidget *VSub3Box2, *ShotgunTypeText, *ShotgunTypeMenu;
   GtkWidget *ShotgunTypeOptMenu;
   char ShotgunTypeAry[MAX_SELECTIONS][25];
   GtkWidget *ShotgunTypeMIAry[MAX_SELECTIONS];
   GtkWidget *VSub3Box3, *StatusText, *StatusMenu, *StatusOptMenu;
   char StatusAry[MAX_SELECTIONS][25];
   GtkWidget *StatusMIAry[MAX_SELECTIONS];

   GtkWidget *Separator4;

   GtkWidget *HBox10, *AcceptButton, *RejectButton, *HelpButton;
};

struct MainWindowInfo mw;
GtkWidget* clone_edit_window; 

GtkWidget* clonesTextview;
GtkWidget* markersTextview;

GtkWidget* dialog;

/* linked list for buried clones */
struct CharList
{
   char name[150];
   struct CharList* next;
};


/* stores misc info not stored in localcl */
struct MiscCloneInfo
{
   int cloneindex;
   int ContigL;     /* left and right bands of the contig */
   int ContigR;
   int NumCtgsInGenome;
   struct CharList* BuriedClones;
   int NumBuriedClones;
};

CLONE localcl;
struct MiscCloneInfo localmisc;

