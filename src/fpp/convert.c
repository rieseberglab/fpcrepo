/*  Last edited: Mar 22 12:09 1996 (il) */
#include "fpp.h"

void reverse(char s[]) 
/* This routine reverse the order ofthe characters in a string */
{
  int c,i,j;

  for(i=0, j= strlen(s) - 1; i<j; i++,j--){
    c = s[i];
    s[i] = s[j];
    s[j] = c;
  }
}

/* sness */
/* This function is bad and is now obsolete */
/* */
/*  void ftoa(float f, char s[]) */
/*  This routine converts a float to a string to two dec pts */
/*  { */
/*    int new; */
/*    float next;   */
/*    char str1[3]; */

/*    new = (int)f; */
/*    itoa(new,s); */
/*    strcat(s,"."); */
/*    next = f - (float)new; */
/*    next = next * 100; */
/*    new = (int)next; */
/*    if(new<0.0) */
/*      new =  (0-new); */
/*    itoa(new,str1); */
/*    strncat(s,str1,2); */
/*  } */

void itoa(int n, char s[])
/* This routine converts an integer into a string */
{
  int i, sign;

  if((sign = n) < 0)
    n = -n;
  i=0;
  do{
    s[i++] = n % 10 + '0';
  } while((n/= 10) >0);
  if(sign<0)
    s[i++] = '-';
  s[i] = '\0';
  reverse(s);
}

int getcolourbox(CLONE *clone)
{
  if(clone->highcol == 0){
    if(!clone->selected) return WHITE;
    else return SELECTED;
  }
  else return clone->highcol;
}

int getcolourbox2(CLONE *clone)
{
  if(clone->selected) return SELECTED;   /*efriedr 11/26/01 give selected clone color
                                           the highest priority.*/
  if(clone->highcol == 0){
    if(!clone->selected) return TRANSPARENT;
    else return SELECTED;
  }
  else
    return clone->highcol;
}

