/****************************************************
 * File: gtkprint.c
 *
 * Author: Fred Engler
 *
 * Description: Handles the printing and image saving
 * capabilities of the contig display.
 ****************************************************/

#include "gtkprint.h"

#define GETCURDIR (strcmp(dirName,"") ? dirName : ".")

extern int track_num;
extern GtkWidget *tracks[];
extern char ctg_window_title[];
extern void removespaces();

/* Types of page sizes*/
enum page_types{LETTER, LEGAL, A3, A4, A5, EXECUTIVE, FOLIO};  
enum page_types page_type = LETTER;

/* Types of file formats*/
enum save_types{JPEG, GIF, TIFF, PNG, BITMAP};
enum save_types save_type = JPEG;

static int action = 0;      /* 0 -> send to printer; 1 -> save to file*/
static int print_area = 0;  /* 0 -> region; 1 -> entire ctg*/
static int orientation = 0; /* 0 -> vertical; 1->horizontal*/
static char print_command[MAX_PRINT_COMMAND_LEN + 1] = "lpr";  /* The print command string*/
static char save_file[MAX_PRINT_COMMAND_LEN + 1];              /* The save command string*/

GtkWidget *print_window=NULL;            /* The print dialog window*/
static GtkWidget *entry1;                /* The print command entry*/
static GtkWidget *entry2;                /* The save command entry*/
static GtkWidget *save_optionmenu;       /* File format option menu*/
static GtkWidget *action_button;         /* Either Save or Print*/
static GtkWidget *action_button_label;   /* String for action_button*/

/*                     DEF: get_ctg_image
 * Creates an image of the contig display.  Stitches together
 * all the tracks into one pixmap, containing either the entire
 * contig or the current region. Also adds a header.*/
GdkPixbuf*
get_ctg_image(){
  GdkPixbuf* pb, *pb2;
  GtkTrack *t=NULL, *t2=NULL;
  int i, x, x2, y, width, width2, height=0;
  GdkPixmap *pixmap;
  GdkGC *print_gc;
  GdkColor color;
  GdkFont *font;
  char stat_str[150];
  char rem_str[150];
  char region_str[150];

  g_return_val_if_fail(track_num!=0, NULL);

   
  /* get height of pixmap for printing*/

  height = 75;   /*Header height*/

  for(i=0; i<track_num; i++){
    t=GTK_TRACK(tracks[i]);
    if(t->entity!=ANCHORS) t2=t;
    height += t->drawing_area->allocation.height;
    if(i!=track_num-1) height += 5;  /*Buffer between tracks*/
  }

  if(print_area==0){ /*region*/
    x = MIN(region[0].scr, region[1].scr);
    width=abs(region[1].scr - region[0].scr) + 1;
  }
  else{  /*entire ctg*/
    x=0;
    if(t2!=NULL)
      width=t2->drawing_area->allocation.width; 
    else if(t!=NULL)
      width=t->drawing_area->allocation.width; 
    else
      width=0;
  }

  /* Create the pixmap to contain the data for all tracks being printed*/
  pixmap=gdk_pixmap_new(t->drawing_area->window, width, height, -1);

  /* Create the graphics context for the dotted line separating tracks*/
  print_gc=gdk_gc_new(t->drawing_area->window);

  /* Draw page header*/
  y=0;
  gdk_draw_rectangle(pixmap, t->drawing_area->style->white_gc,
		     TRUE, 0, y, width, 75);
  if((font = gdk_font_load("-*-courier-bold-r-*-*-*-120-*-*-*-*-*-*"))==NULL)
    g_warning("Failed to allocate font");
  y=15;
  gdk_draw_string(pixmap,
                  font,
                  print_gc,
                  0, y,
                  ctg_window_title);
  y+=15;
  sprintf(rem_str, "Chromosome remark: %s     User remark: %s",
          contigs[currentctg].chr_msg,
          contigs[currentctg].user_msg);
  gdk_draw_string(pixmap,
                  font,
                  print_gc,
                  0, y,
                  rem_str);
  y+=15;
  sprintf(stat_str, "Clones: %d   Markers: %d   Sequenced: %d   Length: %d CB units",
          contigs[currentctg].count, contigs[currentctg].markers, contigs[currentctg].seq,
          page_end_right-page_end_left+1);
  gdk_draw_string(pixmap,
                  font,
                  print_gc,
                  0, y,
                  stat_str);
  y+=15;
  if(print_area==0)
    sprintf(region_str, "Region printed: %d to %d CB units\n", 
            MIN(region[0].band, region[1].band), 
            MAX(region[0].band, region[1].band)); 
  else if(print_area==1)
    sprintf(region_str, "Region printed: %d to %d CB units\n", page_end_left,
            page_end_right);
  gdk_draw_string(pixmap,
                  font,
                  print_gc,
                  0, y,
                  region_str);
  y+=5;
  /* Separator between header and tracks*/
  gdk_draw_line(pixmap, print_gc, 0, y, width/2, y);
  y+=10;
  gdk_font_unref(font);

  gdk_gc_set_line_attributes(print_gc, 
                             0, 
                             GDK_LINE_ON_OFF_DASH,
                             GDK_CAP_BUTT,
                             GDK_JOIN_MITER);
  if(gdk_color_parse("gray", &(color))){
    if(!gdk_colormap_alloc_color(t->colormap, &(color), FALSE, TRUE)){
      printf("WARNING -- could not allocate color\n");
    }
  }
  gdk_gc_set_foreground(print_gc,&color);

  /* Add tracks to single pixmap*/
  for(i=0; i<track_num; i++){
    t=GTK_TRACK(tracks[i]);
    if((print_area == 0) && (t->entity==ANCHORS)){
      x2 = MIN(region[0].xsrc, region[1].xsrc);
      width2=abs(region[1].xsrc - region[0].xsrc) + 1;
      gdk_draw_pixmap(pixmap,
		      t->drawing_area->style->fg_gc[GTK_WIDGET_STATE (t->drawing_area)],
		      t->pixmap,
		      x2,0,
		      0,y,
		      width2,-1);
    }
    else{
      if(t->entity==ANCHORS){
        gdk_draw_rectangle(pixmap,
                           t->drawing_area->style->bg_gc[GTK_WIDGET_STATE (t->drawing_area)],
                           TRUE,
                           x, y,
			   width,t->drawing_area->allocation.height);
      }
      gdk_draw_pixmap(pixmap,
		      t->drawing_area->style->fg_gc[GTK_WIDGET_STATE (t->drawing_area)],
		      t->pixmap,
		      x,0,
		      0,y,
		      width,-1);
    }
    y+=t->drawing_area->allocation.height;
    if(i!=track_num-1){
      /* Track separator*/
      gdk_draw_rectangle(pixmap, t->drawing_area->style->white_gc,
			 TRUE, 0, y, width, 5);
      y+=3;
      gdk_draw_line(pixmap, print_gc, 0, y, width, y);
      y+=2;
    }
  }
  pb = gdk_pixbuf_get_from_drawable(0,pixmap, NULL, 0, 0, 0,0,-1, -1);
  if(orientation==1){ /*horizontal*/
    pb2 =  gdk_pixbuf_rotate_simple(pb,90);
    g_object_unref(pb);
    pb = pb2; // gdk_pixbuf_flip(pb2,TRUE);
  }
  gdk_gc_unref(print_gc);
  return pb;
}
/*                     DEF: do_print
 * Creates an image by calling get_ctg_image, saves it as
 * postscript, and sends it to the printer.  Removes postscript
 * file when done.*/
static
void do_print(){
  GdkPixbuf *pb;
  char file_name[83];
  char file_path[MAXPATHLEN+1];
  char cmd_str[MAXPATHLEN+MAX_PRINT_COMMAND_LEN+2];
  GError *err = 0;

  sprintf(file_name, "%s.ps", ctg_window_title);
  removespaces(file_name);
  sprintf(file_path, "%s/%s", GETCURDIR, file_name);
  
  printf("Preparing to print... ");
  fflush(stdout);
  pb = get_ctg_image();
  gdk_pixbuf_savev(pb, file_path, "jpeg",0,0,&err);
if (err != NULL)
{
    printf("error %s\n",err->message);fflush(0);
}
  sprintf(cmd_str, "%s %s\n", print_command, file_path);
  printf("printing... ");
  fflush(stdout);
  system(cmd_str);
  sprintf(cmd_str, "rm -f %s\n", file_path);
  system(cmd_str);
  printf("done.\n");
  g_object_unref(pb);
}
  
/*                     DEF: do_save
 * Creates an image by calling get_ctg_image, and saves it
 * in the selected format*/
static
void do_save(){
  GdkPixbuf *pb;
  char file_name[84];
  char file_path[MAXPATHLEN+1];
  char type[30];
  GError* err = 0;

  switch(save_type){
    case JPEG:
      sprintf(file_name,"%s.jpg",save_file);
      sprintf(type,"jpeg");
      break;
    case GIF:
      sprintf(file_name,"%s.gif",save_file);
      sprintf(type,"gif");
      break;
    case TIFF:
      sprintf(file_name,"%s.tif",save_file);
      sprintf(type,"tiff");
      break;
    case PNG:
      sprintf(file_name,"%s.png",save_file);
      sprintf(type,"png");
      break;
    case BITMAP:
      sprintf(file_name,"%s.bmp",save_file);
      sprintf(type,"bmp");
      break;
  }
  sprintf(file_path, "%s/%s", GETCURDIR, file_name);
  printf("Saving file to %s... ",file_path);
  fflush(stdout);
  pb = get_ctg_image();
  gdk_pixbuf_savev(pb, file_path, type,0,0,&err);
  printf("done.\n");
  g_object_unref(pb);
}

/*                     DEF: entry1_callback
 * Print command entry callback.*/
static void
entry1_callback(GtkWidget *widget, gpointer data){
  strcpy(print_command, gtk_entry_get_text(GTK_ENTRY(widget)));
}
/*                     DEF: entry2_callback
 * Save command entry callback.*/
static void
entry2_callback(GtkWidget *widget, gpointer data){
  strcpy(save_file, gtk_entry_get_text(GTK_ENTRY(widget)));
}

/*                     DEF: print_radiobutton_callback
 * Print/save toggle*/
static void
print_radiobutton_callback(GtkWidget *widget, gpointer data){
  if (GTK_TOGGLE_BUTTON (widget)->active){
    action=0;
    gtk_label_set_text(GTK_LABEL(action_button_label), "Print");
    gtk_widget_set_sensitive(entry1, TRUE);
    gtk_widget_set_sensitive(entry2, FALSE);
    gtk_widget_set_sensitive(save_optionmenu, FALSE);
  }
}

/*                     DEF: save_radiobutton_callback
 * Print/save toggle*/
static void
save_radiobutton_callback(GtkWidget *widget, gpointer data){
  if (GTK_TOGGLE_BUTTON (widget)->active){
    action=1;
    gtk_label_set_text(GTK_LABEL(action_button_label), "Save");
    gtk_widget_set_sensitive(entry1, FALSE);
    gtk_widget_set_sensitive(entry2, TRUE);
    gtk_widget_set_sensitive(save_optionmenu, TRUE);
  }
}

/*                     DEF: region_radiobutton_callback
 * Region/entire display toggle*/
static void
region_radiobutton_callback(GtkWidget *widget, gpointer data){
  if (GTK_TOGGLE_BUTTON (widget)->active) print_area=0;
}

/*                     DEF: entire_radiobutton_callback
 * Region/entire display toggle*/
static void
entire_radiobutton_callback(GtkWidget *widget, gpointer data){
  if (GTK_TOGGLE_BUTTON (widget)->active) print_area=1;
}

/*                     DEF: vert_radiobutton_callback
 * Vertical/horizontal orientation toggle*/
static void
vert_radiobutton_callback(GtkWidget *widget, gpointer data){
  if (GTK_TOGGLE_BUTTON (widget)->active) orientation=0;
}

/*                     DEF: hoz_radiobutton_callback
 * Vertical/horizontal orientation toggle*/
static void
hoz_radiobutton_callback(GtkWidget *widget, gpointer data){
  if (GTK_TOGGLE_BUTTON (widget)->active) orientation=1;
}

/*                     DEF: save_menu_callback1
 * Save file format = JPEG*/
static void
save_menu_callback1(GtkWidget *widget, gpointer data){
  save_type = JPEG;
}

/*                     DEF: save_menu_callback2
 * Save file format = GIF*/
static void
save_menu_callback2(GtkWidget *widget, gpointer data){
  save_type = GIF;
}

/*                     DEF: save_menu_callback3
 * Save file format = TIFF*/
static void
save_menu_callback3(GtkWidget *widget, gpointer data){
  save_type = TIFF;
}

/*                     DEF: save_menu_callback4
 * Save file format = PNG*/
static void
save_menu_callback4(GtkWidget *widget, gpointer data){
  save_type = PNG;
}

/*                     DEF: save_menu_callback5
 * Save file format = BITMAP*/
static void
save_menu_callback5(GtkWidget *widget, gpointer data){
  save_type = BITMAP;
}


/*                     DEF: cancel_button_callback
 * Cancel button callback (duh)*/
static void
cancel_button_callback(GtkWidget *widget, gpointer data){
  if(print_window!=NULL)
    gtk_widget_destroy(print_window);
  print_window=NULL;
}

/*                     DEF: action_button_callback
 * Perform either a print or a save.*/
static void
action_button_callback(GtkWidget *widget, gpointer data){
  if(action==0)
    do_print();
  else
    do_save();
  gtk_widget_destroy(print_window);
}

/*                     DEF: print_dialog
 * Graphics stuff for the print/save window*/
void
print_dialog(){
  GtkWidget *vbox, *vbox2;
  GtkWidget *hbox, *hbox2;
  GtkWidget *label;
  GtkWidget *frame;
  GtkWidget *print_radiobutton;
  GtkWidget *save_radiobutton;
  GtkWidget *region_radiobutton;
  GtkWidget *entire_radiobutton;
  GtkWidget *hoz_radiobutton;
  GtkWidget *vert_radiobutton;
  GtkWidget *save_menu;
  GtkWidget *jpeg1;
  GtkWidget *gif1;
  GtkWidget *tiff1;
  GtkWidget *png1;
  GtkWidget *bitmap1;
  GtkWidget *cancel_button;
  char title[20];
  char action_button_text[7];
 
  if(print_window!=NULL){
    gdk_window_raise(print_window->window);
    return;
  }

  sprintf(save_file, "ctg%d", currentctg);
  sprintf(title, "Print/Save Ctg%d", currentctg);
  if(action==0) strcpy(action_button_text, "Print");
  else strcpy(action_button_text, "Save");

  print_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_container_set_border_width (GTK_CONTAINER (print_window), 5);
  gtk_window_set_title (GTK_WINDOW (print_window), title);
  gtk_window_set_policy (GTK_WINDOW (print_window), FALSE, TRUE, FALSE);
  gtk_signal_connect (GTK_OBJECT (print_window), "destroy",
		      GTK_SIGNAL_FUNC (cancel_button_callback), NULL);

  vbox=gtk_vbox_new(FALSE, 5);
  gtk_container_add (GTK_CONTAINER (print_window), vbox);

  hbox=gtk_hbox_new(FALSE,5);
  frame = gtk_frame_new("Print");
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, TRUE, 0);
  vbox2=gtk_vbox_new(FALSE,7);
  gtk_container_set_border_width (GTK_CONTAINER (vbox2), 2);

  print_radiobutton = gtk_radio_button_new_with_label(NULL, "Send to printer");
  gtk_signal_connect (GTK_OBJECT (print_radiobutton), "clicked",
		      GTK_SIGNAL_FUNC (print_radiobutton_callback), NULL);
  gtk_box_pack_start (GTK_BOX (vbox2), print_radiobutton, FALSE, FALSE, 0);

  hbox2=gtk_hbox_new(FALSE, 5);
  label = gtk_label_new("Print command:");
  gtk_box_pack_start(GTK_BOX(hbox2), label, FALSE, FALSE, 0);
  entry1 = gtk_entry_new_with_max_length(MAX_PRINT_COMMAND_LEN);
  gtk_entry_set_text(GTK_ENTRY(entry1), print_command);
  gtk_signal_connect (GTK_OBJECT (entry1), "changed",
		      GTK_SIGNAL_FUNC (entry1_callback), NULL);
  gtk_box_pack_start(GTK_BOX(hbox2), entry1, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(vbox2), hbox2, FALSE, FALSE, 0);

  gtk_container_add(GTK_CONTAINER(frame), vbox2);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);


  hbox=gtk_hbox_new(FALSE, 5);
  frame = gtk_frame_new("Save");
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, TRUE, 0);
  vbox2=gtk_vbox_new(FALSE,7);
  gtk_container_set_border_width (GTK_CONTAINER (vbox2), 2);

  save_radiobutton = gtk_radio_button_new_with_label(
                       gtk_radio_button_group(GTK_RADIO_BUTTON(print_radiobutton)),
                       "Save to file");
  gtk_signal_connect (GTK_OBJECT (save_radiobutton), "clicked",
		      GTK_SIGNAL_FUNC (save_radiobutton_callback), NULL);
  gtk_box_pack_start (GTK_BOX (vbox2), save_radiobutton, FALSE, FALSE, 0);

  hbox2=gtk_hbox_new(FALSE, 5);
  label = gtk_label_new("File name:");
  gtk_box_pack_start(GTK_BOX(hbox2), label, FALSE, FALSE, 0);
  entry2 = gtk_entry_new_with_max_length(MAX_PRINT_COMMAND_LEN);
  gtk_entry_set_text(GTK_ENTRY(entry2), save_file);
  gtk_signal_connect (GTK_OBJECT (entry2), "changed",
		      GTK_SIGNAL_FUNC (entry2_callback), NULL);
  gtk_box_pack_start(GTK_BOX(hbox2), entry2, TRUE, TRUE, 0);

  save_optionmenu=gtk_option_menu_new();
  gtk_box_pack_start(GTK_BOX(hbox2), save_optionmenu, FALSE, FALSE, 0);
  save_menu=gtk_menu_new();

  jpeg1 = gtk_menu_item_new_with_label ("Jpeg (.jpg)");
  gtk_signal_connect (GTK_OBJECT (jpeg1), "activate",
		      GTK_SIGNAL_FUNC (save_menu_callback1),
		      NULL);
  gtk_widget_show(jpeg1);
  gtk_container_add (GTK_CONTAINER (save_menu), jpeg1);

  gif1 = gtk_menu_item_new_with_label ("Gif (.gif)");
  gtk_signal_connect (GTK_OBJECT (gif1), "activate",
		      GTK_SIGNAL_FUNC (save_menu_callback2),
		      NULL);
  gtk_widget_show(gif1);
  gtk_container_add (GTK_CONTAINER (save_menu), gif1);

  tiff1 = gtk_menu_item_new_with_label ("Tiff (.tif)");
  gtk_signal_connect (GTK_OBJECT (tiff1), "activate",
		      GTK_SIGNAL_FUNC (save_menu_callback3),
		      NULL);
  gtk_widget_show(tiff1);
  gtk_container_add (GTK_CONTAINER (save_menu), tiff1);

  png1 = gtk_menu_item_new_with_label ("PNG (.png)");
  gtk_signal_connect (GTK_OBJECT (png1), "activate",
		      GTK_SIGNAL_FUNC (save_menu_callback4),
		      NULL);
  gtk_widget_show(png1);
  gtk_container_add (GTK_CONTAINER (save_menu), png1);

  bitmap1 = gtk_menu_item_new_with_label ("Bitmap (.bmp)");
  gtk_signal_connect (GTK_OBJECT (bitmap1), "activate",
		      GTK_SIGNAL_FUNC (save_menu_callback5),
		      NULL);
  gtk_widget_show(bitmap1);
  gtk_container_add (GTK_CONTAINER (save_menu), bitmap1);

  gtk_option_menu_set_menu (GTK_OPTION_MENU (save_optionmenu), save_menu);  
  gtk_option_menu_set_history (GTK_OPTION_MENU (save_optionmenu), save_type);  
  gtk_widget_show(save_optionmenu);
  gtk_box_pack_start(GTK_BOX(vbox2), hbox2, FALSE, FALSE, 0);

  gtk_container_add(GTK_CONTAINER(frame), vbox2);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

  hbox=gtk_hbox_new(FALSE,5);
  frame = gtk_frame_new("Print Area");
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, FALSE, 0);
  hbox2=gtk_hbox_new(FALSE,7);
  gtk_container_set_border_width (GTK_CONTAINER (hbox2), 2);

  region_radiobutton = gtk_radio_button_new_with_label(NULL, "Region");
  gtk_signal_connect (GTK_OBJECT (region_radiobutton), "clicked",
		      GTK_SIGNAL_FUNC (region_radiobutton_callback), NULL);
  gtk_box_pack_start (GTK_BOX (hbox2), region_radiobutton, FALSE, FALSE, 0);

  entire_radiobutton = gtk_radio_button_new_with_label(
                       gtk_radio_button_group(GTK_RADIO_BUTTON(region_radiobutton)),
                       "Entire Display");
  gtk_signal_connect (GTK_OBJECT (entire_radiobutton), "clicked",
		      GTK_SIGNAL_FUNC (entire_radiobutton_callback), NULL);
  gtk_box_pack_start (GTK_BOX (hbox2), entire_radiobutton, FALSE, FALSE, 0);
  gtk_container_add(GTK_CONTAINER(frame), hbox2);

  frame = gtk_frame_new("Orientation");
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, FALSE, 0);
  hbox2=gtk_hbox_new(FALSE,7);
  gtk_container_set_border_width (GTK_CONTAINER (hbox2), 2);

  vert_radiobutton = gtk_radio_button_new_with_label(NULL,"Vertical");
  gtk_signal_connect (GTK_OBJECT (vert_radiobutton), "clicked",
		      GTK_SIGNAL_FUNC (vert_radiobutton_callback), NULL);
  gtk_box_pack_start (GTK_BOX (hbox2), vert_radiobutton, FALSE, FALSE, 0);

  hoz_radiobutton = gtk_radio_button_new_with_label(
                       gtk_radio_button_group(GTK_RADIO_BUTTON(vert_radiobutton)),
                       "Horizontal");
  gtk_signal_connect (GTK_OBJECT (hoz_radiobutton), "clicked",
		      GTK_SIGNAL_FUNC (hoz_radiobutton_callback), NULL);
  gtk_box_pack_start (GTK_BOX (hbox2), hoz_radiobutton, FALSE, FALSE, 0);

  gtk_container_add(GTK_CONTAINER(frame), hbox2);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, FALSE, 0);

/*  hbox=gtk_hbox_new(FALSE,5);
  frame = gtk_frame_new("Page Size");
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, FALSE, 0);
  hbox2=gtk_hbox_new(FALSE,7);
  gtk_container_set_border_width (GTK_CONTAINER (hbox2), 2);

  page_optionmenu=gtk_option_menu_new();
  gtk_box_pack_start(GTK_BOX(hbox2), page_optionmenu, FALSE, FALSE, 0);
  page_menu=gtk_menu_new();

  letter1 = gtk_menu_item_new_with_label ("Letter (8.5\" x 11\")");
  gtk_signal_connect (GTK_OBJECT (letter1), "activate",
		      GTK_SIGNAL_FUNC (page_size_menu_callback1),
		      NULL);
  gtk_widget_show(letter1);
  gtk_container_add (GTK_CONTAINER (page_menu), letter1);

  legal1 = gtk_menu_item_new_with_label ("Legal (8.5\" x 14\")");
  gtk_signal_connect (GTK_OBJECT (legal1), "activate",
		      GTK_SIGNAL_FUNC (page_size_menu_callback2),
		      NULL);
  gtk_widget_show(legal1);
  gtk_container_add (GTK_CONTAINER (page_menu), legal1);

  a3_1 = gtk_menu_item_new_with_label ("A3");
  gtk_signal_connect (GTK_OBJECT (a3_1), "activate",
		      GTK_SIGNAL_FUNC (page_size_menu_callback3),
		      NULL);
  gtk_widget_show(a3_1);
  gtk_container_add (GTK_CONTAINER (page_menu), a3_1);

  a4_1 = gtk_menu_item_new_with_label ("A4");
  gtk_signal_connect (GTK_OBJECT (a4_1), "activate",
		      GTK_SIGNAL_FUNC (page_size_menu_callback4),
		      NULL);
  gtk_widget_show(a4_1);
  gtk_container_add (GTK_CONTAINER (page_menu), a4_1);

  a5_1 = gtk_menu_item_new_with_label ("A5");
  gtk_signal_connect (GTK_OBJECT (a5_1), "activate",
		      GTK_SIGNAL_FUNC (page_size_menu_callback5),
		      NULL);
  gtk_widget_show(a5_1);
  gtk_container_add (GTK_CONTAINER (page_menu), a5_1);

  executive1 = gtk_menu_item_new_with_label ("Executive");
  gtk_signal_connect (GTK_OBJECT (executive1), "activate",
		      GTK_SIGNAL_FUNC (page_size_menu_callback6),
		      NULL);
  gtk_widget_show(executive1);
  gtk_container_add (GTK_CONTAINER (page_menu), executive1);

  folio1 = gtk_menu_item_new_with_label ("Folio");
  gtk_signal_connect (GTK_OBJECT (folio1), "activate",
		      GTK_SIGNAL_FUNC (page_size_menu_callback7),
		      NULL);
  gtk_widget_show(folio1);
  gtk_container_add (GTK_CONTAINER (page_menu), folio1);

  gtk_option_menu_set_menu (GTK_OPTION_MENU (page_optionmenu), page_menu);  
  gtk_widget_show(page_optionmenu);
*/

  cancel_button = gtk_button_new_with_label("Cancel");
  gtk_signal_connect (GTK_OBJECT (cancel_button), "clicked",
		      GTK_SIGNAL_FUNC (cancel_button_callback),
		      NULL);
  gtk_box_pack_start (GTK_BOX (hbox), cancel_button, TRUE, TRUE, 0);

  action_button = gtk_button_new();
  action_button_label = gtk_label_new(action_button_text);
  gtk_misc_set_alignment (GTK_MISC (action_button_label), 0.5, 0.5);
  gtk_container_add (GTK_CONTAINER (action_button), action_button_label);
  gtk_signal_connect (GTK_OBJECT (action_button), "clicked",
		      GTK_SIGNAL_FUNC (action_button_callback),
		      NULL);
  gtk_box_pack_start (GTK_BOX (hbox), action_button, TRUE, TRUE, 0);


  /* Set defaults*/
  if(action==0){
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(print_radiobutton), TRUE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(save_radiobutton), FALSE);
    gtk_widget_set_sensitive(entry1, TRUE);
    gtk_widget_set_sensitive(entry2, FALSE);
    gtk_widget_set_sensitive(save_optionmenu, FALSE);
  }
  else{
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(print_radiobutton), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(save_radiobutton), TRUE);
    gtk_widget_set_sensitive(entry1, FALSE);
    gtk_widget_set_sensitive(entry2, TRUE);
    gtk_widget_set_sensitive(save_optionmenu, TRUE);
  }

  if(print_area==0){
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(region_radiobutton), TRUE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(entire_radiobutton), FALSE);
  }
  else{
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(region_radiobutton), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(entire_radiobutton), TRUE);
  }

  if(orientation==0){
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(vert_radiobutton), TRUE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(hoz_radiobutton), FALSE);
  }
  else{
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(vert_radiobutton), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(hoz_radiobutton), TRUE);
  }

//  gtk_option_menu_set_history (GTK_OPTION_MENU (page_optionmenu), page_type);  

  gtk_widget_show_all(print_window);
}

