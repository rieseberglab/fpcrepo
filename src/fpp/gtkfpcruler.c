/****************************************************
 * Modified by Fred Engler
 *
 * Modification: Code modified from the gtkruler widget.
 * Added two position markers, Selection/movement
 * of position markers is done by clicking and dragging.
 * Metric calculation has been modified to correspond to 
 * CB units/Base pairs for the current zoom level of the FPC contig
 * display.  When a position marker is clicked, a vertical line
 * is drawn across the display to make it easy to see how the
 * postion relates to FPC clones.  All modifications are commented
 * with 'fred'.
 ****************************************************/

/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#include "gtkfpcruler.h"

extern float horiz_scale_factor;

extern GtkAdjustment *main_hadj;  /*fred 8/6/03*/
extern void update_fpcruler();

enum {
  ARG_0,
  ARG_LOWER,
  ARG_UPPER,
  ARG_POSITION1,
  ARG_POSITION2,
  ARG_MAX_SIZE,
  ARG_ZOOM_ADJUST
};

extern int pos1_first;
extern int pos2_first;

static void gtk_fpcruler_class_init    (GtkFpcRulerClass  *klass);
static void gtk_fpcruler_init          (GtkFpcRuler       *fpcruler);
static void gtk_fpcruler_realize       (GtkWidget      *widget);
static void gtk_fpcruler_unrealize     (GtkWidget      *widget);
static void gtk_fpcruler_size_allocate (GtkWidget      *widget,
				     GtkAllocation  *allocation);
static gint gtk_fpcruler_expose        (GtkWidget      *widget,
				     GdkEventExpose *event);
static void gtk_fpcruler_make_pixmap   (GtkFpcRuler       *fpcruler);
static void gtk_fpcruler_set_arg       (GtkObject      *object,
				     GtkArg         *arg,
				     guint           arg_id);
static void gtk_fpcruler_get_arg       (GtkObject      *object,
				     GtkArg         *arg,
				     guint           arg_id);

static GtkWidgetClass *parent_class;

static void bases_callback(GtkWidget *widget, GtkFpcRuler *fpcruler){
  fpcruler->ruler_units=BASES;
  update_fpcruler(GTK_OBJECT(main_hadj), fpcruler);
}

static void cbunits_callback(GtkWidget *widget, GtkFpcRuler *fpcruler){
  fpcruler->ruler_units=CBUNITS;
  update_fpcruler(GTK_OBJECT(main_hadj), fpcruler);
}

/* fred 6/10/03 -- change static fpcruler_metrics table to dynamically
 * computed pixels_per_unit, depending on zoom*/
static GtkFpcRulerMetric *
get_fpcruler_metrics(float zoom, enum rulerUnit ruler_units){
  GtkFpcRulerMetric *ptr;

  ptr= (GtkFpcRulerMetric *) malloc(sizeof(GtkFpcRulerMetric));

  if(ruler_units==BASES)
    ptr->pixels_per_unit = (float)(zoom * horiz_scale_factor)/4.096;
  else
    ptr->pixels_per_unit = zoom * horiz_scale_factor;

  ptr->fpcruler_scale[0] = 1;
  ptr->fpcruler_scale[1] = 2;
  ptr->fpcruler_scale[2] = 5;
  ptr->fpcruler_scale[3] = 10;
  ptr->fpcruler_scale[4] = 25;
  ptr->fpcruler_scale[5] = 50;
  ptr->fpcruler_scale[6] = 100;
  ptr->fpcruler_scale[7] = 250;
  ptr->fpcruler_scale[8] = 500;
  ptr->fpcruler_scale[9] = 1000;

  ptr->subdivide[0] = 1;
  ptr->subdivide[1] = 5;
  ptr->subdivide[2] = 10;
  ptr->subdivide[3] = 50;
  ptr->subdivide[4] = 100;

  return ptr;
}

GtkType
gtk_fpcruler_get_type (void)
{
  static GtkType fpcruler_type = 0;

  if (!fpcruler_type)
    {
      static const GtkTypeInfo fpcruler_info =
      {
	"GtkFpcRuler",
	sizeof (GtkFpcRuler),
	sizeof (GtkFpcRulerClass),
	(GtkClassInitFunc) gtk_fpcruler_class_init,
	(GtkObjectInitFunc) gtk_fpcruler_init,
	/* reserved_1 */ NULL,
        /* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL,
      };

      fpcruler_type = gtk_type_unique (GTK_TYPE_WIDGET, &fpcruler_info);
    }

  return fpcruler_type;
}

static void
gtk_fpcruler_class_init (GtkFpcRulerClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;

  parent_class = gtk_type_class (GTK_TYPE_WIDGET);
  
  object_class->set_arg = gtk_fpcruler_set_arg;
  object_class->get_arg = gtk_fpcruler_get_arg;

  widget_class->realize = gtk_fpcruler_realize;
  widget_class->unrealize = gtk_fpcruler_unrealize;
  widget_class->size_allocate = gtk_fpcruler_size_allocate;
  widget_class->expose_event = gtk_fpcruler_expose;

  class->draw_ticks = NULL;
  class->draw_pos = NULL;

  gtk_object_add_arg_type ("GtkFpcRuler::lower", GTK_TYPE_FLOAT,
			   GTK_ARG_READWRITE, ARG_LOWER);
  gtk_object_add_arg_type ("GtkFpcRuler::upper", GTK_TYPE_FLOAT,
			   GTK_ARG_READWRITE, ARG_UPPER);
  gtk_object_add_arg_type ("GtkFpcRuler::position1", GTK_TYPE_FLOAT,
			   GTK_ARG_READWRITE, ARG_POSITION1);
  gtk_object_add_arg_type ("GtkFpcRuler::position2", GTK_TYPE_FLOAT,
			   GTK_ARG_READWRITE, ARG_POSITION2);
  gtk_object_add_arg_type ("GtkFpcRuler::max_size", GTK_TYPE_FLOAT,
			   GTK_ARG_READWRITE, ARG_MAX_SIZE);
  gtk_object_add_arg_type ("GtkFpcRuler::zoom_adjust", GTK_TYPE_FLOAT,
			   GTK_ARG_READWRITE, ARG_ZOOM_ADJUST);
}

static void
gtk_fpcruler_init (GtkFpcRuler *_fpcruler)
{
  GtkWidget *bases_item;
  GtkWidget *cbunits_item;

  _fpcruler->backing_store = NULL;
  _fpcruler->non_gr_exp_gc = NULL;
  _fpcruler->xsrc1 = 0;
  _fpcruler->ysrc1 = 0;
  _fpcruler->xsrc2 = 0;
  _fpcruler->ysrc2 = 0;
  _fpcruler->slider_size = 0;
  _fpcruler->lower = 0;
  _fpcruler->upper = 0;
  _fpcruler->position1 = 0;
  _fpcruler->position2 = 0;
  _fpcruler->selected_pos=-1; /*fred 2/26/03*/
  _fpcruler->xor_gc=NULL; /*fred 3/13/03*/
  _fpcruler->active_pos_gc=NULL; /*fred 6/10/03*/
  _fpcruler->passive_pos_gc=NULL;
  _fpcruler->max_size = 0;
  _fpcruler->zoom_adjust = 1;
  _fpcruler->ruler_units = CBUNITS;  /*fred 8/6/03*/

  /*fred 8/6/03*/
  _fpcruler->units_popup_menu=gtk_menu_new();
  bases_item=gtk_menu_item_new_with_label("Kilobases");
  gtk_menu_append(GTK_MENU(_fpcruler->units_popup_menu),bases_item);
  gtk_signal_connect(GTK_OBJECT(bases_item),"activate",
                     GTK_SIGNAL_FUNC(bases_callback),
                     _fpcruler);
  gtk_widget_show(bases_item);

  cbunits_item=gtk_menu_item_new_with_label("CB units");
  gtk_menu_append(GTK_MENU(_fpcruler->units_popup_menu),cbunits_item);
  gtk_signal_connect(GTK_OBJECT(cbunits_item),"activate",
                     GTK_SIGNAL_FUNC(cbunits_callback),
                     _fpcruler);
  gtk_widget_show(cbunits_item);

  gtk_fpcruler_set_metric (_fpcruler, 2.0);
}

static void
gtk_fpcruler_set_arg (GtkObject  *object,
		   GtkArg     *arg,
		   guint       arg_id)
{
  GtkFpcRuler *fpcruler = GTK_FPC_RULER (object);

  switch (arg_id)
    {
    case ARG_LOWER:
      gtk_fpcruler_set_range (fpcruler, GTK_VALUE_FLOAT (*arg), fpcruler->upper,
			   fpcruler->position1, fpcruler->position2, fpcruler->max_size,
                           fpcruler->zoom_adjust);
      break;
    case ARG_UPPER:
      gtk_fpcruler_set_range (fpcruler, fpcruler->lower, GTK_VALUE_FLOAT (*arg),
			   fpcruler->position1, fpcruler->position2, fpcruler->max_size,
                           fpcruler->zoom_adjust);
      break;
    case ARG_POSITION1:
      gtk_fpcruler_set_range (fpcruler, fpcruler->lower, fpcruler->upper,
			   GTK_VALUE_FLOAT (*arg), fpcruler->position2, fpcruler->max_size,
                           fpcruler->zoom_adjust);
      break;
    case ARG_POSITION2:
      gtk_fpcruler_set_range (fpcruler, fpcruler->lower, fpcruler->upper,
			      fpcruler->position1, GTK_VALUE_FLOAT (*arg), fpcruler->max_size,
                              fpcruler->zoom_adjust);
      break;
    case ARG_MAX_SIZE:
      gtk_fpcruler_set_range (fpcruler, fpcruler->lower, fpcruler->upper,
			   fpcruler->position1, fpcruler->position2, GTK_VALUE_FLOAT (*arg),
                           fpcruler->zoom_adjust);
      break;
    case ARG_ZOOM_ADJUST:
      gtk_fpcruler_set_range (fpcruler, fpcruler->lower, fpcruler->upper,
			   fpcruler->position1, fpcruler->position2, fpcruler->max_size, 
                           GTK_VALUE_FLOAT(*arg));
      break;
    }
}

static void
gtk_fpcruler_get_arg (GtkObject  *object,
		   GtkArg     *arg,
		   guint       arg_id)
{
  GtkFpcRuler *fpcruler = GTK_FPC_RULER (object);
  
  switch (arg_id)
    {
    case ARG_LOWER:
      GTK_VALUE_FLOAT (*arg) = fpcruler->lower;
      break;
    case ARG_UPPER:
      GTK_VALUE_FLOAT (*arg) = fpcruler->upper;
      break;
    case ARG_POSITION1:
      GTK_VALUE_FLOAT (*arg) = fpcruler->position1;
      break;
    case ARG_POSITION2:
      GTK_VALUE_FLOAT (*arg) = fpcruler->position2;
      break;
    case ARG_MAX_SIZE:
      GTK_VALUE_FLOAT (*arg) = fpcruler->max_size;
      break;
    case ARG_ZOOM_ADJUST:
      GTK_VALUE_FLOAT (*arg) = fpcruler->zoom_adjust;
      break;
    default:
      arg->type = GTK_TYPE_INVALID;
      break;
    }
}

void
gtk_fpcruler_set_metric (GtkFpcRuler      *fpcruler,
		         float metric)
{
  g_return_if_fail (fpcruler != NULL);
  g_return_if_fail (GTK_IS_FPC_RULER (fpcruler));

  /*fred 6/10/03*/
  /*fpcruler->metric = (GtkFpcRulerMetric *) &fpcruler_metrics[metric];*/
  fpcruler->metric = get_fpcruler_metrics(metric, fpcruler->ruler_units);

  if (GTK_WIDGET_DRAWABLE (fpcruler))
    gtk_widget_queue_draw (GTK_WIDGET (fpcruler));
}

void
gtk_fpcruler_set_range (GtkFpcRuler *fpcruler,
		     gfloat    lower,
		     gfloat    upper,
		     gfloat    position1,
		     gfloat    position2,
		     gfloat    max_size,
		     gfloat    zoom_adjust)
{
  g_return_if_fail (fpcruler != NULL);
  g_return_if_fail (GTK_IS_FPC_RULER (fpcruler));

  fpcruler->lower = lower;
  fpcruler->upper = upper;
  fpcruler->position1 = position1;
  fpcruler->xsrc1 = position1;
  fpcruler->position2 = position2;
  fpcruler->xsrc2 = position2;
  fpcruler->max_size = max_size;
  fpcruler->zoom_adjust = zoom_adjust;

  if (GTK_WIDGET_DRAWABLE (fpcruler))
    gtk_widget_queue_draw (GTK_WIDGET (fpcruler));
}

void
gtk_fpcruler_draw_ticks (GtkFpcRuler *fpcruler)
{
  g_return_if_fail (fpcruler != NULL);
  g_return_if_fail (GTK_IS_FPC_RULER (fpcruler));

  if (GTK_RULER_GET_CLASS (fpcruler)->draw_ticks)
    GTK_RULER_GET_CLASS (fpcruler)->draw_ticks ((GtkRuler*)fpcruler);


}

void
gtk_fpcruler_draw_pos (GtkFpcRuler *fpcruler)
{
  g_return_if_fail (fpcruler != NULL);
  g_return_if_fail (GTK_IS_FPC_RULER (fpcruler));

  if (GTK_RULER_GET_CLASS (fpcruler)->draw_pos)
     GTK_RULER_GET_CLASS (fpcruler)->draw_pos ((GtkRuler*)fpcruler);
}


static void
gtk_fpcruler_realize (GtkWidget *widget)
{
  GtkFpcRuler *fpcruler;
  GdkWindowAttr attributes;
  gint attributes_mask;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_FPC_RULER (widget));

  fpcruler = GTK_FPC_RULER (widget);
  GTK_WIDGET_SET_FLAGS (fpcruler, GTK_REALIZED);

  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
  attributes.event_mask = gtk_widget_get_events (widget);
  attributes.event_mask |= (GDK_EXPOSURE_MASK |
			    GDK_POINTER_MOTION_MASK |
			    GDK_BUTTON1_MOTION_MASK |
			    GDK_POINTER_MOTION_HINT_MASK |
                            GDK_BUTTON_PRESS_MASK |    /*fred 2/26/03*/
                            GDK_BUTTON_RELEASE_MASK);  /*  "   "  */

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

  widget->window = gdk_window_new (gtk_widget_get_parent_window (widget), &attributes, attributes_mask);
  gdk_window_set_user_data (widget->window, fpcruler);

  widget->style = gtk_style_attach (widget->style, widget->window);
  gtk_style_set_background (widget->style, widget->window, GTK_STATE_ACTIVE);

  gtk_fpcruler_make_pixmap (fpcruler);
}

static void
gtk_fpcruler_unrealize (GtkWidget *widget)
{
  GtkFpcRuler *fpcruler;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_FPC_RULER (widget));

  fpcruler = GTK_FPC_RULER (widget);

  if (fpcruler->backing_store)
    gdk_pixmap_unref (fpcruler->backing_store);
  if (fpcruler->non_gr_exp_gc)
    gdk_gc_destroy (fpcruler->non_gr_exp_gc);

  fpcruler->backing_store = NULL;
  fpcruler->non_gr_exp_gc = NULL;

  g_free(fpcruler->metric);  /*fred 6/11/03*/

  if (GTK_WIDGET_CLASS (parent_class)->unrealize)
    (* GTK_WIDGET_CLASS (parent_class)->unrealize) (widget);
}

static void
gtk_fpcruler_size_allocate (GtkWidget     *widget,
			 GtkAllocation *allocation)
{
  GtkFpcRuler *fpcruler;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_FPC_RULER (widget));

  fpcruler = GTK_FPC_RULER (widget);
  widget->allocation = *allocation;

  if (GTK_WIDGET_REALIZED (widget))
    {
      gdk_window_move_resize (widget->window,
			      allocation->x, allocation->y,
			      allocation->width, allocation->height);

      gtk_fpcruler_make_pixmap (fpcruler);
    }
}

static gint
gtk_fpcruler_expose (GtkWidget      *widget,
		  GdkEventExpose *event)
{
  GtkFpcRuler *fpcruler;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_FPC_RULER (widget), FALSE);
  //g_return_val_if_fail (event != NULL, FALSE);

  if (GTK_WIDGET_DRAWABLE (widget))
    {
      fpcruler = GTK_FPC_RULER (widget);

      gtk_fpcruler_draw_ticks (fpcruler);
      
      gdk_draw_pixmap (widget->window,
		       fpcruler->non_gr_exp_gc,
		       fpcruler->backing_store,
		       0, 0, 0, 0,
		       widget->allocation.width,
		       widget->allocation.height);
      
      gtk_fpcruler_draw_pos (fpcruler);
    }

  return FALSE;
}

static void
gtk_fpcruler_make_pixmap (GtkFpcRuler *fpcruler)
{
  GtkWidget *widget;
  gint width;
  gint height;

  widget = GTK_WIDGET (fpcruler);

  if (fpcruler->backing_store)
    {
      gdk_window_get_size (fpcruler->backing_store, &width, &height);
      if ((width == widget->allocation.width) &&
	  (height == widget->allocation.height))
	return;

      gdk_pixmap_unref (fpcruler->backing_store);
    }

  fpcruler->backing_store = gdk_pixmap_new (widget->window,
					 widget->allocation.width,
					 widget->allocation.height,
					 -1);

  fpcruler->xsrc1 = 0;
  fpcruler->ysrc1 = 0;
  fpcruler->xsrc2 = 0;
  fpcruler->ysrc2 = 0;
  fpcruler->selected_pos = 0;
  pos1_first = 1;
  pos2_first = 1;

  if (!fpcruler->non_gr_exp_gc)
    {
      fpcruler->non_gr_exp_gc = gdk_gc_new (widget->window);
      gdk_gc_set_exposures (fpcruler->non_gr_exp_gc, FALSE);
    }
}
