/****************************************************
 * Modified by Fred Engler
 *
 * Modification: Code modified from the gtkhruler widget.
 * Added two position markers, Selection/movement
 * of position markers is done by clicking and dragging.
 * Metric calculation has been modified to correspond to 
 * CB units/Base pairs for the current zoom level of the FPC contig
 * display.  When a position marker is clicked, a vertical line
 * is drawn across the display to make it easy to see how the
 * postion relates to FPC clones.  All modifications are commented
 * with 'fred'.
 ****************************************************/

/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#ifndef __GTK_HFPC_RULER_H__
#define __GTK_HFPC_RULER_H__


#include <gdk/gdk.h>
#include "gtkfpcruler.h"


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_HFPC_RULER(obj)          GTK_CHECK_CAST (obj, gtk_hfpcruler_get_type (), GtkHFpcRuler)
#define GTK_HFPC_RULER_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_hfpcruler_get_type (), GtkHFpcRulerClass)
#define GTK_IS_HFPC_RULER(obj)       GTK_CHECK_TYPE (obj, gtk_hfpcruler_get_type ())


typedef struct _GtkHFpcRuler       GtkHFpcRuler;
typedef struct _GtkHFpcRulerClass  GtkHFpcRulerClass;

struct _GtkHFpcRuler
{
  GtkFpcRuler fpcruler;
};

struct _GtkHFpcRulerClass
{
  GtkFpcRulerClass parent_class;
};


guint      gtk_hfpcruler_get_type (void);
GtkWidget* gtk_hfpcruler_new      (void);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_HFPC_RULER_H__ */
