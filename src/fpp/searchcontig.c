/********************************************************
                  searchcontig.c
*********************************************************/
/*  Last edited: Jun 10 09:38 1996 (il) */
#include "fpp.h"
#include <gtk/gtkwidget.h>  /*fred 3/5/03*/
#define searchsize 21
extern GtkWidget *ctg_window;  /*fred 3/5/03*/
void subcontigsearch()
{
  struct list *p=NULL,*oldroot,*temp,*q;
  int i,j,len, lencl,pos; 
  char search[searchsize],search2[searchsize];
  BOOL found,first;
  char str1[80],test[10];

  if (acedata==NULL) return;
  q = listroot;
  oldroot = listroot;
  listroot = NULL;
  
  strcpy(search,chsearchglob);
  if (search[0]=='\0' || search[0]=='\n') strcpy(search,"*");

  if(strcmp(search,"*")==0){              /* if all to be found */
    listroot = oldroot;
    displaykeyset(1);
    return;
  }
  if(strncmp(search,"*",1)==0){          /* starts with * */
    j =0;
    for(i=1;i<searchsize;i++){ /* get the char bit */
      search[j++]=search[i];
    }
    if(strstr(search,"*")!=NULL){    /* if there is another * then format is *char* */
      first = TRUE;
      j =0;
      for(i=0;i<searchsize;i++){ /* get the char bit */
	if(search[i]!='*')
	  search2[j++]=search[i];
	else{
	  search2[j++]='\0';
	  break;
	}
      }
      while(q!=NULL){
	sprintf(test,"ctg%d",q->index);
	if(strstr(test,search2)){
	  if(first){
	    listroot  = (struct list *)messalloc((sizeof(struct list)));	
	    listroot->next = NULL;
	    p = listroot;
	    p->index = q->index;
	    first = FALSE;
	  }
	  else{
	    p->next = (struct list *)messalloc((sizeof(struct list)));	
	    p=p->next;
	    p->index = q->index;
	  }
	}
	q=q->next;
      }
      if(!first)
	p->next = NULL;
    }
    else{                     /* format is *char */
      
      len = strlen(search);
      
      first = TRUE;
      while(q!=NULL){
	sprintf(test,"ctg%d",q->index);
	lencl = strlen(test);
	found = TRUE;
	for(j=len;j>=0;j--){
	  if(search[j]!=test[lencl--]){
	    found = FALSE;
	    break;
	  }
	}
	if(found){
	  if(first){
	    listroot  = (struct list *)messalloc((sizeof(struct list)));	
	    listroot->next = NULL;
	    p = listroot;
	    p->index = q->index;
	    first = FALSE;
	  }
	  else{
	    p->next = (struct list *)messalloc((sizeof(struct list)));	
	    p=p->next;
	    p->index = q->index;
	  }
	}
	q=q->next;
      }
      if(!first)
	p->next = NULL;
    }
  }
  else if(strstr(search,"*")!=NULL){   /* the format is char* */
    strcpy(search2,search);
    if(strncasecmp(search,"ctg",3)!=0){  /* if search item does not have ctg then add it */
      strcpy(search2,"ctg");
      strcat(search2,search);
    }
    
    pos = 0;
    for(i=0;i<=searchsize;i++){
      if(search2[i]=='*'){
	pos= i;
	break;
      }
    }
    if(pos == 0)
      printf("error * not found but should be ????\n");
    first = TRUE;
    while(q!=NULL){
      sprintf(test,"ctg%d",q->index);
      if(strncasecmp(search2,test,pos)==0){
	if(first){
	  listroot  = (struct list *)messalloc((sizeof(struct list)));	
	  listroot->next = NULL;
	  p = listroot;
	  p->index = q->index;
	  first = FALSE;
	}
	else{
	  p->next = (struct list *)messalloc((sizeof(struct list)));	
	  p=p->next;
	  p->index = q->index;
	}
      }	
      q=q->next; 
    }
    if(!first)
      p->next = NULL;
  }
  else{                  /* no special characters */
    strcpy(search2,search);
    if(strncasecmp(search,"ctg",3)!=0){  /* if search item does not have ctg then add it */
      strcpy(search2,"ctg");
      strcat(search2,search);
    }
    first = TRUE;
    while(q!=NULL){
      sprintf(test,"ctg%d",q->index);	  
      if(strcasecmp(search2,test)==0){
	if(first){
	  listroot  = (struct list *)messalloc((sizeof(struct list)));	
	  listroot->next = NULL;
	  p = listroot;
	  p->index = q->index;
	  first = FALSE;
	}
	else{
	  p->next = (struct list *)messalloc((sizeof(struct list)));	
	  p=p->next;
	  p->index = q->index;
	}
      }
      q=q->next;
    }
    if(!first)
      p->next = NULL;
  }
  
  if(listroot==NULL){
    listroot = oldroot;
      i=0;
      while(oldroot != NULL){
        i++;
        oldroot= oldroot->next;
      }
      sprintf(str1,"Contig search (Name=%s) failed for keyset of %d items.",chsearchglob, i);
      displaymess(str1);
      displaykeyset(0);
  }
  else{
    page = 0;
    p= oldroot;
    while(p!=NULL){
      temp=p;
      p=p->next;
      if(!messfree(temp))
        printf("error freeing list memory\n");
    }
    displaykeyset(1);
  }
  /* graphPop();  fred 3/5/03*/
  if(ctg_window != NULL)
    gdk_window_raise(ctg_window->window);
}
