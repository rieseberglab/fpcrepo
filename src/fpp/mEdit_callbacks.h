#include <gtk/gtk.h>

struct tempMarkerInfo{
   int marker_index;
   int type;
} tempMk;
GtkWidget *clonesTextview;
GtkWidget *remarksTextview;

void
on_help_ok_clicked		       (GtkButton 	*button,
                                        gpointer         user_data);
void
on_pickClonesToggle_toggled            (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_addRemarkButton_clicked             (GtkButton       *button,
                                        int *type);

void
on_removeRemarkButton_clicked          (GtkButton       *button,
                                        int *type);

void
on_probe1_activate                     (GtkMenuItem     *menuitem,
                                        int *type);

void
on_sts1_activate                       (GtkMenuItem     *menuitem,
                                        int *type);

void
on_end1_activate                       (GtkMenuItem     *menuitem,
                                        int *type);

void
on_epcr1_activate                      (GtkMenuItem     *menuitem,
                                        int *type);

void
on_snp1_activate                       (GtkMenuItem     *menuitem,
                                        int *type);

void
on_yac1_activate                       (GtkMenuItem     *menuitem,
                                        int *type);

void
on_bac1_activate                       (GtkMenuItem     *menuitem,
                                        int *type);

void
on_pac1_activate                       (GtkMenuItem     *menuitem,
                                        int *type);

void
on_cdna1_activate                      (GtkMenuItem     *menuitem,
                                        int *type);

void
on_cosmid1_activate                    (GtkMenuItem     *menuitem,
                                        int *type);

void
on_fosmid1_activate                    (GtkMenuItem     *menuitem,
                                        int *type);

void
on_locus1_activate                     (GtkMenuItem     *menuitem,
                                        int *type);

void
on_clone1_activate                     (GtkMenuItem     *menuitem,
                                        int *type);

void
on_ebac1_activate                      (GtkMenuItem     *menuitem,
                                        int *type);

void
on_emrk1_activate                      (GtkMenuItem     *menuitem,
                                        int *type);

void
on_tc1_activate                        (GtkMenuItem     *menuitem,
                                        int *type);

void
on_ssr1_activate                       (GtkMenuItem     *menuitem,
                                        int *type);

void
on_rflp1_activate                      (GtkMenuItem     *menuitem,
                                        int *type);

void
on_overgo1_activate                    (GtkMenuItem     *menuitem,
                                        int *type);

void
on_rep1_activate                       (GtkMenuItem     *menuitem,
                                        int *type);

void
destroy_edit_marker_callback           (GtkWidget *widget,
                                        gpointer data);

void
on_cancelButton_clicked                (GtkButton       *button,
                                        GtkWidget *window);

void
on_applyButton_clicked                 (GtkButton       *button,
                                        struct tempMarkerInfo *tempMk);
