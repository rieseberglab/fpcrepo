
/***************************************************************
                     type.h
efriedr 4/2/02 -- added TC and SSR types;  previously added eBAC and eMRK
efriedr 7/30/02 -- added RFLP, OVERGO, and REP types
fred 3/3/03 -- added SEQ clone type
cari 12/30/03 - add > # Bands
****************************************************************/
#define VERSION "10.01"
#define LASTMOD "02 May 13"

#ifndef MAIN
extern char *clonetype[],*markertype[],*seqstat[],*seqtype[],
    *seqtype1[], *searchname[], *ctgstat[], *seq_type_names[];
#endif

/**********************CTG ************************/
/* sequence status changes in fpc_load, edit.c, and recalc.c 
   and the function in recalc is called by clonedit.c */
            /* 15jun99 the seqstat==AVOID will become obsolete.*/

#define OLDCTG_MARK 100000
#define CTGUSERMSG "::"

#define ctgstatnum 5
#define ND 0
#define NOCB 1
#define AVOID 2
#define NOACE 3
#define DEAD 4

#ifdef MAIN
char *ctgstat[]= {"Ok", "NoCB", "Avoid", "NoAce", "Dead"};
#endif

#define IsSeq(clp) (clp->seqstat != 0 && clp->seqstat!=CANCELLED)

#ifdef SANGER
#define AddToCtgSeq(clp, j)\
   {if (clp->seqstat != 0 && clp->seqstat!=CANCELLED) contigs[j].seq++;\
   if (contigs[j].ctgstat==ND && contigs[j].seq > 1) contigs[j].ctgstat=NOCB;}
#else
#define AddToCtgSeq(clp, j)\
   {if (clp->seqstat != 0 && clp->seqstat!=CANCELLED) contigs[j].seq++;}
#endif

#define NoSum(j)  (contigs[j].ctgstat==DEAD)
#define NoAce(j)  (contigs[j].ctgstat==DEAD || contigs[j].ctgstat==NOACE)
#define NoBuild(j) (contigs[j].ctgstat==DEAD || contigs[j].ctgstat==AVOID)
#define NoCBmap(j) (contigs[j].ctgstat==NOCB)

/****************** SEQUENCE ****************************/
/* ADD 18mar99 - remove GAP from stat and add all types */
/* fields are in fpshr.h - seq and seqtype for seqstat and seqtype respectively; 
   cloneedit.c lists again on pull-down menus of clone editor 
           (chg if add to seqtype or seqstat)
   clonegen.c uses seqstat[clp->seq] and seqtype[clp->seqtype]. 
   fpc_load and fpc_save use seqstat[clp->seq] and seqtype[clp->seqtype]
   pace_save check for seq status of not NONE or CANCELLED, and prints seqtype
   ace4_save checks for seq status of not NONE or CANCELLED
*/
   
#define FULLX 1
#define HALFX 2
#define GAPCLOSURE 3
#define AUTOMATIC 4
#define ANOTHER 5
#define seqtypenum 6
#ifdef MAIN
char *seqtype[] =  { "NONE", "Full_X", "Half_X", "Gap_closure", "Automatic", 
                     "Another_purpose"};
char *seqtype1[] =  { "NONE", "Full_X", "Half_X", "Gap_cl", "Auto", "Anothe"};
#endif

/* on fpc_load, just checs first two char */
#define TILE 1
#define SENT 2
#define READY 3
#define SHOTGUN 4
#define FINISHED 5
#define SD 6    /* simulated digest cari 25feb04 */
#define CANCELLED 7 /* for cancelled clones */
#define seqstatnum 8
#ifdef MAIN
char *seqstat[] = {"NONE","TILE","SENT","READY","SHOTGUN", "FINISHED","SD", "CANCELLED"};
#endif

/********************* CLONES *********************/
/* if change here, change in cloneedit.c *****/
#define NUM_TYPES 6
#define TYPECLONE 0
#define TYPEYAC 1
#define TYPEBAC 2
#define TYPEPAC 3
#define TYPECOSMID 4
#define TYPEFOSMID 5
#define TYPESEQ 6      /*fred 3/3/03*/
#define clonetypenum 7

#ifdef MAIN
char *clonetype[]= {"Clone","YAC","BAC","PAC","Cosmid","Fosmid","Seq"};
#endif

/****************** MARKERS *************************/
#define markREP 19
#define markOVERGO 18
#define markRFLP 17
#define markSSR 16
#define markTC 15
#define markeMRK 14
#define markeBAC 13
#define markSNP 12
#define markPCR 11
#define markCOSMID 10
#define markCDNA 9
#define markPAC 8 
#define markFOSMID 7
#define markBAC 6
#define markCLONE 5
#define markYAC 4
#define markSTS 3
#define markEND 2
#define markPROBE 1
#define markLOCUS 0
#define markertypenum 20

#ifdef MAIN
char *markertype[] = {"Locus","Probe","End","STS","YAC","Clone","BAC",
                 "Fosmid","PAC","cDNA","Cosmid", "ePCR", "SNP","eBAC","eMRK",
                 "TC","SSR","RFLP","OVERGO","REP"};
char* seq_type_names[] = {"Draft","Clone","BES"};

#endif



/********************* SEARCH ****************************/

/* Marker and clone searches */
#define SeaName 1
#define SeaDateCreateBefore 2
#define SeaDateCreateAfter 3
#define SeaDateModBefore 4
#define SeaDateModAfter 5
#define SeaRemark 6
#define SeaNotRemark 7
#define lastGeneralSea 7

/* Clone specific searches */
#define SeaGel 8
#define SeaFP 9
#define SeaCtg 10
#define SeaGtNbands 11
#define lastCloneInputSea 11

#define SeaSingle 12
#define SeaMultFp 13
#define SeaNoFp 14
#define SeaSelect 15
#define SeaCancel 16
#define SeaFull 17
#define SeaHalf 18
#define SeaGap 19
#define SeaContaminated 20
#define lastCloneSea 20

/* Marker specific searches */
#define SeaMultCtg 20
#define SeaNoCtg 21
#define Sea10Clones 22
#define SeaType 23
#define lastSea 24

#ifdef MAIN
char *searchname[]={"neverused","Name",
        "Created before","Created after",
        "Modified before","Modified after",
        "Remark","!Remark",
        "Gel Identifier","Clone Name","Contig", "> # Bands", "Singletons",
        "Mult Fingerprints", "No Fingerprints", 
        "Selected", "Cancelled", "Full_X", "Half_X", "Gap_closure",
        "> N Contigs", "<= N Contig", "> N Clones", "MarkerType" 
        };
#endif


