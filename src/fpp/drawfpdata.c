/***********************************************************************
                        drawfpdata.c
written by IL
edited by CAS

fphigh is index into cloneindex of high clone
 cloneindex[fphigh] = clhigh->next if clhigh!=NULL
cloneindex[FPMAX] are indices into arr(acedata,i, CLONE)
clonefpindex[FPMAX] indicates which Gel of clone
************************************************************************/
#include "clam.h"
#include <gtk/gtkwidget.h>

#define MATCH_BAND RED       /* bands matching highlighted clone */
#define HIGH_NBANDS BLACK      /* bands not-matched for highlighted clone */
#define HIGH_PBAND BLUE        /* bands matched for highlighted clone */
#define HIGH_PBAND_ALT GREEN  /* bands matched for highlighted clone */
#define HIGH_NAME BLUE        /* name of highlighted clone */
#define HIGH_L_MATCH DARKGREEN /* mtp colors...*/
#define HIGH_R_MATCH BLUE
#define HIGH_LR_MATCH CYAN
#define HIGH_NO_MATCH ORANGE
#define HIGH_FLANK VIOLET/* bands matched for left (right), flanker clone (mtp) */

extern GtkWidget *ctg_window;

int Zbcm(), ClamShare();
int Altflag=0, Altbox;

void fpaddoption(void);
int getcolourbox(CLONE *clone);
void recalculorder(void);
extern void refresh_gtk_ctgdisplay();

 
typedef struct markerstuff {
  char message[5];
  int value;
} MARKERSTUFF;

#define TOLERANCE 0
#define MATCH 1
#define NONE 2
static int ShowLabel=0; /* 0 = name, 1=Gel, 2=Ctg */

BOOL SCmarker=FALSE;
static Array SCmarkerValues=0;
static int rotate=0;

/***********************************************************
                 DEF: displayFPmtp
************************************************************/
void displayFPmtp()
{
  clonefpindex[0]=clonefpindex[1]=clonefpindex[2]=clonefpindex[3]=clonefpindex[4]=0;
  toler=2;
  fphigh = 2;
  drawfpdata(0);
}
/***************************************************************
                 DEF: loadmarker
SC special: load the marker values used in Image in the first column
****************************************************************/
void loadmarker()
{
  MARKERSTUFF ms;
  int i=0;
  FILE *mp;
  char junk[256];

  if(!SCmarker){
    if((mp = fopen("/nfs/disk8/scanner/fpc/special.data","r")) == NULL ){
      printf("file special data does not exist?\n");
      return ; 
    }
    
    SCmarker = TRUE;
    SCmarkerValues = arrayCreate(100,MARKERSTUFF);
    while(fgets(junk,255,mp) != NULL){ 
      sscanf(junk,"%d %s",&ms.value,ms.message);
      array(SCmarkerValues,i++,MARKERSTUFF) = ms;
      strcpy(ms.message,"   ");
    }
  }
  else{
    arrayDestroy(SCmarkerValues);
    SCmarker = FALSE;
  }
  drawfpdata(0);
}

void settoler(char *temp)
{
  sscanf(temp,"%d",&Pz.tol);
  ClamShare(0);
  drawfpdata(0);
}
static void toler0()
{
  toler=0;
  drawfpdata(0);
}
static void toler1()
{
  toler=1;
  drawfpdata(0);
}
static void toler2()
{
  toler=2;
  rotate = -1;
  drawfpdata(0);
}
static void tolLabelcallback()
{
  if (toler==2) toler0();
  else if (toler==1) toler2();
  else if (toler==0) toler1();
}

static void ShowLabelcallback()
{
  if (ShowLabel==2) ShowLabel=0;
  else ShowLabel++;
  drawfpdata(0);
}
static void show0()
{
  ShowLabel=0;
  drawfpdata(0);
}
static void show1()
{
  ShowLabel=1;
  drawfpdata(0);
}
static void show2()
{
  ShowLabel=2;
  drawfpdata(0);
}

/************************************************************
                   DEF: redrawfpdata
redraws fingerprints in contig order
************************************************************/
void redrawfpdata()
{
  int i,j=0;
  int index,old=-1;
  struct contig *pctg;
  int newcloneindex[FPMAX],newfpindex[FPMAX];

  recalculorder();
  if(ctg_window!=NULL){
    refresh_gtk_ctgdisplay(); /* fred 16.5.4 */
  }
  if (fphigh != -1) old = cloneindex[fphigh];
  fphigh = -1;

         /* for each clone in contig, go thru fp indices */
  for (pctg = root; pctg!=NULL; pctg = pctg->new){
    i=0;
    index=cloneindex[i++];
    while(index != -1){
      if(pctg->next == index){
        if(old == index) fphigh = j;
	newcloneindex[j] = index;
	newfpindex[j++] = clonefpindex[i-1];
	index = -1;
      }
      else
	index = cloneindex[i++];
    }
  }
  if(j>0){
    for(i=0;i<j;i++){
      cloneindex[i] = newcloneindex[i];
      clonefpindex[i] = newfpindex[i];
    }
    for(i=j;i<FPMAX;i++)
      cloneindex[i] = -1;
  }
  drawfpdata(1); /* centre on high */
}
/**********************************************************
                DEF: drawfpdata
***********************************************************/
void drawfpdata(int centre)
{
  int kk; /* do not use k for loop variable */
  int i,j,m;
  CLONE clone;
  int gelmin=99999, last, width,numcol,increment,inc1, height2;
  float widthpercolomn,xpos,ypos;
  int availheight;
  BOOL found,new;
  int  i1, i2;
  char str1[CLONE_SZ],str2[CLONE_SZ],str3[CLONE_SZ];
  float y,xpt,old;
  float boxscalestart,boxscalelen,fpscale;
  float ytop=0,ybot=0,botscrollstart=0,botscrolllen=0,hblength=0,x=0;
  int totalposs=0,idiff=0,reqbox=0,num=0;
  int start, end;
  static char chtolerance[10];
  BOOL hozbar;
  static MENUOPT fpmenu[] = {
  { fpquit, "Close"},
  { graphPrint,"Print Screen"},
#ifdef SANGER
  { loadmarker,"Special"},
#endif
  { 0, 0 } };
  int showoptions, toloptions;
  static MENUOPT showMenu[] = {
     { (VoidRoutine)show0,"Show Name..."},
     { (VoidRoutine)show1,"Show Gel Index..."},
     { (VoidRoutine)show2,"Show Ctg..."},
     {0,0} };
  static char showlabel[3][20] = {"Show Name...","Show Gel Index...","Show Ctg..."};
  static MENUOPT tolMenu[] = {
     { (VoidRoutine)toler0,"None..."},
     { (VoidRoutine)toler1,"Tolerance..."},
     { (VoidRoutine)toler2,"MTP..."},
     {0,0} };
  static char tollabel[3][20] = {"None...", "Tolerance...","MTP..."};
  int glw, b;
  int leftcnt=0, sharecnt=0, rightcnt=0;
static int whichleft[NBANDS], highBand[NBANDS], leftBand[NBANDS], rightBand[NBANDS], 
           lflankBand[NBANDS], rflankBand[NBANDS];
static struct fpdata *fp=NULL,*highfpdata=NULL, *leftfpdata=NULL, *rightfpdata=NULL,
                     *lflankfpdata=NULL, *rflankfpdata=NULL;
static CLONE *c1, *c2, *c3, *clflank, *crflank;

  if (cloneindex[0]== -1) return; /* can this happen ? */
  numcol = FPMAX-1; /* case all used */

  for(i=0;i<FPMAX;i++){                  /* get the number of colomns. */
    if(cloneindex[i] == -1){
      numcol = i;
      found = TRUE;
      break;
    }
  }
  
  if(graphActivate(g3)){
    graphClear();
    new = FALSE;
  }
  else{	
    rotate = -1;
    new= TRUE;
    g3 = graphCreate (TEXT_FIT,"FingerPrints",.2,.2,.4,.8) ;  
    graphRegister(PICK, mapmove);
    graphRegister(RESIZE,drawfpdata);
    graphMenu (fpmenu) ;
  }
  graphButton("Whole", fpwhole, 1.0, 1.0);
  graphText("Zoom:", 8.0, 1.0);
  graphButton("In", fpzoomin, 13.0, 1.0);
  graphButton("Out", fpzoomout, 17.0, 1.0);

  showoptions = graphButton(showlabel[ShowLabel],ShowLabelcallback, 23.0, 1.0);
  graphBoxMenu(showoptions, showMenu);

  fpmovebox = graphButton("Move",fpmoveoption,1.0,3.0);
  if(fpmove) graphBoxDraw(fpmovebox,BLACK,RED);
  fpremovebox = graphButton("Remove",fpremove,7.0,3.0);
  if(deletefp) graphBoxDraw(fpremovebox,BLACK,RED);
  fpaddbox = graphButton("Add",fpaddoption,15.0,3.0);
  if(fpadd) graphBoxDraw(fpaddbox,BLACK,RED);

  graphButton("ReDraw",redrawfpdata,1.0,5.0);
  graphButton("Remove All",fpclear,9.0,5.0);

  fpcolourbox = graphBoxStart();
  graphArc(23.0, 3.5, 0.7, 0, 360); 
  if (fpcolour) graphFillArc(23.0, 3.5, 0.4, 0, 360);
  graphBoxEnd();
  graphBoxDraw(fpcolourbox, BLACK, TRANSPARENT);
  graphText("Colour",25.0,3.0);

  Altbox = graphBoxStart();
  graphArc(33.0, 3.5, 0.7, 0, 360); 
  if (Altflag) graphFillArc(33.0, 3.5, 0.4, 0, 360);
  graphBoxEnd();
  graphBoxDraw(Altbox, BLACK, TRANSPARENT);
  graphText("HighGreen",35.0,3.0);

  toloptions = graphButton(tollabel[toler],tolLabelcallback, 23.0, 5.0);
  graphBoxMenu(toloptions, tolMenu);

  itoa(Pz.tol,chtolerance);
  reqbox = graphTextEntry(chtolerance, 3, 39.0, 5.0,settoler);

  glw = graphLinewidth(-1);
  widthpercolomn = FPWIDTHPERCOLOMN;  /* work out the width per colomn NOT*/
  graphFitBounds(&width,&height2);   /* get the width and height of the box/graph */
  fpheight = (float)height2 - 1.0;/* subtract 1 to give border at the bottom */
  
  totalposs = (int)(((float)width - FPSCALEWIDTH)/widthpercolomn);
  if(numcol > totalposs) hozbar = TRUE;
  else hozbar = FALSE;
  
  if(fphigh >=0){
    if(cloneindex[fphigh] < 0)
      fphigh = -1;
  }
  if(hozbar){
    if(fphigh > -1 && centre) {
      int x;
      if ((fpcolstart+totalposs)%2==1) x= fpcolstart+totalposs-1;
      else x = fpcolstart+totalposs;
      if (fphigh<=fpcolstart || fphigh >= x)
         fpcolstart = fphigh-(int)(totalposs/2);
    }
    if(fpcolstart+totalposs > numcol)
       fpcolstart = numcol - totalposs;
    else if (fpcolstart<0) fpcolstart=0;
  }
  else fpcolstart = 0;
  
  /* get maximum value to be plotted */
  gelmax = 1; gelmin=99999;
  for(i=0;i<FPMAX;i++){
      if(cloneindex[i] ==-1 ) continue;
      clone = arr(acedata, cloneindex[i], CLONE);
      if(clone.fp == NULL) continue;
      fp = clone.fp;
      for(j=0;j<clonefpindex[i];j++) fp = fp->next;
      for(j=0;j<fp->b2;j++){
	  b = arr(bands,fp->b1+(j-1),int);
          gelmax = MaX(gelmax, b);
          gelmin = MiN(gelmin, b);
      }
  }
  
  if(!SCmarker) ytop = (gelmin/100)*100;
  else ytop = 0;
  
  availheight = fpheight - (FPBUTTONHEIGHT + 1.0);  /* add one for bottom border*/
  fpscale = ((float)availheight /(float)(gelmax-ytop)) * FPZOOM; /* calculate the scale */
  if(FPYSHIFT < 0.0) FPYSHIFT = 0.0;
  else if(FPYSHIFT + FPBUTTONHEIGHT + ((float)availheight/FPZOOM) > fpheight) 
    FPYSHIFT = fpheight - ( FPBUTTONHEIGHT + ((float)availheight/FPZOOM));
  
  if(!SCmarker) kk = 0;
  else{
    kk =3;
    xpos = FPSCALEWIDTH;
    old = graphTextHeight(0.2);
    for(i=0;i<arrayMax(SCmarkerValues);i++){
      ypos = ((float)arrp(SCmarkerValues,i,MARKERSTUFF)->value* fpscale ) + 
                FPBUTTONHEIGHT - (FPYSHIFT*FPZOOM);
      if( ypos > FPBUTTONHEIGHT){
	graphLine(xpos,ypos,xpos+(widthpercolomn-1.0),ypos);
	graphText(arrp(SCmarkerValues,i,MARKERSTUFF)->message,
             xpos+widthpercolomn,ypos-0.25);
      }
    }
    old = graphTextHeight(old);
  }

/*** Tolerance, MTP or None ***/
   if (toler==0 || fphigh < 0) goto TOL0;
   if (toler==2) goto TOL2;

   highfpdata = arr(acedata,cloneindex[fphigh],CLONE).fp;
   for(m=0;m<clonefpindex[fphigh];m++)
	highfpdata = highfpdata->next;

     /* for the highlighted clone, highlight bands correctly */
   for(m=0;m<highfpdata->b2;m++) highBand[m]=HIGH_NBANDS;

   for(i=fpcolstart;i<fpcolstart+totalposs;i++){
       if(cloneindex[i] == -1) continue;

       fp = arr(acedata, cloneindex[i], CLONE).fp; 
       for (m=0;m<clonefpindex[i];m++) fp = fp->next;
       if(fp == NULL || fp == highfpdata) continue;

       m = start = highfpdata->b1 - 1;
       end =  highfpdata->b1 + highfpdata->b2 - 1 ;

       for(j= -1; j< fp->b2-1; j++){   
          for(b=start; b<end; b++){
	      idiff=arr(bands,(fp->b1+j),int)-arr(bands,b,int);
	      if(Ztol(arr(bands,b,int), idiff)) {
                   highBand[b - m] = (Altflag) ? HIGH_PBAND_ALT : HIGH_PBAND;
                   start = b + 1;
                   break;
              }
              else if (idiff < 0)
              {
                   start = b;
                   break;
              }
          }
       }
    }
    goto TOL0;

/* MTP find clone to left and right of high clone */
TOL2: ;
   if (fphigh>0) {
     leftfpdata = arr(acedata, cloneindex[fphigh-1], CLONE).fp; 
     for(m=0;m<clonefpindex[fphigh-1];m++) leftfpdata = leftfpdata->next;
     for(m=0;m<leftfpdata->b2;m++) leftBand[m]=HIGH_NO_MATCH;
     c1 = arrp(acedata, cloneindex[fphigh-1], CLONE);
   }
   highfpdata = arr(acedata,cloneindex[fphigh],CLONE).fp;
   for(m=0;m<clonefpindex[fphigh];m++) highfpdata = highfpdata->next;
   for(m=0;m<highfpdata->b2;m++) {
       highBand[m]=HIGH_NO_MATCH;
       whichleft[m]=0;
   }
   c2 = arrp(acedata, cloneindex[fphigh], CLONE);
   if (cloneindex[fphigh+1] != -1) {
     rightfpdata = arr(acedata,cloneindex[fphigh+1],CLONE).fp;
     for(m=0;m<clonefpindex[fphigh+1];m++) rightfpdata = rightfpdata->next;
     for(m=0;m<rightfpdata->b2;m++) rightBand[m]=HIGH_NO_MATCH;
     c3 = arrp(acedata, cloneindex[fphigh+1], CLONE);
   }

   /* Flanking clones */
   if(fphigh-1>0){
     lflankfpdata = arr(acedata, cloneindex[fphigh-2], CLONE).fp; 
     for(m=0;m<clonefpindex[fphigh-2];m++) lflankfpdata = lflankfpdata->next;
     for(m=0;m<lflankfpdata->b2;m++) lflankBand[m]=HIGH_NBANDS;

     clflank = arrp(acedata, cloneindex[fphigh-2], CLONE);
   }
   if(cloneindex[fphigh+2] != -1){
     rflankfpdata = arr(acedata, cloneindex[fphigh+2], CLONE).fp; 
     for(m=0;m<clonefpindex[fphigh+2];m++) rflankfpdata = rflankfpdata->next;
     for(m=0;m<rflankfpdata->b2;m++) rflankBand[m]=HIGH_NBANDS;

     crflank = arrp(acedata, cloneindex[fphigh+2], CLONE);
   }

   if (highfpdata==NULL) goto TOL0;

/* MTP find left, center and right sets of bands */
   if(leftfpdata!=NULL){
     start = -1;
     for(j= -1; j< leftfpdata->b2-1; j++)
     {   
	 for(b= start; b<highfpdata->b2-1; b++)
	 {
	    idiff=(arr(bands,(leftfpdata->b1+j),int)-
		   arr(bands,(highfpdata->b1+b),int));
	    if(abs(idiff) <= Pz.tol) {
		 highBand[b+1] = HIGH_L_MATCH;
		 leftBand[j+1] = HIGH_L_MATCH;
		 whichleft[b+1] = j+1;
		 start = b+1;
		 leftcnt++;
		 break;
	     }
	     else if (idiff < 0)
	     {
		 start = b;
		 break;
	     }
	 }
     } 
   }

   if(rightfpdata!=NULL){
     start = -1;
     for(j= -1; j< rightfpdata->b2-1; j++){   
	 for(b= start; b<highfpdata->b2-1; b++)
	 {
	    idiff=(arr(bands,(rightfpdata->b1+j),int)-
		   arr(bands,(highfpdata->b1+b),int));
	    if(abs(idiff) <= Pz.tol) {
		 if (highBand[b+1] == HIGH_L_MATCH) {
		     highBand[b+1] = HIGH_LR_MATCH;
		     rightBand[j+1] = HIGH_LR_MATCH;
		     leftBand[whichleft[b+1]] = HIGH_LR_MATCH;
		     leftcnt--; sharecnt++;
		 }
		 else {
		     highBand[b+1] = HIGH_R_MATCH;
		     rightBand[j+1] = HIGH_R_MATCH;
		     rightcnt++;
		 }
		 start = b + 1;
		 break;
	     }
	     else if (idiff < 0)
	     {
		 start = b;
		 break;
	     }
	 }
     } 
   }

   /* Flanking clones*/
   if(lflankfpdata!=NULL){
     start = -1;
     for(j= -1; j< lflankfpdata->b2-1; j++){   
	 for(b= start; b<leftfpdata->b2-1; b++)
	 {
	    if (leftBand[b+1] == HIGH_NO_MATCH){
	      idiff=(arr(bands,(lflankfpdata->b1+j),int)-
		     arr(bands,(leftfpdata->b1+b),int));
	      if(abs(idiff) <= Pz.tol) {
		   leftBand[b+1] = HIGH_FLANK;
		   lflankBand[j+1] = HIGH_FLANK;
		   start = b + 1;
		   break;
	       }
	       else if (idiff < 0)
	       {
		   start = b;
		   break;
	       }
	    }
	 }
     } 
   }

   if(rflankfpdata!=NULL){
     start = -1;
     for(j= -1; j< rflankfpdata->b2-1; j++){   
	 for(b= start; b<rightfpdata->b2-1; b++)
	 {
	    if (rightBand[b+1] == HIGH_NO_MATCH){
	      idiff=(arr(bands,(rflankfpdata->b1+j),int)-
		     arr(bands,(rightfpdata->b1+b),int));
	      if(abs(idiff) <= Pz.tol) {
		   rightBand[b+1] = HIGH_FLANK;
		   rflankBand[j+1] = HIGH_FLANK;
		   start = b + 1;
		   break;
	       }
	       else if (idiff < 0)
	       {
		   start = b;
		   break;
	       }
	    }
	 }
     } 
   }

/*
   printf(">> %s %s %s \n", c1->clone, c2->clone, c3->clone);
   printf("Match (blue  red   green) %2d %2d %2d\n",leftcnt, sharecnt, rightcnt);
   for(rightcnt=m=0;m<rightfpdata->b2;m++) 
       if (rightBand[m]==HIGH_NO_MATCH) rightcnt++;
   for(sharecnt=m=0;m<highfpdata->b2;m++) 
       if (highBand[m]==HIGH_NO_MATCH) sharecnt++;
   for(leftcnt=m=0;m<leftfpdata->b2;m++)
       if (leftBand[m]==HIGH_NO_MATCH) leftcnt++;
   printf("Black (left center right) %2d %2d %2d\n", leftcnt, sharecnt, rightcnt);
*/
        
TOL0: ;
    if(fpcolstart !=0)
         for(i=0;i<fpcolstart;i++)
              indexbox[i]=-1;
      
    for(i=fpcolstart;i<fpcolstart+totalposs;i++){
        if(cloneindex[i] == -1) continue;

        clone = arr(acedata, cloneindex[i], CLONE); 
        xpos = FPSCALEWIDTH + ((float)(kk++)*widthpercolomn); 
        indexbox[i] = graphBoxStart();                      

        /* PRINT NAME */
        old = graphTextHeight(0.2);
        if (ShowLabel==0) {
             for (i1=0;i1<3 && clone.clone[i1]!='\0';i1++) 
                 str3[i1] = clone.clone[i1];
             str3[i1]='\0';
             for (i2=0;i1<6 && clone.clone[i1]!='\0';i1++) 
                  str2[i2++] = clone.clone[i1];
             str2[i2]='\0';
             for (i2=0;i1<9 && clone.clone[i1]!='\0';i1++) 
                  str1[i2++] = clone.clone[i1];
             str1[i2]='\0';
         }
         else {
             if (ShowLabel==1) sprintf(str3,"%-4d",clonefpindex[i]+1);
             else sprintf(str3,"%-4d",clone.ctg);
             for (i1=0;i1<4 && clone.clone[i1]!='\0';i1++) 
                 str2[i1] = clone.clone[i1];
             str2[i1]='\0';
             for (i2=0;i1<8 && clone.clone[i1]!='\0';i1++) 
                  str1[i2++] = clone.clone[i1];
             str1[i2]='\0';
         }
	 fp = clone.fp;                    
	 for(m=0;m<clonefpindex[i];m++) fp = fp->next;
         if (fphigh!=-1)
             if (cloneindex[fphigh] == cloneindex[i])
	          graphColor(HIGH_NAME);
	 if(highfpdata == fp && toler==1) graphColor(HIGH_NAME);
	 else if(highfpdata == fp && toler==2) graphColor(PALEBLUE);
	 else if(leftfpdata == fp && toler==2) graphColor(BLUE);
	 else if(rightfpdata == fp && toler==2) graphColor(BLUE);
	 else if(lflankfpdata == fp && toler==2) graphColor(PALEGRAY);
	 else if(rflankfpdata == fp && toler==2) graphColor(PALEGRAY);
	 if(fpcolour){
	    if(currentctg == clone.ctg){
	      num = getcolourbox(&clone);
	      if(num == 0) num= BLACK;    
	    }
	    else num= BLACK;
	    graphColor(num);
	 }
	 if (str3[0]!='\0') graphText(str3,xpos,FPBUTTONHEIGHT - 3.0);
	 if (str2[0]!='\0') graphText(str2,xpos,FPBUTTONHEIGHT - 2.0);
	 if (str1[0]!='\0') graphText(str1,xpos,FPBUTTONHEIGHT - 1.0);
	 old = graphTextHeight(old);
	 graphColor(BLACK);

         /** PRINT BANDS **/

         last = 0;
	 for(j=-1;j<fp->b2-1;j++){       
            y = (float)(arr(bands,fp->b1+j,int) - ytop);
            ypos = (y* fpscale ) + FPBUTTONHEIGHT - (FPYSHIFT*FPZOOM);
            if (last == arr(bands,fp->b1+j,int)) {
               if (toler!=2) graphLinewidth(glw+0.3);
               else ypos += 0.1;
            } 
	    if(toler==1 && fphigh >= 0 ){
              if (highfpdata==fp) graphColor(highBand[j+1]);
              else { 
                 start = highfpdata->b1 - 1;
                 end =  highfpdata->b1 + highfpdata->b2 - 1 ;
	         for(b=start;b<end;b++){
	            idiff=(arr(bands,(fp->b1+j),int)-
                           arr(bands,b,int));
	            if(Ztol(arr(bands,b,int), idiff)) {
                         graphColor(MATCH_BAND);      
                         start = b+1;
		         break;
	            }
	            else if (idiff < 0) {
                         graphColor(BLACK);     
                         start = b;
                         break;
                    }
                    else graphColor(BLACK);     
                  }
	      }
	    }
            else if (toler==2 && fphigh >= 0) {
                 if (highfpdata==fp) graphColor(highBand[j+1]);
                 else if (leftfpdata==fp) graphColor(leftBand[j+1]);
                 else if (rightfpdata==fp) graphColor(rightBand[j+1]);
                 else if (lflankfpdata==fp) graphColor(lflankBand[j+1]);
                 else if (rflankfpdata==fp) graphColor(rflankBand[j+1]);
                 else graphColor(BLACK);     
            }
	    else graphColor(BLACK);

	   if(ypos > FPBUTTONHEIGHT &&  ypos < (float)fpheight)
	      graphLine(xpos,ypos,xpos+(widthpercolomn-1.0),ypos); 

           if (last == arr(bands,fp->b1+(j),int)) 
               graphLinewidth(glw);
           last = arr(bands,fp->b1+(j),int);
	} 
        graphColor(BLACK);     
	graphBoxEnd();
    } 

/* Vertical scale  */

    boxscalelen = (float)availheight/FPZOOM; 
    boxscalestart = FPYSHIFT +FPBUTTONHEIGHT; 
    xpt = FPSCALEWIDTH/2.0;        

    graphLine(1.0,FPBUTTONHEIGHT,1.0,fpheight); /* left line */ 
    graphLine(xpt,FPBUTTONHEIGHT,xpt,fpheight); /* right line */
    graphLine(1.0,boxscalestart,xpt,FPBUTTONHEIGHT);  /* top horizontal */
    graphLine(1.0,boxscalestart+boxscalelen,xpt,fpheight); /* bottome horizontal */

/* Draw the scaled up map */
    if (gelmax > 10000) inc1=2000;
    else inc1=200;

    if (FPZOOM!=1.0) {
      increment = inc1/FPZOOM;
      for(i=0;i<gelmax-ytop;i+=increment){
        y = (float)i * fpscale +FPBUTTONHEIGHT - (FPYSHIFT*FPZOOM);
        if(y > FPBUTTONHEIGHT && y< fpheight){
	  graphLine(xpt-0.5,y,xpt+0.5,y); 
	  itoa(i+ytop,str1);
	  graphText(str1,xpt+0.5,y-0.5);
        }
      }
    } 

 /* Draw normal (left) scale */
    for(i=0;i< (gelmax-ytop) +inc1;i+=inc1){
      y = ((float)i * (fpscale/FPZOOM)) +FPBUTTONHEIGHT ; 
      if (y < fpheight) {
        graphLine(1.0,y,2.0,y); 
        itoa(i+ytop,str1);
        graphText(str1,2.0,y-0.5);
      }
    }

/* green bar */
    band = graphBoxStart();
    graphRectangle(0.5,boxscalestart,1.5,boxscalestart+boxscalelen);
    graphBoxEnd();
    graphBoxDraw(band,BLACK,GREEN);

/* END SCALE BIT */

    /* draw horizontal scroll bar if NEEDED */
    if(hozbar){
                                          /* draw the background for the bar */
      ytop = fpheight;
      ybot = fpheight+1.0;
      hblength = (float)totalposs*3.2;
      sizepercol = hblength/(float)numcol;
      for(i=0;i<numcol;i++){
	x = FPSCALEWIDTH + ((float)i*sizepercol);
	graphRectangle(x,ytop,x+(sizepercol),ybot);
      }
      /* draw the scroll bar */
      botscrollstart = FPSCALEWIDTH + (fpcolstart*sizepercol);
      botscrolllen = ((float)totalposs/(float)numcol)*hblength;
      botscrollbox = graphBoxStart();
      graphRectangle(botscrollstart,ytop,botscrollstart+botscrolllen,ybot);
      graphBoxEnd();
      graphBoxDraw(botscrollbox,BLACK,GREEN);
    }
    else
      botscrollbox = -1;
  if (toler==2 && rotate==0) {
     highfpdata = leftfpdata = rightfpdata = lflankfpdata = rflankfpdata = NULL;
  }
  graphRedraw();
} 

