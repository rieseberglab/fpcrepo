/***********************************************************
                    fpstruct.h
written by IL
edited by CAS
*************************************************************/


#define NUMCTGPERPAGE 24 /* number of contigs diaplayed in the box */
#define FPPMAXCOL 50     /* maximum number of colomns for clones in contig */

#define FPMAX 500 /* number of fingerprints that can be displayed */
#define GELMAX 75 /* number of gels that can be displayed */

#define FPSCALEWIDTH 15.0 /* width of the scale bar in fp map */
#define FPBUTTONHEIGHT 10 /* size of the top of screen used for buttons on fp map */
#define FPSCALESIZE  12.0
#define FPWIDTHPERCOLOMN 3.2
#define BUTTONHEIGHT  6 /* size of the top of the contig map */
#define WIDTHSTART 3 /* number of units to left to be left blank on horiz display*/
#define SCALEWIDTH  8.0 /* width of the scale bar in the contig map */
#define MAXCONTIG 350 /* maximum number of contigs allowed */
#define REGNUM 2  /* number of region bars */
#define SCALEINIT 0.4 /* random guess */
#define MAXLEVEL 20
#define MAXCOL 20
#define COLWIDTH 3.0
#define NORMAL 0
#define SINGLE 1
#define MULTIPLE 2
#define DELCLONES 3

#define CTGCLASS 0
#define CLONECLASS 1
#define MARKERCLASS 2
#define SEQCLASS 5
#define EXPANDCONTIGS 3
#define EXPANDCLONES 4

/* marker edit stuff */

#define EDITMARKER 2
#define EDITCLONE 1
#define EDITREMARK 4
#define EDITNOTHING 0

#define UNCHANGED 0
#define DELETED 1
#define NEWISH 2
#define ADDED 3
#define NEW_THEN_DEL 4

/* end */

/* structures*/

typedef struct region {
  int band; /* coordinates of region bar in bands */
  float scr; /* as above but screen coors */
  int xsrc; /*fred 4/22/03 -- the position on the screen*/
} REGION ;

/* pos holds the y? coordinate of where the bullet goes */
typedef struct clonepos {
  int pos[10];
} CLONEPOS;

struct list {
  int index;               /* index of array acedata giving the clone in list */
  int box;                /* box number it is displayed in */
  int fpindex;           /* for the fpindex'th fp number */
  struct sequence* seq;
  struct list  *next;    /* pointer to next clone in list */
  };

struct preslist {
  int graph;
  struct preslist  *next;
  };

/* sness */
/* A "contig" is actually a clone in a contig with a pointer to */
/* the next clone.  This is most commonly used as in "contig *root" */
/* where "root" is the first clone in the contig we're looking at. */
struct contig {
  int next;               /* index of array acedata giving the clone in contig */
  int box;                /* box that clone is displayed in */
  int chbox;              /* box that contains the character's naming clone */
  int pos;                /* y position for bullet etc */
  int xpos;               /* colomn number */
  struct contig  *new;    /* pointer to next clone in contig */
  };

typedef struct texthigh {
  int project,markers,markerbox; /* il 10/7/95 */
  int ctg,clonebox;
  int markerbox2; /*il 11/7/95 */
} TEXTHIGH;

struct markerdisplist {
  int cloneindex;
  int high;
  int box;
  int type;
  struct markerdisplist *next;
};

/* edit stuff */


typedef struct editmarker{
  int box;
  int num;
  int status; /* use for clone adding,deleting etc */
  int type; /* i.e. ctg,x,y,remark,clone */
  int value; /* used for x, y and midpt and cloneindex*/
  struct remarker *remark; /* ptr to remarks for marker */
  struct markerctgpos *pos; /* ptr to ctg,x,y,midpt */
  char charbit[41];
  struct editmarker *next;
} EDIT;

typedef struct markereditlist{     /* I.E. markertop with status */
  int markerindex;
  char marker[MARKER_SZ+1];
  int status;
  struct markereditlist *nextmarker;
} MARKEREDITLIST;

/*
typedef struct cloneeditlist{
  int cloneindex;
  int status;
  struct cloneeditlist nextclone;
} CLONEEDITLIST;
*/
/* ednd edit stuff */
/* routine list */
void displayclone(int index);
void displaykeyset(int rc);
int getmaxmin(int *bot, int *top);
void point(int box, double x, double y);
void ctgdisplay(int ctg);
void ctgpick(int box, double x , double y);
int openandreadfile(void);
BOOL find_Clone(int ctg);
void whole(void);
void zoomin(void);
void zoomout(void);
void displayFpMessage(void);

void fpRegion(void);
void fpContig(void);
void fpSelected(void);
void addfp(int index,int fpindex);
void addgel(int index,int fpindex);
void gelredraw();

void fpwhole(void);
void fpzoomin(void);
void fpzoomout(void);
void itoa(int n, char s[]);

void fpquit(void);
void drawfpdata(int centre);
void fingerPrint(int box);
int findConIndex(int ctg);
void fpRegdisp(void);
void regquit(void);
int getcontigsmax(void);
/* sness */
/*void ftoa(float f,char s[]); */
void myAceDump(void);
void SetCtgForMatchingClones(void);
void sortcontigs(void);
void markerfreemem(Array markerArr);
void clonefreemem(Array cloneArr);
void ctgfreemem(void);
void botscrolldrag(float *x, float *y, BOOL isUP);
void banddrag(float *x, float *y, BOOL isUP);
void removefp(int box);
void mapdrag(float *x, float *y, BOOL isUp);
int fpRead(void);
BOOL setypshift(float *y);
void resetmode(void);
void pagedown(void);
void pageup(void);
void setendpts(struct contig *pctg,int i);
void mapmove(int box);
void fpmoveoption();
void fpremove();
void fpquit();
void fpclear();
void orderclones(int ctg);
void requestClone(char *temp);
void toggleburied(void);
int colload(struct contig *pctg);
int PositionClone(int x, int y,int *pt1, int *pt2);
BOOL movecolomn(float *x);
void setclonecmp(int box);
void settoler(char *temp);
void toggleinfo(void);
void togglefmap(void);
void togglescrcoor(void);
void showdebug(void);
void toggleall(void);
int getmaxlevel(void);
void configctgdisplay(void);
void reselect(int box,double x,double y);
void setcolwidth(char *temp);
void setcharmax(char *temp);
void setcolnum(char *temp);
void redrawctg(void);
void destroyMain(void);
void addNames(void);
void highlightfriends(BOOL high);

void analysis(void);
void savefile(void);
void drawmain(void);
void setclassctg(void);
void setclassclone(void);
void listclass(void);
void addclonename(void);
void nameaddition(char *temp);
void searchclass(char *temp);
void fpnumadd(char *temp);
void nextclone(void);
void doneadd(void);
void loadfile(void);
void listctgscreen(void);
void listclonescreen(void);
void getclonelist(void);
void displayclonelist(void);
void menu_main(void);
void unattachedclones(void);
void multiplefpnum(void);
void unusedfpnum(void);
void addclonemovecursor(int box,double x, double y);
void hozctgdisplay(int ctg);
void reversefpnums(void);
void message_box();
void main_menu(void);
void gelPrint(int box);
void getlist(void);
void displaymess(char *temp);
int readace(FILE *fpace);
void addNames(void);
void pMapShowSegs(int ctg);

BOOL fppFind(Array a, char *s, int *ip, BOOL (* order)());
BOOL fppInsert(Array a, char *s, int *index, BOOL (*order)());
BOOL cloneOrder(void *s, void *b);
BOOL markerOrder(void *s, void *b);

