/***********************************************************
                 getlist.c
written by IL
seriously edited by CAS

creates the list to be displayed in the keyset window
***********************************************************/
#include "fpp.h"
#include <gtk/gtkwidget.h>

extern int PRTMESS;
extern GtkWidget *ctg_window;

extern void mrkremsearch();
extern void mrknotremsearch();
extern void refresh_all_track_colors();
extern void clear_listroot();

void markersearch(void);
void clonenamesearch(void);
void searchEngine();
void listMarkerType();

int NewSearchClass=1;

void freelistmem()
{
  struct list *p,*temp;

  p= listroot;
  while(p!=NULL){
    temp=p;
    p=p->next;
    if(!messfree(temp))
      printf("error freeing list memory\n");
  }
  listroot = NULL;
}

/*****************************************************************************
                         DEF: refreshlist
*****************************************************************************/
void refreshlist()
{
int save = page;

       /* FIX quite trying to fix */
    return;

    if (searchtype==1) getlist();
    if (!graphExists(g2)) return;

    PRTMESS=0;
    if (searchtype==1 && 
        (strcmp(chsearchglob,"*")==0 || chsearchglob[0]=='\0'))
             getlist();
    else searchEngine();
    PRTMESS=1;
    page = save;
    displaykeyset(1);
}
/***************************************************************
                  DEF: removespaces
****************************************************************/
void removespaces(char *temp)
{
  char temp2[NAME_SZ];
  register int i,j;

  if(searchtype >=2 && searchtype <= 5) return; /* FIX 13june99 */
  if((searchtype == SeaRemark) || (searchtype == SeaNotRemark)) return;  /*fred 11/13/02*/
  
  for(i=0,j=0;i<=strlen(temp);i++){
    if(temp[i]!= ' '){
      temp2[j++]=temp[i];
    }
  }
  strcpy(temp,temp2);
}
/***************************************************************
                  DEF: searchEngine
called from main menu on Search or hitting <CR> in yellow box
****************************************************************/
void searchEngine()
{
void subcontigsearch(), subclonesearch(), remsearch(), notremsearch(), gelsearch();
void FPsearch(), ctgclonesearch(), creationdates_clone(), clonesnofp(), searchselected();
void listdeleted(), submarkersearch(), creationdates_marker(), listnocontigs(), listmultiplecontigs();
void gtNbands();
void list10clones();

  if (classctg == SEQCLASS) {
    clear_listroot();
    classctg = main_classctg;
  }

  if (!dataloaded) return;
  if (classctg==-1) return;
  if (NewSearchClass || !graphExists(g2)) getlist(); 

  removespaces(chsearchglob);


  if(classctg == CTGCLASS){
    if(searchtype ==SeaName)
      subcontigsearch();
  }
  else if(classctg == CLONECLASS){
    if(searchtype ==SeaName) subclonesearch();
    else if(searchtype ==SeaRemark) remsearch();
    else if(searchtype == SeaNotRemark) notremsearch();
    else if(searchtype == SeaGel) gelsearch();
    else if(searchtype == SeaFP) FPsearch();
    else if(searchtype== SeaCtg) ctgclonesearch();
    else if(searchtype == SeaDateCreateBefore) creationdates_clone(-1,TRUE);
    else if(searchtype == SeaDateModBefore) creationdates_clone(-1,FALSE);
    else if(searchtype == SeaDateCreateAfter) creationdates_clone(1,TRUE);
    else if(searchtype == SeaDateModAfter) creationdates_clone(1,FALSE);
    else if (searchtype==SeaSingle) unattachedclones();
    else if (searchtype==SeaMultFp)  multiplefpnum();
    else if (searchtype==SeaNoFp) clonesnofp(0);
    else if (searchtype==SeaSelect) searchselected();
    else if (searchtype==SeaCancel) listdeleted();
    else if (searchtype==SeaGtNbands) gtNbands();
  }
  else{
    if(searchtype == SeaName) submarkersearch();
    else if(searchtype == SeaDateCreateBefore) creationdates_marker(-1,TRUE);
    else if(searchtype == SeaDateCreateAfter) creationdates_marker(1,TRUE);
    else if(searchtype == SeaDateModBefore) creationdates_marker(-1,FALSE);
    else if(searchtype == SeaDateModAfter) creationdates_marker(1,FALSE);
    else if(searchtype ==SeaRemark) mrkremsearch();   
    else if(searchtype ==SeaNotRemark) mrknotremsearch();  
    else if (searchtype==SeaNoCtg) listnocontigs();
    else if (searchtype==SeaMultCtg)  listmultiplecontigs();
    else if (searchtype==Sea10Clones)  list10clones();
    else if (searchtype==SeaType)  listMarkerType();
  }
}

/*****************************************************************************
                         DEF: getlist
gets all items for class
*****************************************************************************/
void getlist()
{
  struct list *p=NULL,*oldroot,*temp;
  int i; 
  BOOL first;
  char tempglob[NAME_SZ];

  NewSearchClass=0;
  strcpy(tempglob,chsearchglob);
  strcpy(chsearchglob,"*");
  oldroot = listroot;
  listroot = NULL; 

  if(classctg == MARKERCLASS){
    markersearch();
  }
  else if(classctg == CLONECLASS){	/* if search for clone */		
    clonenamesearch();
  }
  else if(classctg == SEQCLASS){	
  }
  else{                 /* search for contigs */
      first = TRUE;
      for(i=1;i<=max_contig;i++){
	if(contigs[i].count != 0){ /*il ctg->count 19/07/96 */
	  if(first){
	    listroot  = (struct list *)messalloc((sizeof(struct list)));	
	    listroot->next = NULL;
	    p = listroot;
	    p->index = i;
	    first = FALSE;
	  }
	  else{
	    p->next = (struct list *)messalloc((sizeof(struct list)));	
	    p=p->next;
	    p->index = i;
	  }
	}
      }
      if(!first)
	p->next = NULL;
      else /* CAS 13 jan 98 - check PRTMESS to shut-up this message */
	if (PRTMESS) displaymess("No contigs Exist Yet."); 
  }

  if(listroot==NULL){
    listroot = oldroot;
    if(graphActivate(g2))
      graphDestroy();
  }
  else{
    page = 0;
    p= oldroot;
    while(p!=NULL){
      temp=p;
      p=p->next;
      if(!messfree(temp))
        printf("error freeing list memory\n");
    }
  }
  strcpy(chsearchglob,tempglob);
}

/****************************************************************/
void keysetselect()
{
  int count =0,index,start, cnt=0;
  struct list *p,*pstart;
  BOOL last = FALSE;
  CLONE *clone;

  if (listroot ==NULL) return;
  if(ctg_window==NULL) return;

  for (p=listroot; p!=NULL; p=p->next) count++;

  if(contigs[currentctg].count > count){
      start = contigs[currentctg].start;
      for (p=listroot; p!=NULL; p=p->next){
	 last = FALSE;
         index = contigs[currentctg].start;
	 while(!last){
	    clone = arrp(acedata,index,CLONE);
	    if(p->index == index){
	       start = index;
	       clone->selected = TRUE;
	       last = TRUE;
               cnt++;
	    }
	    else{
	       if(clone->next == -1) last=TRUE;
	       else index = clone->next;
	    }
	 }
      }
    }
    else{
      for (index = contigs[currentctg].start; index!=-1; index = clone->next)
      {
	 clone = arrp(acedata,index,CLONE);
	 p = listroot;
	 last = FALSE;
	 while(!last){
	    if(p->index == index){
	       pstart = p;
	       clone->selected = TRUE;
	       last= TRUE;
               cnt++;
	    }
	    else{
	       if(p->next != NULL) p=p->next;
	       else last=TRUE;
	    }
	 }
      }
   }
   printf("Highlight %d from Keyset of %d\n",cnt, count);
   refresh_all_track_colors();  /*fred 5/7/03*/
}
