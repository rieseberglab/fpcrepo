/*  Last edited: Jan 15 11:32 1997 (il) */
#include "fpp.h"

void setendpts(struct contig *pctg,int i)
{
  int j;
  struct contig *p1;

  p1 = pctg;
  for(j=0;j<=colnum;j++){
    endpoints[i][j] = arr(acedata, p1->next, CLONE).y;
    if(p1->new == NULL)
      break;
    else
      p1 = p1->new;
  }
}
    

int colload(struct contig *pctg)
{
  int i,j,k;
  BOOL found=FALSE;
  struct contig *p1;
  CLONE *clone;

   if(pctg == root){              /* reset values this is the start i.e. root */
    for(i=0;i< MAXLEVEL; i++){
      for(j=0;j<colnum;j++){
	endpoints[i][j] = INT_MIN;
      }
    }
  }    

  /* find the level where no colomns overlap */
  for(i=0;i< MAXLEVEL; i++){
   p1 = pctg;
    k = 0;
    for(j=0;k<colnum;j++){
      clone = arrp(acedata,p1->next,CLONE);
       found = TRUE;
/*      if( (showburied) || clone->mattype <= PARENT ){*/
      if(!showburied || clone->mattype <= PSPARENT || (showburied == 2 && clone->mattype != PSEUDO)){ 
	if(endpoints[i][k++] >= clone->x){
	  found = FALSE;           /* overlap therefore try next level */
	  break;
	}
	else if(p1->new == NULL){     /* less than colnum but no overlaps */
	  break;
	}
      }
      else if(p1->new == NULL){     /* less than colnum but no overlaps */
	break;
      }
      p1 = p1->new;
    }
    if(found){
      setendpts(pctg,i);
      return(i); /* all colnum are okay for this level */
    }
  }
  printf("ERROR ran out off levels reset MAXLEVEL to a higher value\n");
  return(-1);
}
