/**********************************************************
                 addfp.c
written by IL
edited by CAS
*************************************************************/
#include "fpp.h"
#include <gtk/gtkwidget.h>

extern GtkWidget *ctg_window;

/************************************************************
                DEF: finerprinthigh
called by Select Operation Menu
************************************************************/
void fingerprinthigh()
{
  struct contig *p, *save;
  BOOL draw;
  CLONE *clone;
  int i;

  if (!arrayExists(bands))
         if (fpRead() == -1) return;

  if(ctg_window==NULL)
    return;

     /* if highlighed clone in not in fp display, do not put fp's
        in front of it, but highlight it afterwards */
  if (fphigh <0) {
      save = clhigh;
      clhigh=NULL;
  }
  else save=NULL;

  draw = FALSE;
  for (p = root; p!=NULL; p = p->new){                             
    clone = arrp(acedata,p->next,CLONE);
           /* cari 4 feb 05 */
    if (showburied==1 && clone->match[0]!=' ') continue;
    if(clone->selected){
        addfp(p->next,0);
        draw = TRUE;
    }
  } 

  if(save!=NULL){
     clhigh=save;
     for(i=0;i<FPMAX;i++){                  
        if (clhigh!=NULL && clhigh->next == cloneindex[i]) {  
            fphigh = i;
            break;
        }
     }
  }
  if (draw) {
    drawfpdata(1);
    graphPop();
  }
  else fprintf(stderr,"No visable clones selected\n");
}

/*******************************************************************
                  DEF: addfp
******************************************************************/
void addfp(int index,int fpindex)
{
  int i;
  BOOL found;
  CLONE clone;
  char str1[60];

  clone = arr(acedata, index, CLONE);
  if(clone.fp == NULL){
     sprintf(str1,"Clone %s has no finger print data",clone.clone);    
     displaymess(str1);
     return;
  }
  if(!bands)                           /* If cor file has not been read */
     if(fpRead()<=0){                         /* then read it */
	  displaymess("*** no .cor file or could not read it\n");
	  return;
      }
        
  if(fphigh < 0){
     for(found=i=0;i<FPMAX && !found;i++){                  
	  if(cloneindex[i] == -1){           
	    cloneindex[i] = index;         
	    clonefpindex[i] = fpindex;
            if (clhigh!=NULL && clhigh->next == index)  
                fphigh = i;
            found=1;
	  }
     }
     if(!found){
        sprintf(str1,
          "*** maximum number of fingerprints to display %d has been exceeded",FPMAX);
        displaymess(str1);
     }
  }
  else{
	/* 1 move cmp and above  up one */
     for(i=FPMAX-1;i>=fphigh;i--){
	  cloneindex[i] = cloneindex[i-1];
	  clonefpindex[i] = clonefpindex[i-1];
     }
     cloneindex[fphigh] = index;
     clonefpindex[fphigh] = fpindex;
     fphigh++;
  }
}
