/*  Last edited: May  8 16:31 1996 (il) */
#include "fpp.h"

void clonefreemem(Array cloneArr)
{
	int i;
	int acemax;
	CLONE *clone;
	struct fpdata *p, *ptemp;
	struct remark *r, *rtemp;
	struct markertop *mp,*mptemp;

	if(!cloneArr)
		return;
	acemax = arrayMax(cloneArr);
	for(i=0;i<acemax;i++){
		clone = arrp(cloneArr,i,CLONE);
		p = clone->fp;
		while(p != NULL){
			ptemp = p->next;
			if(!(messfree(p))) {
				printf("error freeing fpdata memory\n");
			}
			p = ptemp;
		}
		clone->fp = NULL;

		r = clone->remark;
		while(r != NULL){
			rtemp = r->next;
			if(!(messfree(r))) {
				printf("error freeing remark memory\n");
			}
			r = rtemp;
		}
		clone->remark = NULL;

		r = clone->fp_remark;
		while(r != NULL){
			rtemp = r->next;
			if(!(messfree(r))) {
				printf("error freeing fp remark memory\n");
			}
			r = rtemp;
		}
		clone->fp_remark = NULL;

		mp = clone->marker;
		while(mp!=NULL){
			mptemp= mp->nextmarker;
			if(!(messfree(mp))) {
				printf("error freeing marker memory");
			}
			mp = mptemp;
		}
		clone->marker = NULL;

		SEQHIT * sh = clone->seq_hits;
		SEQHIT * tempsh = clone->seq_hits;
		while(sh != NULL) {
			tempsh= sh->next;
			if(!(messfree(sh))) {
				printf("error freeing clone sequence hits");
			}
			sh = tempsh;
		}
		clone->seq_hits = NULL;
	}
}

int clonefreemarkermem()
{
	int i;
	int acemax,j=0;
	CLONE *clone;
	struct markertop *mp,*mptemp;

	if(!acedata)
		return 0;
	acemax = arrayMax(acedata);
	for(i=0;i<acemax;i++){
		clone = arrp(acedata,i,CLONE);
		mp = clone->marker;
		while(mp!=NULL){
			j++;
			mptemp= mp;
			mp=mp->nextmarker;
			if(!(messfree(mptemp)))
				printf("error freeing marker memory");
		}
		clone->marker = NULL;
	}
	return j;
}

void ctgfreemem()
{
	if(root!= NULL){
		messfree(root);
		root= NULL;
	}
	clhigh = NULL;
}





