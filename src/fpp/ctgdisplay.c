/**************************************************************************
                         ctgdisplay
written by IL
edited by CAS
**************************************************************************/
#include "fpp.h"
#include <gtk/gtkwidget.h>
extern GtkWidget *ctg_window;

extern void redrawfingerprints(), gelhighlight();
extern void setCtgConfig();
extern void quitCB();
extern BOOL movedone;
extern int centreMarker, centreClone;
extern int qstuff;  /*efriedr 12/6/01*/
extern int gtk_ctgdisplay(); /*fred 3/3/03*/
void point2 (int box, double x, double y);
void pMapShowSegs(int ctg);

void selectOptions(void);
void fingerprinthigh(void);
void clearall(void);
int getcolourbox2(CLONE *clone);

/*************************************************
              ctgdisplay
***********************************************/
void ctgdisplay(int ctg)
{
  if(qstuff) return;  /*efriedr 12/6/01*/
  if(currentctg != ctg){
    newctg = TRUE;
    togglejustchanged = FALSE;
  }
       /* merge gets called with ctg=0 */
  if(ctg<=0) return; 

  if(displaymap){
    gtk_ctgdisplay(ctg);
  }
}

void hideNone(){
  showburied = 0;
  togglejustchanged = TRUE;
  ctgdisplay(currentctg);
}
void hideBuried(){
  showburied = 1;
  togglejustchanged = TRUE;
  ctgdisplay(currentctg);
}
void hidePseudo(){
  showburied = 2;
  togglejustchanged = TRUE;
  ctgdisplay(currentctg);
}
void hiddenToggle(){
  if(!showburied)
    hidePseudo();
  else if(showburied == 1)
    hideNone();
  else
   hideBuried();
}

int getmaxlevel()
{
  int count,level,maxlevel=0;
  struct contig *pctg;
  BOOL last;

  count =99;
  last = FALSE;
  pctg = root; 
  while(!last){                
    if(count >= colnum){
      level = colload(pctg);
      count = 1;
      if(level > maxlevel)
	maxlevel = level;
    }
    else{
      count++;
    }
    if(pctg->new==NULL) 
      last = TRUE;	
    pctg = pctg->new;
  }/* end while */
  return(maxlevel);
}  

void redrawctg()
{
  ctgdisplay(currentctg);
}
