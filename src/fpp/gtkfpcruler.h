/****************************************************
 * Modified by Fred Engler
 *
 * Modification: Code modified from the gtkruler widget.
 * Added two position markers, Selection/movement
 * of position markers is done by clicking and dragging.
 * Metric calculation has been modified to correspond to 
 * CB units/Base pairs for the current zoom level of the FPC contig
 * display.  When a position marker is clicked, a vertical line
 * is drawn across the display to make it easy to see how the
 * postion relates to FPC clones.  All modifications are commented
 * with 'fred'.
 ****************************************************/

/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#ifndef __GTK_FPC_RULER_H__
#define __GTK_FPC_RULER_H__


#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <stdlib.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_TYPE_FPC_RULER            (gtk_fpcruler_get_type ())
#define GTK_FPC_RULER(obj)            (GTK_CHECK_CAST ((obj), GTK_TYPE_FPC_RULER, GtkFpcRuler))
#define GTK_FPC_RULER_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_FPC_RULER, GtkFpcRulerClass))
#define GTK_IS_FPC_RULER(obj)         (GTK_CHECK_TYPE ((obj), GTK_TYPE_FPC_RULER))
#define GTK_IS_FPC_RULER_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_FPC_RULER))

enum rulerUnit {BASES, CBUNITS};  /*fred 8/6/03*/

typedef struct _GtkFpcRuler        GtkFpcRuler;
typedef struct _GtkFpcRulerClass   GtkFpcRulerClass;
typedef struct _GtkFpcRulerMetric  GtkFpcRulerMetric;

/* All distances below are in 1/72nd's of an inch. (According to
 * Adobe that's a point, but points are really 1/72.27 in.)
 */
struct _GtkFpcRuler
{
  GtkWidget widget;

  GdkPixmap *backing_store;
  GdkGC *non_gr_exp_gc;
  GdkGC *xor_gc;        /*fred 3/13/03*/
  GdkGC *active_pos_gc;        /*fred 6/10/03*/
  GdkGC *passive_pos_gc;
  GtkFpcRulerMetric *metric;
  gint xsrc1, ysrc1;
  gint xsrc2, ysrc2;    /*fred 2/26/03*/
  gint slider_size;

  /* The upper limit of the fpcruler (in points) */
  gfloat lower;
  /* The lower limit of the fpcruler */
  gfloat upper;
  /* fred 2/26/03 -- added 2 positions to ruler*/
  /* The position of the first mark on the fpcruler */
  gfloat position1;
  /* The position of the second mark on the fpcruler */
  gfloat position2;
  /* The position currently being repositioned fred 2/26/03*/
  gint selected_pos;
  /* The maximum size of the fpcruler */
  gfloat max_size;
  /* Adjustment for tick values w.r.t. the current zoom level*/
  gfloat zoom_adjust;  

  /* fred 8/6/03*/
  GtkWidget *units_popup_menu;
  enum rulerUnit ruler_units;
};

struct _GtkFpcRulerClass
{
  GtkWidgetClass parent_class;

  void (* draw_ticks) (GtkFpcRuler *fpcruler);
  void (* draw_pos)   (GtkFpcRuler *fpcruler);
};

struct _GtkFpcRulerMetric
{
  gchar *metric_name;
  gchar *abbrev;
  /* This should be points_per_unit. This is the size of the unit
   * in 1/72nd's of an inch and has nothing to do with screen pixels */
  gfloat pixels_per_unit;
  gfloat fpcruler_scale[10];
  gint subdivide[5];        /* five possible modes of subdivision */
};


GtkType gtk_fpcruler_get_type   (void);
void    gtk_fpcruler_set_metric (GtkFpcRuler       *fpcruler,
			         float metric);
void    gtk_fpcruler_set_range  (GtkFpcRuler       *fpcruler,
			      gfloat          lower,
			      gfloat          upper,
			      gfloat          position1,
			      gfloat          position2,
			      gfloat          max_size,
			      gfloat          zoom_adjust);
void    gtk_fpcruler_draw_ticks (GtkFpcRuler       *fpcruler);
void    gtk_fpcruler_draw_pos   (GtkFpcRuler       *fpcruler);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_FPC_RULER_H__ */
