/****************************************************
 * File: gtkhighlight.h
 *
 * Author: Fred Engler
 ****************************************************/

#include "gtktrack.h"

/*Function prototypes*/
void copy_to_save_pixmap(GtkTrack *t, int index);
void copy_from_save_pixmap(GtkTrack *t, int index);
void entity_highlight(GtkTrack *t, int index, enum highlightTypes mode);
void gtk_track_highlight_clear_colors(GtkTrack *t, enum highlightTypes clear_type);
void gtk_track_clear_all(GtkTrack *t, enum highlightTypes clear_type);
void clear_highlights();
int redo_highlights();
void clear_matches();
void refresh_track_colors(GtkTrack *t);
void refresh_all_track_colors();
int do_centre_stuff();
void gtk_track_highlight_clone_active(GtkTrack *t, int index);
void gtk_track_highlight_clone_passive(GtkTrack *t, int index);
void gtk_track_highlight_clone_match(GtkTrack *t, int index);
void gtk_track_highlight_clone_selected(GtkTrack *t, int index);
void gtk_track_highlight_marker_active(GtkTrack *t, int index);
void gtk_track_highlight_marker_passive(GtkTrack *t, int index);
void gtk_track_highlight_marker_match(GtkTrack *t, int index);
void gtk_track_highlight_remark_active(GtkTrack *t, int index);
void gtk_track_highlight_remark_passive(GtkTrack *t, int index);
void gtk_track_highlight_remark_match(GtkTrack *t, int index);
int gtk_track_highlight_matches(GtkTrack *t, char *pat, int *totmidpt);
void gtk_select_colored();
void gtk_track_highlight_sequence_active(GtkTrack *track, int index);
