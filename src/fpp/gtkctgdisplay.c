/****************************************************
 * File: gtkctgdisplay.c
 *
 * Author: Fred Engler
 *
 * Description: Contains the code that draws the contig
 * display, and provides its functionality.  The tracks
 * are stored in the 'tracks' array, and track_num
 * is the number of tracks in the display.
 ****************************************************/
int* pfoo;

#include "gtkctgdisplay.h"
#include <sys/timeb.h>
#include <gdk/gdkkeysyms.h>

extern void appendMarkerToList();
/* What scope to use for storing contig layout*/
enum storeTypes {THIS_CTG, ALL_CTG, DEFAULT_CTG};
enum storeTypes store_layout;

/* Options for the size of the display screen*/
enum sizeOptions {SIZE_NORMAL, SIZE_NO_STATS, SIZE_TRACKS_ONLY};
enum sizeOptions size_option=SIZE_NORMAL;

int track_num=0;     /*The number of tracks in the current contig display*/
float old_fpcruler_value=0.0;  /*Used for keeping the region markers in the same
                                *relative position when scrolling*/
enum entities add_entity;      /*The type of entity to put in newly created track*/
GtkObject *main_hadj;          /*The adjustment for the horizontal scrollbar*/
GtkObject *zoom_adj;           /*The adjustment for the zoom*/
GtkWidget *yes_radiobutton;    /*Show buried clones*/
GtkWidget *no_radiobutton;     /*Don't show buried clones*/
static GtkWidget *page_entry1; /*Entry for left end of page*/
static GtkWidget *page_entry2; /*Entry for right end of page*/
static GtkWidget *move_left_button; /*Page left*/
static GtkWidget *move_right_button;/*Page right*/
static int saved_left_end=INT_MIN, saved_right_end=INT_MIN;  /*Used to check if contig

                                                              *left, right ends have changed*/
/*fred 2/26/04 -- Use these settings for the session once a change has been made*/
int glbl_show_anchor_bins=-1;
int glbl_show_anchor_pos=-1;
int glbl_show_anchor_lines=-1;

extern  gint gtk_hfpcruler_motion_notify (GtkWidget      *widget,
				      GdkEventMotion *event);

void draw_anchors(GtkTrack* t);

GtkWidget *fpcruler;
extern GtkWidget* detail_window;

GdkPixmap* pos_pixmap = NULL;
GdkGC* pos_gc;

/* XPM */
static char *right_active_xpm[] = {
/* width height num_colors chars_per_pixel */
"    9    17       2            1",
/* colors */
". c None",
"# c #228B22",
/* pixels */
"#........",
"##.......",
"###......",
"####.....",
"#####....",
"######...",
"#######..",
"########.",
"#########",
"########.",
"#######..",
"######...",
"#####....",
"####.....",
"###......",
"##.......",
"#........"
};

static char *left_active_xpm[] = {
/* width height num_colors chars_per_pixel */
"    9    17       2            1",
/* colors */
". c None",
"# c #228B22",
/* pixels */
"........#",
".......##",
"......###",
".....####",
"....#####",
"...######",
"..#######",
".########",
"#########",
".########",
"..#######",
"...######",
"....#####",
".....####",
"......###",
".......##",
"........#"
};

GtkWidget *lpixmapwid=NULL;
GtkWidget *rpixmapwid=NULL;

extern BOOL find_Clone();
extern void orderclones();
extern void setmarkerposctg();
extern void sortmarkers();
extern void compute_track_size();
extern void selectOptions();
extern void store_single_ctg_layout(int ctg);
extern int buryFlag, unburyFlag, removeFlag;
extern int pick_pair_start;
extern GtkWidget *merge_window1, *merge_window2;
extern struct layout_ctg_info *ctg_layout;
extern int configure_event();

extern void print_dialog();

extern void draw_anchor_shade(GtkTrack *t);
extern void do_centre_stuff();
extern void CurrentCtgRemark();
extern void selectOptions();
extern void selectOptions2();
extern void EvalCtgDisplay();
extern void CBCtgDisplay();
extern void EditCtgDisplay();
extern void show_help(char * title);
extern void ctgdisplay_help();
extern void show_legend();
extern void gtk_select_colored();
extern void keysetselect();
extern void fpRegion();
extern void ShowIncr();
extern void clear_highlights();
extern void redo_highlights();
extern void pickClam();
extern void appendCloneToMarkerList();
extern void redrawCBmap();
extern void clearall();
extern void quitMark();
extern void quitClamA();
extern void quitClamB();
extern void quitClamC();
extern void quitCB();
extern void load_layout();
extern void check_ctg_layout_mem();
extern void clear_matches();
extern void displaymarker();
extern void merge_display1();
extern void refresh_all_track_colors();
extern int markerTrail;
extern int mergespace;
extern void gtk_track_highlight_clone_selected();
extern void gtk_track_highlight_clone_active();
extern void gtk_track_highlight_marker_active();
extern void gtk_track_highlight_remark_active();
extern void gtk_track_clear_all();
extern int gtk_track_highlight_matches();
extern void move_to_pair();
extern void bss_display();

static void quit_button_callback( GtkWidget *widget, gpointer data );
static void add_select_callback1(GtkWidget *widget, gpointer data);
static void add_select_callback2(GtkWidget *widget, gpointer data);
static void add_select_callback3(GtkWidget *widget, gpointer data);
static void add_select_callback4(GtkWidget *widget, gpointer data);
static void add_select_callback5(GtkWidget *widget, gpointer data);
static void store_this_ctg_callback(GtkWidget *widget, gpointer data);
static void store_all_ctg_callback(GtkWidget *widget, gpointer data);
static void store_default_ctg_callback(GtkWidget *widget, gpointer data);
static void merge_button_callback(GtkWidget *widget, gpointer data);
static void trail_toggle_callback();
static void mtrail_toggle_callback();
static void clear_all_callback(GtkWidget *widget, gpointer data);
static void size_normal_callback();
static void size_no_stats_callback();
static void size_tracks_only_callback();
static void zoom_in_callback();
static void zoom_out_callback();
extern void gtk_track_highlight_sequence_active(GtkTrack *track, int index);

extern gint
gtk_hfpcruler_expose(GtkWidget *widget, GdkEventExpose *event, gpointer _foo);

/* The items for the menu bar at the top of the ctgdisplay*/
static GtkItemFactoryEntry menu_items[] = {
  { "/File", NULL, NULL, 0, "<Branch>" },
  { "/File/Print...", "<control>P", print_dialog, 0, NULL },
  { "/File/Close", "<control>Q", quit_button_callback, 0, NULL },

  { "/Edit", NULL, NULL, 0, "<Branch>" },
  { "/Edit/Contig remarks...", NULL, CurrentCtgRemark, 0, NULL },
  { "/Edit/Select clones...", NULL, selectOptions, 0, NULL },
  { "/Edit/Edit clone coordinates...", NULL, selectOptions2, 0, NULL },
  { "/Edit/Merge contigs...", NULL, merge_button_callback, 0, NULL },

  { "/Analysis", NULL, NULL, 0, "<Branch>" },
  { "/Analysis/Evaluate...", NULL, EvalCtgDisplay, 0, NULL },
  { "/Analysis/Compute CB Maps...", NULL, CBCtgDisplay, 0, NULL },
  { "/Analysis/Semi-auto edits...", NULL, EditCtgDisplay, 0, NULL },
  { "/Analysis/BSS...", NULL, bss_display, 0, NULL },

  { "/Highlight", NULL, NULL, 0, "<Branch>" },
  { "/Highlight/Clear all", "<control>L", clear_all_callback, 0, NULL },
  { "/Highlight/Trail", "<control>T", trail_toggle_callback, 0, "<CheckItem>"},
  { "/Highlight/Marker Trail", NULL, mtrail_toggle_callback, 0, "<CheckItem>"},
  { "/Highlight/Select Colored", NULL, gtk_select_colored, 0, NULL},
  { "/Highlight/Select Region", NULL, fpRegion, 0, NULL},
  { "/Highlight/Select Keyset", NULL, keysetselect, 0, NULL},
  { "/Highlight/Show Additions", NULL, ShowIncr, 0, NULL},

  { "/Add track", NULL, NULL, 0, "<Branch>" },
  { "/Add track/New marker track", "<control>M", add_select_callback1, 0, NULL },
  { "/Add track/New clone track", "<control>C", add_select_callback2, 0, NULL },
  { "/Add track/New remark track", "<control>R", add_select_callback3, 0, NULL },
  { "/Add track/New anchor track", "<control>A", add_select_callback4, 0, NULL },
#ifdef WITH_SEQTRACK
  { "/Add track/New sequence track", "<control>S", add_select_callback5, 0, NULL },
#endif
  { "/Layout", NULL, NULL, 0, "<Branch>" },
  { "/Layout/Store layout for this contig", NULL, store_this_ctg_callback, 0, NULL },
  { "/Layout/Store layout for all contigs", NULL, store_all_ctg_callback, 0, NULL },
  { "/Layout/Store layout as default", NULL, store_default_ctg_callback, 0, NULL },

  { "/Size options", NULL, NULL, 0, "<Branch>" },
  { "/Size options/Normal", NULL, size_normal_callback, 0, "<RadioItem>" },
  { "/Size options/No contig stats", NULL, size_no_stats_callback, 0, "/Size options/Normal" },
  { "/Size options/Tracks only", NULL, size_tracks_only_callback, 0, "/Size options/No contig stats" },
  { "/Size options/sep1", NULL, NULL, 0, "<Separator>" },
  { "/Size options/Zoom in", "<control>I", zoom_in_callback, 0, NULL },
  { "/Size options/Zoom out", "<control>O", zoom_out_callback, 0, NULL },

  { "/Help", NULL, NULL, 0, "<LastBranch>" },
  { "/Help/Show Help", NULL, ctgdisplay_help, 0, NULL },
  { "/Help/Legend", NULL, show_legend, 0, NULL },
};

static GtkItemFactory *item_factory;

/*                    DEF: get_main_menu
 *Create menubar with the menu_items*/
static void
get_main_menu( GtkWidget  *window,
               GtkWidget **menubar )
{
  GtkAccelGroup *accel_group;
  gint nmenu_items = sizeof (menu_items) / sizeof (menu_items[0]);

  accel_group = gtk_accel_group_new ();

  item_factory = gtk_item_factory_new (GTK_TYPE_MENU_BAR, "<main>",
                                       accel_group);

  gtk_item_factory_create_items (item_factory, nmenu_items, menu_items, NULL);

  gtk_window_add_accel_group (GTK_WINDOW (window), accel_group);

  if (menubar)
    /* Finally, return the actual menu bar created by the item factory. */
    *menubar = gtk_item_factory_get_widget (item_factory, "<main>");
}

/*                    DEF: track_deleted
 * Called whenever a track gets deleted; Updates the global tracks
 * structure and decrements track_num*/
static int
track_deleted( GtkWidget *widget,
               gpointer data)
{
  int i,j;
  int found=FALSE;

  for(i=0;(i<track_num) && !found;i++){
    if(tracks[i]==widget){
      for(j=i;j<track_num-1;j++){
        tracks[j] = tracks[j+1];
        GTK_TRACK(tracks[j])->track_pos--;
        GTK_TRACK(tracks[j])->track_pos_new--;
      }
      track_num--;
      found=TRUE;
    }
  }
  return FALSE;
}

void print_sequence_hits(struct t_seq* ts, int ctg)
{
    SEQHIT* phit;
    struct fpc_seq_list* pctgs;
    FPC_SEQ* pctg, *pseq;
    CLONE* clp;  
    int nhits = 0;

    assert(ts);
    assert(ts->ctgpos);
    assert(ts->seq);

    printf("%s ctg%d: %d hits, correlation %f\n",ts->seq->name,ctg,ts->ctgpos->score,ts->ctgpos->corr);fflush(0);
    return;

    pseq = ts->seq;
    
    for (phit = pseq->hits; phit; phit = phit->next) {
      clp = arrp(acedata,phit->clone_idx,CLONE);
      if (clp->ctg == ctg) {
            printf("%s %s%c %d\n",pseq->name,clp->clone,phit->rf,phit->seq_start);fflush(0);
            nhits++;
      }
    }
    for (pctgs = pseq->ctgs; pctgs; pctgs = pctgs->next) {
      pctg = pctgs->seq;
      for (phit = pctg->hits; phit; phit = phit->next) {
         clp = arrp(acedata,phit->clone_idx,CLONE);
         if (clp->ctg == ctg) {
             printf("%s %s%c %d %d\n",pctg->name,clp->clone,phit->rf,clp->x,clp->y);fflush(0);
            nhits++;
         }
      }
    }

}


/*                    DEF: handle_button_press
 * Called whenever a button press is detected on a track, and
 * is not handled by the track.  This includes highlighting,
 * selecting, or matching entities, scrolling through the contig
 * via middle button click, and centering on anchor marker.*/
int handle_button_press ( GtkWidget *widget,
                          GdkEventButton *event, GtkTrack *track)
{
  int i, j, index;
  int clicked_track_index;
  GtkTrack *clicked_track;
  GtkTrack *marker_track=NULL;
  GtkAdjustment *adj;
  int midpt=0, value_pos;
  struct contig *cptr;
  FPC_SEQ* clicked_seq;
  struct t_seq* ts;


  if(handled_bpress) return FALSE;

  clicked_track_index = track->track_pos - 1;
  clicked_track=GTK_TRACK(tracks[clicked_track_index]);

  if(event->type!=GDK_BUTTON_PRESS) return FALSE;

  if(event->button==2){
    /* Center on postion clicked*/
    if(clicked_track->entity==ANCHORS) return FALSE;
    adj = gtk_scrolled_window_get_hadjustment(GTK_SCROLLED_WINDOW(clicked_track->scrolled_window));
    midpt=(float)event->x/(float)(display_zoom * horiz_scale_factor);
    value_pos = MIN(adj->upper - adj->page_size,
                    midpt * display_zoom * horiz_scale_factor - adj->page_size/2);
    gtk_adjustment_set_value(adj, value_pos);
    return FALSE;
  }
  else if(event->button==1){
    if(!trail && !markerTrail) clear_highlights();

    if(clicked_track->highdata==-1) return FALSE;

    if(clicked_track->entity==CLONES){
      index = clicked_track->data.cloneList[clicked_track->highdata].index;
      if(fpflag)
	gtk_track_highlight_clone_selected(clicked_track, clicked_track->highdata);
      else if(fpadd){
        addfp(clicked_track->data.cloneList[clicked_track->highdata].index,0);
	if(bands)
	  drawfpdata(1);
	graphPop();
      }
      else if (geladd) { /* cari 10.05.04 gel add did not work */
        addgel(clicked_track->data.cloneList[clicked_track->highdata].index,0);
        gelredraw();
        graphPop();
      }
      else if(pick_pair_start){
        move_to_pair(clicked_track->data.cloneList[clicked_track->highdata].index);
      }
      else if(buryFlag || unburyFlag || removeFlag){
	for(cptr=root;cptr!=NULL;cptr=cptr->new){
	  if(cptr->next==index){ pickClam(cptr); break;}
	}
      }
      else if(editmarkerstatus){
	appendCloneToMarkerList(index);
      }
      else{
	highmark=-1;
	highremark=-1;
    highseq = 0;
	for(cptr=root;cptr!=NULL;cptr=cptr->new){   /*"new" is next; "next" is clone index*/
	  if(cptr->next ==
             clicked_track->data.cloneList[clicked_track->highdata].index) clhigh = cptr;
	}
	gtk_track_highlight_clone_active(clicked_track,clicked_track->highdata);
      }
      if (graphActivate(g99)) redrawCBmap(); /* cbmap */
      /* cari 21.5.4 */
      if (graphActivate(g3) && clhigh!=NULL)    {/* cari 21.6.4 */
         for(fphigh = -1, i=0;i<FPMAX && cloneindex[i]!=-1;i++){
           if(cloneindex[i] == clhigh->next){
               fphigh =i; /* index into cloneindex array - hold fp index */
               drawfpdata(1); /* centre on fphigh */
               break;
           }
         }
      }
      if (graphActivate(gtest) && clhigh!=NULL) {/* cari 21.6.4 */
         for(gelhigh = -1, i=0;i<FPMAX && gelclone[i]!=-1;i++){
            if (gelclone[i] == clhigh->next){
               gelhigh = clhigh->next;
               gelredraw();
            }
         }
      }
    }
    else if(clicked_track->entity==MARKERS){
      if(fpflag) return FALSE;
      clhigh=NULL;
      highremark=-1;
      highseq = 0;
      highmark=clicked_track->data.markerList[clicked_track->highdata].index;
      if (editclonestatus) appendMarkerToList(highmark);
      else
          gtk_track_highlight_marker_active(clicked_track, clicked_track->highdata);
    }
    else if(clicked_track->entity==SEQUENCE){
      if(fpflag) return FALSE;
      clhigh=NULL;
      highremark=-1;
      highmark = -1;
      ts = &clicked_track->data.seqList[clicked_track->highdata];
      clicked_seq = ts->seq;
      //print_sequence_hits(ts,currentctg);
      if (highseq != ts->seq) {
         ts->cycle = 0;
      }
      else if (centreSequence == clicked_seq) {
         /* a hack; means we have arrived from user clicking on a ctg in the seq info display */
         centreSequence = 0;
      }

      highseq=clicked_seq;
      clicked_track->data.seqList[clicked_track->highdata].nclicks++;
      gtk_track_highlight_sequence_active(clicked_track,clicked_track->highdata);

    }
    else if(clicked_track->entity==REMARKS){
      if(fpflag) return FALSE;
      clhigh=NULL;
      highmark=-1;
      highseq = 0;
      highremark=clicked_track->highdata;
      gtk_track_highlight_remark_active(clicked_track, clicked_track->highdata);
    }
    else if(clicked_track->entity==ANCHORS){
      if(fpflag) return FALSE;
      index = clicked_track->data.anchorList[clicked_track->highdata].index;
      clhigh=NULL;
      highremark=-1;
      highseq = 0;
      highmark=index;
      /* Go through all marker tracks and compute the midpt, highlight marker*/
      for(i=0;i<track_num;i++){
	switch (GTK_TRACK(tracks[i])->entity){
	  case CLONES:
	    break;
	  case MARKERS:
	    for(j=0;j<GTK_TRACK(tracks[i])->numdata;j++){
	      if(GTK_TRACK(tracks[i])->data.markerList[j].index == index){
                GTK_TRACK(tracks[i])->highdata=j;
		gtk_track_highlight_marker_active(GTK_TRACK(tracks[i]),j);
		midpt = GTK_TRACK(tracks[i])->data.markerList[j].pos - page_end_left;
		marker_track=GTK_TRACK(tracks[i]);
	      }
	    }
	    break;
	  case REMARKS:
	    break;
	  case ANCHORS:
	    break;
	  case SEQUENCE:
	    break;
	  case NONE:
	    break;
	}
      }
      if((marker_track!=NULL) && (midpt>0)){
	/* Center on marker clicked*/
	adj = gtk_scrolled_window_get_hadjustment(GTK_SCROLLED_WINDOW(marker_track->scrolled_window));
	value_pos = MIN(adj->upper - adj->page_size,
			midpt * display_zoom * horiz_scale_factor - adj->page_size/2);
	gtk_adjustment_set_value(adj, value_pos);
      }
    }
  }
  return FALSE;
}

/*                    DEF: reorder_tracks
 * Called when the track position has changed for a track*/
void
reorder_tracks(){
  int i;

  /* Remove all tracks from container*/
  for(i=0;i<track_num;i++){
    gtk_widget_ref(tracks[i]);
    gtk_container_remove(GTK_CONTAINER(tracks[i]->parent), tracks[i]);
    GTK_TRACK(tracks[i])->cursor_set=FALSE;
  }

  /* Repack them with new order; prevent pixmap draws while reordering, as
   * redrawing triggers a highlight refresh, which tries to redraw highlights
   * on tracks that are not yet redrawn, which causes warnings. */
  for(i=0;i<track_num;i++){
    gtk_signal_handler_block_by_func(GTK_OBJECT (GTK_TRACK(tracks[i])->drawing_area),
				     GTK_SIGNAL_FUNC(configure_event),
				     GTK_TRACK(tracks[i]));
    gtk_box_pack_start(GTK_BOX(tracks_vbox), tracks[i], FALSE, FALSE, 0);
  }
  /* Now re-enable configure_events, and queue the tracks to be shown*/
  for(i=0;i<track_num;i++){
    gtk_signal_handler_unblock_by_func(GTK_OBJECT (GTK_TRACK(tracks[i])->drawing_area),
				       GTK_SIGNAL_FUNC(configure_event),
				       GTK_TRACK(tracks[i]));
    gtk_widget_show(tracks[i]);
  }
}

/*                    DEF: add_track
 * Create a new track, put it in tracks, and increment track_num*/
GtkWidget *
add_track( GtkWidget *widget,
                GtkObject *adj)
{
  tracks[track_num] = gtk_track_new();
  gtk_track_set_hadjustment(GTK_TRACK(tracks[track_num]), GTK_ADJUSTMENT(adj));

  gtk_signal_connect(GTK_OBJECT (tracks[track_num]), "destroy",
		     GTK_SIGNAL_FUNC (track_deleted), NULL);
  gtk_signal_connect_after(GTK_OBJECT (tracks[track_num]), "button_press_event",
		           GTK_SIGNAL_FUNC (handle_button_press),
                           GTK_TRACK(tracks[track_num]));
  gtk_box_pack_start(GTK_BOX(tracks_vbox), tracks[track_num], FALSE, FALSE, 0);
  track_num++;
  return tracks[track_num-1];
}

/*                    DEF: create_new_track
 * Calls function to create a new track, set's entity according to the
 * value of add_entity, and sets up default filters.*/
void
create_new_track( GtkWidget *widget, GtkObject *adj){
  GtkWidget *track;

  track = add_track(widget,adj);
  checked_width=FALSE;
  switch(add_entity){
    case MARKERS:
      gtk_track_set_entity(GTK_TRACK(track),MARKERS, 200, AUTO_POLICY, 0, 0, 0, 0);
      gtk_track_add_filter(GTK_TRACK(track),
                           "*","",ANY_MARK,ANY_STATUS,ANY_CHROM,ANY_ATTACH,BLACK_COLOR,NULL);
      gtk_track_fill_data(GTK_TRACK(track));
      compute_track_size(GTK_TRACK(track));
      break;
    case CLONES:
      gtk_track_set_entity(GTK_TRACK(track),CLONES, 300, FIT_POLICY, 0, 0, 0, 0);
      gtk_track_add_filter(GTK_TRACK(track),
                           "*","",ANY_CLONE,ANY_STATUS,ANY_CHROM,ANY_ATTACH,BLACK_COLOR,NULL);
      gtk_track_fill_data(GTK_TRACK(track));
      compute_track_size(GTK_TRACK(track));
      break;
    case REMARKS:
      gtk_track_set_entity(GTK_TRACK(track),REMARKS, 200, AUTO_POLICY, 0, 0, 0, 0);
      gtk_track_add_filter(GTK_TRACK(track),
                           "*","",ANY_REMARK,ANY_STATUS,ANY_CHROM,ANY_ATTACH,BLACK_COLOR,NULL);
      gtk_track_fill_data(GTK_TRACK(track));
      compute_track_size(GTK_TRACK(track));
      break;
    case ANCHORS:
      gtk_track_set_entity(GTK_TRACK(track),ANCHORS, 200, AUTO_POLICY, 0,
                           glbl_show_anchor_bins==-1 ? 0 : glbl_show_anchor_bins,
                           glbl_show_anchor_pos==-1 ? 0 : glbl_show_anchor_pos,
                           glbl_show_anchor_lines==-1 ? 1 : glbl_show_anchor_lines);
      gtk_track_add_filter(GTK_TRACK(track),
                           "*","",ANY_ANCHOR,ANY_STATUS,ANY_CHROM,ANY_ATTACH,BLACK_COLOR,NULL);
      gtk_track_add_filter(GTK_TRACK(track),
                           "*","",PLACE_ANCHOR,ANY_STATUS,ANY_CHROM,ANY_ATTACH,GRAY_COLOR,NULL);
      gtk_track_add_filter(GTK_TRACK(track),
                           "*","",ANY_ANCHOR,ANY_STATUS,BAD_CHROM,ANY_ATTACH,RED_COLOR,NULL);
      gtk_track_fill_data(GTK_TRACK(track));
      compute_track_size(GTK_TRACK(track));
      break;
    case SEQUENCE:
      gtk_track_set_entity(GTK_TRACK(track),SEQUENCE, 200, AUTO_POLICY, 0, 0, 0, 0);
      gtk_track_add_filter(GTK_TRACK(track),
                           "*","",ANY_SEQ,ANY_STATUS,ANY_CHROM,ANY_ATTACH,BLACK_COLOR,NULL);
      gtk_track_fill_data(GTK_TRACK(track));
      compute_track_size(GTK_TRACK(track));
      break;
    case NONE:
      break;
  }
  gtk_widget_show(track);
}

/*                    DEF: close_ctg_window
 * Called when gtk_widget_destroy() is called on the ctg_window.
 * Clean up routine.*/
void close_ctg_window( GtkWidget *widget,
                       gpointer   data )
{
  if(ctg_window!=NULL){
    ctg_window=NULL;
    clhigh=NULL;
    highmark=-1;
    highremark=-1;
    highseq = 0;
    currentctg = -1;

    if (root!=NULL) messfree(root);
    root=NULL;

    clearall();
    quitMark(); /* clears markerlistroot */
    quitClamA();
    quitClamB();
    quitClamC();
    quitCB();
    if(graphActivate(gselected)) {
       fpflag=FALSE;
       graphDestroy();
    }
    if(graphActivate(gselected2)) {
       graphDestroy();
    }
  }
   if (pos_pixmap) gdk_pixmap_unref(pos_pixmap); 

    if (detail_window != NULL)
    {
        gtk_widget_destroy(detail_window);
        detail_window = NULL;
    }

}

/*                    DEF: quit_button_callback
 * Callback for Close option on File menu*/
static void
quit_button_callback( GtkWidget *widget,
                           gpointer   data ){
  if(ctg_window!=NULL) gtk_widget_destroy(ctg_window);
  if (merge_window2!=NULL) gtk_widget_destroy(merge_window2); /* cari 10.5.4 */
  if (merge_window1!=NULL) gtk_widget_destroy(merge_window1); /* cari 10.5.4 */
}

/*                    DEF: remove_all_tracks
 * Removes all tracks in ctg_window.  Used when opening a
 * contig when another is already being displayed*/
void remove_all_tracks(){
  int i;
  int num;
  GtkWidget *widget;

  num = track_num;
  for(i=0;i<num;i++){
    widget=tracks[track_num-1];
    gtk_widget_destroy(widget);
  }
}

/*                    DEF: load_tracks
 * Create all tracks according to specs in the ctg_layout*/
void
load_tracks(GtkObject *adj){
  int index=0;
  struct layout_track_info *t_info;
  struct layout_filter_info *f_info;
  GtkWidget *track;

  if(ctg_layout==NULL) load_layout();
  else check_ctg_layout_mem();

  if(ctg_layout[currentctg].defined) index = currentctg;

  checked_width=FALSE;
  for(t_info=ctg_layout[index].tracks; t_info!=NULL; t_info=t_info->next){
    track = add_track(NULL,adj);
    gtk_track_set_entity(GTK_TRACK(track), t_info->entity,
                         t_info->height, t_info->row_policy, t_info->numrows,
                         glbl_show_anchor_bins==-1 ? t_info->show_anchor_bins : glbl_show_anchor_bins,
                         glbl_show_anchor_pos==-1 ? t_info->show_anchor_pos : glbl_show_anchor_pos,
                         glbl_show_anchor_lines==-1 ? t_info->show_anchor_lines : glbl_show_anchor_lines);
    for(f_info=t_info->filters; f_info!=NULL; f_info=f_info->next){
      gtk_track_add_filter(GTK_TRACK(track), f_info->name, f_info->remark, f_info->type,
                           f_info->status, f_info->chrom, f_info->attachment,
                           f_info->color_choice, &(f_info->color));
    }
    gtk_track_fill_data(GTK_TRACK(track));
    compute_track_size(GTK_TRACK(track));
  }
  /* Call do_centre_stuff once all tracks have been realized*/
  gtk_idle_add((GtkFunction) do_centre_stuff, NULL);
}

/*                    DEF: update_zoom
 * Update the display_zoom and redraw tracks*/
void
update_zoom( GtkObject *adj, gpointer data){
  int i;
  float adj_value;
  CLONE *clp;

  if(clhigh!=NULL){
    clp=arrp(acedata, clhigh->next, CLONE);
    centre_pos = (clp->y - clp->x)/2 + clp->x;
  }


  adj_value = GTK_ADJUSTMENT(adj)->value;
  display_zoom = adj_value;

  checked_width=FALSE;


  for(i=0;i<track_num;i++){
    /* The drawing_area_size call in compute_track_size triggers a configure_event,
     * which will cause everything to be redrawn.*/
    compute_track_size(GTK_TRACK(tracks[i]));
  }

  /* Call do_centre_stuff once all tracks have been realized*/
  gtk_idle_add((GtkFunction) do_centre_stuff, NULL);
}

/*                    DEF: whole_callback
 * Show everything in the currently viewable area.  The display_zoom equation is
 * computed from:
 * right*display_zoom*ZOOM_ADJUSTMENT - left*display_zoom*ZOOM_ADJUSTMENT + 1 = width*/
void
whole_callback( GtkWidget *widget, GtkObject *adj){
  int left, right, width;

  if(track_num==0) return;
  left = page_end_left;
  right = page_end_right;
  width = GTK_WIDGET(tracks[0])->allocation.width - SCROLLBAR_WIDTH - 3;

  display_zoom = (float)(width-1)/(float)(horiz_scale_factor*(right-left));

  checked_width=FALSE;
  gtk_adjustment_set_value(GTK_ADJUSTMENT(adj), display_zoom);
}

/*                    DEF: update_fpcruler
 * Update the ruler scale, region indicators, and anchor shading.
 * Called when horizontal scrolling takes place, and other times
 * when ruler needs to be refreshed*/
void update_fpcruler( GtkWidget *widget,
                      GtkWidget *fpcruler){
  int i;
  GtkTrack *t;
  GtkAdjustment *adj;
  int left_offset;
  int left_start;

  left_offset = page_end_left * display_zoom * horiz_scale_factor;
  left_start = GTK_ADJUSTMENT(widget)->value + left_offset;

  if(!GTK_IS_FPC_RULER(fpcruler)) return;   /* Happens for some reason when Clean Up
                                             * is clicked on Main Menu */

    gtk_fpcruler_set_range(GTK_FPC_RULER(fpcruler),left_start,
			left_start + GTK_ADJUSTMENT(widget)->page_size,
                        GTK_FPC_RULER(fpcruler)->position1 +
                            (left_start - old_fpcruler_value),
                        GTK_FPC_RULER(fpcruler)->position2 +
                            (left_start - old_fpcruler_value),
			GTK_ADJUSTMENT(widget)->upper,
			1);
  
  old_fpcruler_value=left_start;

  /* Set ticks w.r.t. zoom*/
  gtk_fpcruler_set_metric(GTK_FPC_RULER(fpcruler), display_zoom);

  /* Set FPC region variables*/
  region[0].band= MAX(0, (float)(GTK_FPC_RULER(fpcruler)->position1) /
		         (float)(display_zoom * horiz_scale_factor));
  region[0].scr=MAX(0, GTK_FPC_RULER(fpcruler)->position1);
  region[0].xsrc=GTK_FPC_RULER(fpcruler)->xsrc1;
  region[1].band= MAX(0, ((float)GTK_FPC_RULER(fpcruler)->position2) /
			 (float)(display_zoom * horiz_scale_factor));
  region[1].scr=MAX(0, GTK_FPC_RULER(fpcruler)->position2);
  region[1].xsrc=GTK_FPC_RULER(fpcruler)->xsrc2;

  /* Refresh anchor shading*/
  for(i=0; i<track_num; i++){
    t = GTK_TRACK(tracks[i]);
    if(t->entity==ANCHORS){
      if(GTK_WIDGET_REALIZED(GTK_WIDGET(t))){

       draw_anchor_shade(t);
     draw_anchors(t);
        adj = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(t->scrolled_window));
        gdk_draw_pixmap(t->drawing_area->window,
		        t->drawing_area->style->fg_gc[GTK_WIDGET_STATE (t->drawing_area)],
		        t->pixmap,
		        0, adj->value,
		        0, adj->value,
                        t->drawing_area->allocation.width,
                        t->scrolled_window->allocation.height);

      }
    }
  }
}

/*                    DEF: update_region
 * Called when region markers get changed*/
gint
update_region( GtkWidget *widget, GdkEventMotion *event,
               gpointer data){
  GtkFpcRuler *ruler;
  ruler = GTK_FPC_RULER(widget);
  if(event->state & GDK_BUTTON1_MASK){
    if(ruler->selected_pos==1){
      region[0].band= MAX(0, (float)ruler->position1 /
                             (float)(display_zoom * horiz_scale_factor));
      region[0].scr=MAX(0, ruler->position1);
      region[0].xsrc=ruler->xsrc1;
    }
    else if(ruler->selected_pos==2){
      region[1].band= MAX(0, (float)ruler->position2 /
                             (float)(display_zoom * horiz_scale_factor));
      region[1].scr=MAX(0, ruler->position2);
      region[1].xsrc=ruler->xsrc2;
    }
  }
return FALSE;
}

/*                    DEF: yes_radiobutton_callback
 * Show buried clones callback*/
static void
yes_radiobutton_callback(GtkWidget *widget, gpointer data){
  int i;


  clear_highlights();
  if (GTK_TOGGLE_BUTTON (widget)->active){
    showburied=0;
    for(i=0;i<track_num;i++){
      if((GTK_TRACK(tracks[i])->entity == CLONES) ||
	 (GTK_TRACK(tracks[i])->entity == REMARKS))
	compute_track_size(GTK_TRACK(tracks[i]));
    }
  }
  gtk_idle_add((GtkFunction)redo_highlights, NULL);
}

/*                    DEF: no_radiobutton_callback
 * Don't show buried clones callback*/
static void
no_radiobutton_callback(GtkWidget *widget, gpointer data){
  int i;

  clear_highlights();
  if (GTK_TOGGLE_BUTTON (widget)->active){
    showburied=1;
    for(i=0;i<track_num;i++){
      if((GTK_TRACK(tracks[i])->entity == CLONES) ||
	 (GTK_TRACK(tracks[i])->entity == REMARKS))
	compute_track_size(GTK_TRACK(tracks[i]));
    }
  }
  gtk_idle_add((GtkFunction)redo_highlights, NULL);
}

/*                    DEF: add_select_callback1
 * Add marker track from Add track menu*/
static void
add_select_callback1(GtkWidget *widget, gpointer data){
  add_entity=MARKERS;
  create_new_track(widget, main_hadj);
}

/*                    DEF: add_select_callback2
 * Add clone track from Add track menu*/
static void
add_select_callback2(GtkWidget *widget, gpointer data){
  add_entity=CLONES;
  create_new_track(widget, main_hadj);
}

/*                    DEF: add_select_callback3
 * Add remark track from Add track menu*/
static void
add_select_callback3(GtkWidget *widget, gpointer data){
  add_entity=REMARKS;
  create_new_track(widget, main_hadj);
}

/*                    DEF: add_select_callback5
 * Add sequence track from Add track menu*/
static void
add_select_callback5(GtkWidget *widget, gpointer data){
  add_entity=SEQUENCE;
  create_new_track(widget, main_hadj);
}

/*                    DEF: add_select_callback4
 * Add anchor track from Add track menu*/
static void
add_select_callback4(GtkWidget *widget, gpointer data){
  add_entity=ANCHORS;
  create_new_track(widget, main_hadj);
}

/*                    DEF: search_tracks
 * Callback for keyword search; highlights matches, centers at
 * midpoint of all matches*/
int
search_tracks(GtkWidget *widget, gpointer data){
  char search_str[CLONE_SZ+1];
  int i;
  int found=0;
  int midpt=0, totmidpt=0;
  GtkAdjustment *adj;
  int value_pos;
  int index;

  clear_matches();
  strcpy(search_str,gtk_entry_get_text(GTK_ENTRY(widget)));
  if(!strcmp(search_str, "")) return FALSE;

  for(i=0;i<track_num;i++){
    found += gtk_track_highlight_matches(GTK_TRACK(tracks[i]), search_str, &totmidpt);
  }
  if(found) midpt = totmidpt/found - page_end_left;
  if((midpt>0)){
    /* Center on search matches*/
    adj = GTK_ADJUSTMENT(main_hadj);
    value_pos = MIN(adj->upper - adj->page_size,
                    midpt * display_zoom * horiz_scale_factor - adj->page_size/2);
    gtk_adjustment_set_value(adj, value_pos);
  }
  if(!found){
    /* Search for exact match in all contigs... if found pop up text window*/
    if(strstr(search_str, "*")==NULL){
      if(fppFind(acedata, search_str, &index, cloneOrder)){
        displayclone(index);
        return FALSE;
      }
      else if(fppFind(markerdata, search_str, &index, markerOrder)){
        displaymarker(index);
        return FALSE;
      }
    }
    printf("No match to %s\n", search_str);
  }
  else if(found==1) printf("1 match found to %s\n", search_str);
  else printf("%d matches found to %s\n", found, search_str);

  return FALSE;
}

/*                    DEF: update_page_numbers
 * Callback for refreshing the values in the page start
 * and page end text entries*/
void
update_page_numbers(){
  char text[8];

  sprintf(text, "%d", page_end_left);
  gtk_entry_set_text(GTK_ENTRY(page_entry1),text);
  sprintf(text, "%d", page_end_right);
  gtk_entry_set_text(GTK_ENTRY(page_entry2),text);

  if(page_end_left==contigs[currentctg].left){
    gtk_widget_hide(move_left_button);
  }
  else if(!GTK_WIDGET_VISIBLE(move_left_button)){
    gtk_widget_show(move_left_button);
  }

  if(merging && page_end_right == contigs[mergecontig1].right+contigs[mergecontig2].right+mergespace){
    gtk_widget_hide(move_right_button);
  }
  else if(page_end_right==contigs[currentctg].right){
    gtk_widget_hide(move_right_button);
  }
  else if(!GTK_WIDGET_VISIBLE(move_right_button)){
    gtk_widget_show(move_right_button);
  }
}

/*                    DEF: update_page_view
 * Callback for updating the page start and page end values*/
void
update_page_view(GtkWidget *widget, gpointer data){
  int left, right;
  int left_save = page_end_left;
  int right_save = page_end_right;

  if(sscanf(gtk_entry_get_text(GTK_ENTRY(page_entry1)), "%d", &left)!=1) goto bad_input;
  if(sscanf(gtk_entry_get_text(GTK_ENTRY(page_entry2)), "%d", &right)!=1) goto bad_input;
  if(left >= right)
    g_print("Left endpoint must be less than right endpoint\n");
  else{
    if(left < contigs[currentctg].left){
      g_print("Left endpoint must be greater than left end of contig\n");
      page_end_left=contigs[currentctg].left;
      page_end_right = right;
      update_page_numbers();
    }
    else if((right > contigs[currentctg].right) && ! merging){
      g_print("Right endpoint must be less than right end of contig\n");
      page_end_right=contigs[currentctg].right;
      page_end_left = left;
      update_page_numbers();
    }
    else{
      page_end_left = left;
      page_end_right = right;
    }
    page_size = right-left+1;
    if (page_size*zoom_max*horiz_scale_factor >= MAXIMUM_DISPLAY_WIDTH) {
        messout("Selected range is too large to display.\n");
        page_end_left = left_save;
        page_end_right = right_save;
        update_page_numbers();
        return;  
    }
    refresh_gtk_ctgdisplay();

    if(page_end_left==contigs[currentctg].left){
      gtk_widget_hide(move_left_button);
    }
    else if(!GTK_WIDGET_VISIBLE(move_left_button)){
      gtk_widget_show(move_left_button);
    }

    if(page_end_right==contigs[currentctg].right){
      gtk_widget_hide(move_right_button);
    }
    else if(!GTK_WIDGET_VISIBLE(move_right_button)){
      gtk_widget_show(move_right_button);
    }
    return;
  }

bad_input:
  update_page_numbers();
}

/*                    DEF: move_left_callback
 * Callback for when the page left arrow is clicked*/
void
move_left_callback( GtkWidget *widget, gpointer data){
  int left, right;

  if(sscanf(gtk_entry_get_text(GTK_ENTRY(page_entry1)), "%d", &left)!=1) goto bad_input;
  if(sscanf(gtk_entry_get_text(GTK_ENTRY(page_entry2)), "%d", &right)!=1) goto bad_input;
  if((left!=page_end_left) || (right!=page_end_right)){
    update_page_view(widget, data);
    return;
  }

  page_end_right = page_end_left-1;
  page_end_left = MAX(contigs[currentctg].left, page_end_left-page_size);

  if(page_end_left==contigs[currentctg].left){
    gtk_widget_hide(move_left_button);
  }
  if(!GTK_WIDGET_VISIBLE(move_right_button)){
    gtk_widget_show(move_right_button);
  }

  centre_pos=page_end_right;
  refresh_gtk_ctgdisplay();
  return;

bad_input:
  update_page_numbers();
}

/*                    DEF: move_right_callback
 * Callback for when the page right arrow is clicked*/
void
move_right_callback( GtkWidget *widget, gpointer data){
  int left, right;

  if(sscanf(gtk_entry_get_text(GTK_ENTRY(page_entry1)), "%d", &left)!=1) goto bad_input;
  if(sscanf(gtk_entry_get_text(GTK_ENTRY(page_entry2)), "%d", &right)!=1) goto bad_input;
  if((left!=page_end_left) || (right!=page_end_right)){
    update_page_view(widget, data);
    return;
  }

  page_end_left = page_end_right + 1;
  page_end_right = MIN(contigs[currentctg].right, page_end_right+page_size);

  if(page_end_right==contigs[currentctg].right){
    gtk_widget_hide(move_right_button);
  }
  if(!GTK_WIDGET_VISIBLE(move_left_button)){
    gtk_widget_show(move_left_button);
  }

  centre_pos=page_end_left;
  refresh_gtk_ctgdisplay();
  return;

bad_input:
  update_page_numbers();
}

/*                    DEF: trail_toggle_callback
 *Highlight menu -- Trail*/
static void
trail_toggle_callback(){
  GtkWidget *item= gtk_item_factory_get_item (item_factory, "/Highlight/Trail");;

  if (GTK_CHECK_MENU_ITEM (item)->active) trail=TRUE;
  else trail=FALSE;
}

/*                    DEF: mtrail_toggle_callback
 *Highlight menu -- Marker Trail*/
static void
mtrail_toggle_callback(){
  GtkWidget *item= gtk_item_factory_get_item (item_factory, "/Highlight/Marker Trail");;

  if (GTK_CHECK_MENU_ITEM (item)->active) markerTrail=TRUE;
  else markerTrail=FALSE;
}

/*                    DEF: clear_all_callback
 *Highlight menu -- Clear all*/
static void
clear_all_callback(GtkWidget *widget, gpointer data){
  int i;

  for(i=0;i<track_num;i++){
    gtk_track_clear_all(GTK_TRACK(tracks[i]), CLEAR2);
  }
  clhigh=NULL;
  highmark=-1;
  highremark=-1;
  highseq = 0;
}


/*                    DEF: merge_button_callback
 *Edit menu -- Merge contigs*/
static void
merge_button_callback(GtkWidget *widget, gpointer data){
  if(merge_window1 != NULL)
    gdk_window_raise(merge_window1->window);
  else if(merge_window2 != NULL)
    gdk_window_raise(merge_window2->window);
  else{
    if(page_end_right != contigs[currentctg].right)
      messout("Please move to end of contig before merging");
    else
      merge_display1();
  }
}

/*                    DEF: store_this_ctg_callback
 *Layout menu -- Store layout for this contig*/
static void
store_this_ctg_callback(GtkWidget *widget, gpointer data){
  check_ctg_layout_mem();
  printf("Storing layout for contig %d.\n",currentctg);
  store_single_ctg_layout(currentctg);
  update=TRUE;
}

/*                    DEF: store_all_ctg_callback
 *Layout menu -- Store layout for all contigs*/
static void
store_all_ctg_callback(GtkWidget *widget, gpointer data){
  int i;

  check_ctg_layout_mem();
  if(messQuery("Are you sure you want to overwrite all previous layouts?")){
    printf("Storing layout for all contigs.\n");
    for(i=1; i<=max_contig; i++){
      ctg_layout[i].defined=FALSE;
    }
    store_single_ctg_layout(0);
    update=TRUE;
  }
}

/*                    DEF: store_default_ctg_callback
 *Layout menu -- Store layout as default*/
static void
store_default_ctg_callback(GtkWidget *widget, gpointer data){
  check_ctg_layout_mem();
  printf("Storing default contig layout.\n");
  store_single_ctg_layout(0);
  update=TRUE;
}

/*                    DEF: size_normal_callback
 *Size options menu -- Normal*/
static void
size_normal_callback(){
  int ctg;
  GtkWidget *item= gtk_item_factory_get_item (item_factory, "/Size options/Normal");

  if (GTK_CHECK_MENU_ITEM (item)->active) {
    size_option=SIZE_NORMAL;
    ctg=currentctg;           /* Close ctg and reopen with new size specs*/
    gtk_widget_destroy(ctg_window);
    gtk_ctgdisplay(ctg);
  }
}

/*                    DEF: size_no_stats_callback
 *Size options menu -- No contig stats*/
static void
size_no_stats_callback(){
  int ctg;
  GtkWidget *item= gtk_item_factory_get_item (item_factory, "/Size options/No contig stats");

  if (GTK_CHECK_MENU_ITEM (item)->active){
    size_option=SIZE_NO_STATS;
    ctg=currentctg;           /* Close ctg and reopen with new size specs*/
    gtk_widget_destroy(ctg_window);
    gtk_ctgdisplay(ctg);
  }
}

/*                    DEF: size_tracks_only_callback
 *Size options menu -- Tracks only*/
static void
size_tracks_only_callback(){
  int ctg;
  GtkWidget *item= gtk_item_factory_get_item (item_factory, "/Size options/Tracks only");

  if (GTK_CHECK_MENU_ITEM (item)->active){
    size_option=SIZE_TRACKS_ONLY;
    ctg=currentctg;           /* Close ctg and reopen with new size specs*/
    gtk_widget_destroy(ctg_window);
    gtk_ctgdisplay(ctg);
  }
}

/*                    DEF: zoom_in_callback
 *Size options menu -- Zoom in*/
static void
zoom_in_callback(){
  display_zoom = MIN(display_zoom+1, 31);
  gtk_adjustment_set_value(GTK_ADJUSTMENT(zoom_adj), display_zoom);
}

/*                    DEF: zoom_out_callback
 *Size options menu -- Zoom out*/
static void
zoom_out_callback(){
  display_zoom = MAX(display_zoom-1, 0);
  gtk_adjustment_set_value(GTK_ADJUSTMENT(zoom_adj), display_zoom);
}

/*                    DEF: refresh_gtk_ctgdisplay
 *Refill the track structures and redraw, but don't recompute the
 *FPC structures or reload tracks.*/
void
update_track_pixmaps()
{
    int i,w,h;
    GtkTrack* t;

    for(i=0; i<track_num; i++)
    {
        t = GTK_TRACK(tracks[i]);
        if (t->pixmap) 
		{
			gdk_pixmap_unref(t->pixmap);
			t->pixmap = 0;
		}
        gdk_drawable_get_size(GTK_WIDGET(t)->window,&w,&h);        
        t->pixmap = gdk_pixmap_new(GTK_WIDGET(t)->window,w,h,-1);
    }

}
void
refresh_gtk_ctgdisplay()
{
  int i;

  if(ctg_window==NULL) return;
  checked_width=FALSE;
  for(i=0; i<track_num; i++){
    gtk_track_clear_data(GTK_TRACK(tracks[i]));
    gtk_track_fill_data(GTK_TRACK(tracks[i]));
    compute_track_size(GTK_TRACK(tracks[i]));
  }
  update_page_numbers();
  update_fpcruler((GtkWidget*)main_hadj,(GtkWidget*)fpcruler);

  /* Call do_centre_stuff once all tracks have been realized*/
  gtk_idle_add((GtkFunction) do_centre_stuff, NULL);
}

void draw_region_lines()
{
    int w,h;
    GtkWidget* widget = GTK_WIDGET(ctg_window);

    gdk_drawable_get_size(widget->window,&w,&h);

    if (pos_pixmap)
    {
        gdk_draw_pixmap (widget->window,
			        pos_gc,
			        pos_pixmap,
			        0,0,0,0,w,h);
    }
}


/*                    DEF: gtk_ctgdisplay
 *The main function that draws the ctgdisplay*/
int
gtk_ctgdisplay(int ctg)
{
  GtkWidget *vbox, *vbox2;
  GtkWidget *hbox, *hbox2;
  GtkWidget *table;
  GtkWidget *frame;
  GtkWidget *spacer_hbox;
  GtkWidget *zoom_hscale;
  GtkWidget *hscrollbar;
  GtkWidget *scrolled_window;
  GtkWidget *whole_button;

  GtkWidget *label;
  GtkWidget *search_entry;
  GdkColor color;
  char ctg_msg_str[150];
  char chr_str[10] = "";
  char mess_str[300];
  char user_str[150];
  char clones_str[30];
  char markers_str[30];
  char sequenced_str[30];
  char length_str[30];
  char page_entry1_str[7];
  char page_entry2_str[7];
  int index=0;
  float zoom_val=1.0;
  int chromosome;
  struct contig *cptr;
  CLONE *clp;
  int num_buried=0;
  int same_ctg=FALSE;
  int ctg_size_changed=FALSE;
  GtkWidget *menubar;
  float avg_bands = 30;
  int horiz_size;
  GtkTextBuffer* buffer;
  GtkTextIter it1;
  GtkTextTag* tag;

  GdkPixmap *pixmap;
  GdkBitmap *mask;

  /* WN 10/3/03 - CB map calc followed by OK can result in empty contig; do not display */
  if (contigs[ctg].count == 0) {
     if (ctg_window != NULL) {
        gtk_widget_destroy(ctg_window);
        ctg_window = NULL;
     }
     return 0;
  }

  if (Proj.tot_bands > 0) {
     avg_bands = ((float)Proj.tot_bands)/((float)arrayMax(acedata));
  }
  horiz_scale_factor = 0.8*30.0/avg_bands;

  if (strlen(fileName) < 65)
    sprintf(ctg_window_title,"FPC Ctg%d %s", ctg, fileName);
  else sprintf(ctg_window_title,"FPC Ctg%d", ctg);

  g_snprintf(ctg_msg_str, 149, "Ctg%d of %s\n",ctg, fileName);

  find_Clone(ctg);   /* get the clones and store data in struct contig */
  orderclones(ctg);
  if(currentctg==ctg) same_ctg=TRUE;
  currentctg = ctg;
  setmarkerposctg(); /* get markers*/
  sortmarkers();
  if((contigs[currentctg].left != saved_left_end) ||
     (contigs[currentctg].right != saved_right_end))
    ctg_size_changed=TRUE;
  saved_left_end = contigs[currentctg].left;
  saved_right_end = contigs[currentctg].right;

  barhighlighted=TRUE;
  clhigh=NULL;
  highmark=-1;
  highremark=-1;
  highseq = 0;

  if(ctg_layout==NULL) load_layout();
  else check_ctg_layout_mem();

  if(ctg_layout[currentctg].defined) index = currentctg;

  if(!same_ctg || ctg_size_changed){
    page_end_left = contigs[currentctg].left;
    page_end_right = MIN(Proj.default_page_size-1, contigs[currentctg].right);
    if(page_end_left >= page_end_right) page_end_left=0;
    page_size = page_end_right-page_end_left+1;
  }

  for(cptr=root;cptr!=NULL;cptr=cptr->new){   /*"new" is next; "next" is clone index*/
    clp=arrp(acedata, cptr->next, CLONE);
    if(clp->mattype & (EXACT | APPROX | PSEUDO)) num_buried++;
    if(!(same_ctg || showburied==3)){
      /* Clear selected and highlight fields in fpc structs*/
      clp=arrp(acedata, cptr->next, CLONE);
      clp->selected=clp->highcol=0;
    }
  }

  /* WMN 9/19/06 determine max and starting zoom value to avoid overflow values */

  zoom_max = (float)(int)(MAXIMUM_DISPLAY_WIDTH/((page_end_right - page_end_left + 1)*horiz_scale_factor));
  if (zoom_max > 30.0) {
    zoom_max = 30.0;
  }
  else if (zoom_max < 5) {
     sprintf(mess_str,"The contig is too large to be displayed with the current default page size of %d.\nReduce the page size setting on the Configuration page.\n",Proj.default_page_size);
     messout(mess_str);
     return 0;
  }


  if(!same_ctg){
    if(showburied==3) showburied=0;  /*3 means show everything; override layout*/
    else showburied = ctg_layout[index].bury;
    zoom_val = ctg_layout[index].zoom;
    if (zoom_val < 1) {
        zoom_val = 5;
    }
    if (zoom_val > zoom_max) {
       zoom_val = zoom_max - 1;
    }
  }

  if(ctg_window != NULL){
    if(!same_ctg) remove_all_tracks();
    gtk_window_set_title (GTK_WINDOW (ctg_window), ctg_window_title);

    /* Set ctg remarks*/
    gtk_frame_set_label(GTK_FRAME(ctg_rem_frame), ctg_msg_str);
    sprintf(mess_str,"%s\n%s", contigs[currentctg].chr_msg,
                               contigs[currentctg].user_msg);
    if(sscanf(contigs[currentctg].chr_msg, "Chr%d", &chromosome)==1){
      sprintf(chr_str, "Chr%d", chromosome);
      sprintf(mess_str,"%s\n",&(contigs[currentctg].chr_msg[strlen(chr_str)]));
      sprintf(user_str,"%s", contigs[currentctg].user_msg);
    }
    else{
      sprintf(mess_str,"%s\n",contigs[currentctg].chr_msg);
      sprintf(user_str,"%s", contigs[currentctg].user_msg);
    }

  GtkTextIter chr_start,chr_end,user_start,user_end,mess_start,mess_end;
  GtkTextTag *tag;
  int ichr_start,imess_start,iuser_start;

    buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(ctgrem_text_box));
    gtk_text_buffer_set_text(buffer,"",-1);

    if(gdk_color_parse("#228B22", &color)){
      if(!gdk_colormap_alloc_color(gdk_colormap_get_system(), &color, FALSE, TRUE)){
	g_warning("Could not allocate color");
      }
    }
    ichr_start = 0;
    gtk_text_buffer_insert_at_cursor(buffer,  chr_str, -1);
    imess_start = gtk_text_buffer_get_char_count(buffer);
    gtk_text_buffer_insert_at_cursor(buffer,  mess_str, -1);

    if(gdk_color_parse("blue", &color)){
      if(!gdk_colormap_alloc_color(gdk_colormap_get_system(), &color, FALSE, TRUE)){
	g_warning("Could not allocate color");
      }
    }

    iuser_start = gtk_text_buffer_get_char_count(buffer);
    gtk_text_buffer_insert_at_cursor(buffer,  user_str, -1);

    gtk_text_buffer_get_iter_at_offset(buffer,&chr_start,0);
    gtk_text_buffer_get_iter_at_offset(buffer,&chr_end,imess_start);

    gtk_text_buffer_get_iter_at_offset(buffer,&mess_start,imess_start);
    gtk_text_buffer_get_iter_at_offset(buffer,&mess_end,iuser_start);

    gtk_text_buffer_get_iter_at_offset(buffer,&user_start,iuser_start);
    gtk_text_buffer_get_end_iter(buffer,&user_end);

  tag = gtk_text_buffer_create_tag (buffer, 0,
	   		            "foreground", "#228b22", NULL);  
  gtk_text_buffer_apply_tag (buffer, tag, &chr_start, &chr_end);
//  tag = gtk_text_buffer_create_tag (buffer, 0,
//	   		            "foreground", "red", NULL);  
//  gtk_text_buffer_apply_tag (buffer, tag, &mess_start, &mess_end);
  tag = gtk_text_buffer_create_tag (buffer, 0,
	   		            "foreground", "blue", NULL);  
  gtk_text_buffer_apply_tag (buffer, tag, &user_start, &user_end);

    /* Set ctg stats*/
    sprintf(clones_str, "Clones: %d (%d buried)", contigs[ctg].count, num_buried);
    gtk_label_set_text(GTK_LABEL(clones_label), clones_str);

    sprintf(markers_str, "Markers: %d", contigs[ctg].markers);
    gtk_label_set_text(GTK_LABEL(markers_label), markers_str);

    sprintf(sequenced_str, "Sequenced: %d", contigs[ctg].seq);
    gtk_label_set_text(GTK_LABEL(sequenced_label), sequenced_str);

    sprintf(length_str, "Length: %d CB units",
            contigs[currentctg].right-contigs[currentctg].left+1);
    gtk_label_set_text(GTK_LABEL(length_label), length_str);


    /* Set buried state*/
    if(showburied==0){
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(yes_radiobutton), TRUE);
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(no_radiobutton), FALSE);
    }
    else{
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(yes_radiobutton), FALSE);
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(no_radiobutton), TRUE);
    }

    if(!same_ctg) gtk_adjustment_set_value(GTK_ADJUSTMENT(zoom_adj), zoom_val);

    if(same_ctg) refresh_gtk_ctgdisplay();
    else load_tracks(main_hadj);
    gtk_widget_show_all (ctg_window);
    update_page_numbers();
    return 0;
  }

  add_entity=MARKERS;
  store_layout=THIS_CTG;
  track_num=0;
  bar_active=0;
  trail=markerTrail=0;

  ctg_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  /* WMN 10/19/06 add some code to try to detect dual-screen monitors and put display in just one screen */
  if(size_option==SIZE_NORMAL) {
    horiz_size =  (gdk_screen_width() < 2000 ? gdk_screen_width()/1.3 : gdk_screen_width()/2.1);
    gtk_window_set_default_size(GTK_WINDOW (ctg_window),horiz_size,gdk_screen_height()/1.2);
  }
  else if(size_option==SIZE_NO_STATS) {
    horiz_size =  (gdk_screen_width() < 2000 ? gdk_screen_width()/1.5 : gdk_screen_width()/2.2);
    gtk_window_set_default_size(GTK_WINDOW (ctg_window),horiz_size,gdk_screen_height()/1.2);
  }
  else if(size_option==SIZE_TRACKS_ONLY) {
    horiz_size =  (gdk_screen_width() < 2000 ? gdk_screen_width()/1.5 : gdk_screen_width()/2.2);
    gtk_window_set_default_size(GTK_WINDOW (ctg_window),horiz_size,gdk_screen_height()/1.4);
  }
  gtk_window_set_title (GTK_WINDOW (ctg_window), ctg_window_title);
//  gtk_window_set_policy (GTK_WINDOW (ctg_window), FALSE, TRUE, FALSE);
  gtk_window_set_resizable (GTK_WINDOW (ctg_window),TRUE);

  main_hadj = gtk_adjustment_new (0.0, 0.0, 0.0, 1.0, 1.0, 1.0);

  vbox=gtk_vbox_new(FALSE, 5);
  gtk_container_add (GTK_CONTAINER (ctg_window), vbox);

  gtk_signal_connect (GTK_OBJECT (ctg_window), "destroy",
		      GTK_SIGNAL_FUNC (close_ctg_window), NULL);

  hbox=gtk_hbox_new(FALSE,1);
  get_main_menu (ctg_window, &menubar);
  gtk_box_pack_start (GTK_BOX (hbox), menubar, TRUE, TRUE, 0);
  gtk_widget_show (menubar);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

  switch(size_option){
    case SIZE_NORMAL:
      GTK_CHECK_MENU_ITEM (
        gtk_item_factory_get_item (item_factory, "/Size options/Normal"))->active=TRUE;
      GTK_CHECK_MENU_ITEM (
        gtk_item_factory_get_item (item_factory, "/Size options/No contig stats"))->active=FALSE;
      GTK_CHECK_MENU_ITEM (
        gtk_item_factory_get_item (item_factory, "/Size options/Tracks only"))->active=FALSE;
      break;
    case SIZE_NO_STATS:
      GTK_CHECK_MENU_ITEM (
        gtk_item_factory_get_item (item_factory, "/Size options/Normal"))->active=FALSE;
      GTK_CHECK_MENU_ITEM (
        gtk_item_factory_get_item (item_factory, "/Size options/No contig stats"))->active=TRUE;
      GTK_CHECK_MENU_ITEM (
        gtk_item_factory_get_item (item_factory, "/Size options/Tracks only"))->active=FALSE;
      break;
    case SIZE_TRACKS_ONLY:
      GTK_CHECK_MENU_ITEM (
        gtk_item_factory_get_item (item_factory, "/Size options/Normal"))->active=FALSE;
      GTK_CHECK_MENU_ITEM (
        gtk_item_factory_get_item (item_factory, "/Size options/No contig stats"))->active=FALSE;
      GTK_CHECK_MENU_ITEM (
        gtk_item_factory_get_item (item_factory, "/Size options/Tracks only"))->active=TRUE;
      break;
  }

  table = gtk_table_new(2,2,FALSE);
  if(size_option!=SIZE_TRACKS_ONLY) /*Only show top of display if Tracks only not set*/
    gtk_box_pack_start (GTK_BOX (vbox), table, FALSE, FALSE, 0);

  hbox=gtk_hbox_new(FALSE,7);

  /* The zoom adjustments*/
  /* WMN 9/19/06 change to use zoom_max to prevent out of range values */ 
  frame = gtk_frame_new("Zoom");
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, FALSE, 0);
  gtk_widget_show(frame);
  hbox2=gtk_hbox_new(FALSE,3);
  gtk_container_set_border_width (GTK_CONTAINER (hbox2), 2);

  zoom_adj = gtk_adjustment_new (zoom_val, 0.1, zoom_max, 0.1, 1.0, 1.0);
  gtk_signal_connect (GTK_OBJECT (zoom_adj), "value-changed",
		      GTK_SIGNAL_FUNC (update_zoom), NULL);
  zoom_hscale=gtk_hscale_new(GTK_ADJUSTMENT(zoom_adj));
  gtk_scale_set_digits(GTK_SCALE(zoom_hscale), 1);
  update_zoom(GTK_OBJECT (zoom_adj),0);
  //gtk_adjustment_set_value(GTK_ADJUSTMENT(zoom_adj), zoom_val);
  gtk_widget_set_usize(zoom_hscale,130,-2);
  gtk_range_set_update_policy(GTK_RANGE(zoom_hscale),GTK_UPDATE_DELAYED);
  gtk_widget_show(zoom_hscale);
  gtk_box_pack_start (GTK_BOX (hbox2), zoom_hscale, FALSE, FALSE, 0);

  whole_button = gtk_button_new_with_label("Whole");
  gtk_signal_connect (GTK_OBJECT (whole_button), "clicked",
		      GTK_SIGNAL_FUNC (whole_callback), zoom_adj);
  gtk_widget_show (whole_button);
  gtk_box_pack_start (GTK_BOX (hbox2), whole_button, FALSE, FALSE, 0);

  gtk_container_add(GTK_CONTAINER(frame), hbox2);

  /* The radiobuttons that determine which clones are hidden*/
  frame = gtk_frame_new("Show buried clones");
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, FALSE, 0);
  gtk_widget_show(frame);
  hbox2=gtk_hbox_new(FALSE,3);
  gtk_container_set_border_width (GTK_CONTAINER (hbox2), 2);

  yes_radiobutton = gtk_radio_button_new_with_label(NULL,"Yes");
  gtk_signal_connect (GTK_OBJECT (yes_radiobutton), "clicked",
		      GTK_SIGNAL_FUNC (yes_radiobutton_callback), NULL);
  gtk_box_pack_start (GTK_BOX (hbox2), yes_radiobutton, FALSE, FALSE, 0);

  no_radiobutton = gtk_radio_button_new_with_label(
                       gtk_radio_button_group(GTK_RADIO_BUTTON(yes_radiobutton)),
                       "No");
  gtk_signal_connect (GTK_OBJECT (no_radiobutton), "clicked",
		      GTK_SIGNAL_FUNC (no_radiobutton_callback), NULL);
  gtk_box_pack_start (GTK_BOX (hbox2), no_radiobutton, FALSE, FALSE, 0);

  gtk_container_add(GTK_CONTAINER(frame), hbox2);

  frame = gtk_frame_new("Search");
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, FALSE, 0);
  gtk_widget_show(frame);
  hbox2=gtk_hbox_new(FALSE,3);
  gtk_container_set_border_width (GTK_CONTAINER (hbox2), 2);

  search_entry=gtk_entry_new_with_max_length(CLONE_SZ);
  gtk_signal_connect (GTK_OBJECT (search_entry), "activate",
		      GTK_SIGNAL_FUNC (search_tracks), NULL);
  gtk_box_pack_start(GTK_BOX(hbox2), search_entry, FALSE, FALSE, 0);
  gtk_widget_show(search_entry);

  gtk_container_add(GTK_CONTAINER(frame), hbox2);

  /* Paging info*/
  frame = gtk_frame_new("CB Unit Range");
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, FALSE, 0);
  gtk_widget_show(frame);
  hbox2=gtk_hbox_new(FALSE,5);
  gtk_container_set_border_width (GTK_CONTAINER (hbox2), 2);


  /* Create pixmap widgets for paging arrows*/
  pixmap=gdk_pixmap_colormap_create_from_xpm_d(NULL, gdk_colormap_get_system(), &mask,
                                               NULL, left_active_xpm);
  lpixmapwid = gtk_pixmap_new( pixmap, mask );
  gdk_pixmap_unref(pixmap);

  pixmap=gdk_pixmap_colormap_create_from_xpm_d(NULL, gdk_colormap_get_system(), &mask,
                                               NULL, right_active_xpm);
  rpixmapwid = gtk_pixmap_new( pixmap, mask );
  gdk_pixmap_unref(pixmap);

  move_left_button = gtk_button_new();
  gtk_container_add (GTK_CONTAINER (move_left_button), lpixmapwid);
  gtk_signal_connect (GTK_OBJECT (move_left_button), "clicked",
		      GTK_SIGNAL_FUNC (move_left_callback), zoom_adj);
  gtk_box_pack_start (GTK_BOX (hbox2), move_left_button, TRUE, FALSE, 0);

  page_entry1 = gtk_entry_new_with_max_length(6);
  gtk_widget_set_usize(page_entry1, 50,-2);
  gtk_signal_connect (GTK_OBJECT (page_entry1), "activate",
		      GTK_SIGNAL_FUNC (update_page_view), NULL);
  sprintf(page_entry1_str,"%d",page_end_left);
  gtk_box_pack_start(GTK_BOX(hbox2), page_entry1, TRUE, FALSE, 0);

  label = gtk_label_new("to");
  gtk_box_pack_start(GTK_BOX(hbox2), label, TRUE, FALSE, 0);
  gtk_widget_show(label);

  page_entry2 = gtk_entry_new_with_max_length(6);
  gtk_widget_set_usize(page_entry2, 50,-2);
  gtk_signal_connect (GTK_OBJECT (page_entry2), "activate",
		      GTK_SIGNAL_FUNC (update_page_view), NULL);
  sprintf(page_entry2_str,"%d",page_end_right);
  gtk_box_pack_start(GTK_BOX(hbox2), page_entry2, TRUE, FALSE, 0);

  move_right_button = gtk_button_new();
  gtk_container_add (GTK_CONTAINER (move_right_button), rpixmapwid);
  gtk_signal_connect (GTK_OBJECT (move_right_button), "clicked",
		      GTK_SIGNAL_FUNC (move_right_callback), zoom_adj);
  gtk_box_pack_start (GTK_BOX (hbox2), move_right_button, TRUE, FALSE, 0);

  gtk_container_add(GTK_CONTAINER(frame), hbox2);

  if(size_option==SIZE_NORMAL) /*Contig stats are shown in second column*/
    gtk_table_attach_defaults(GTK_TABLE(table), hbox, 0,1, 0,1);
  else if(size_option==SIZE_NO_STATS) /*No contig stats -- use both columns*/
    gtk_table_attach_defaults(GTK_TABLE(table), hbox, 0,2, 0,1);

  /* Contig remarks*/
  hbox=gtk_hbox_new(FALSE,7);
  ctg_rem_frame = gtk_frame_new(ctg_msg_str);
  gtk_box_pack_start (GTK_BOX (hbox), ctg_rem_frame, TRUE, TRUE, 0);
  gtk_widget_show(ctg_rem_frame);
  hbox2=gtk_hbox_new(FALSE,7);
  gtk_container_set_border_width (GTK_CONTAINER (hbox2), 2);

  if(sscanf(contigs[currentctg].chr_msg, "Chr%d", &chromosome)==1){
    sprintf(chr_str, "Chr%d", chromosome);
    sprintf(mess_str,"%s\n",&(contigs[currentctg].chr_msg[strlen(chr_str)]));
    sprintf(user_str,"%s", contigs[currentctg].user_msg);
  }
  else{
    sprintf(mess_str,"%s\n",contigs[currentctg].chr_msg);
    sprintf(user_str,"%s", contigs[currentctg].user_msg);
  }
  ctgrem_text_box = gtk_text_view_new();
  //gtk_widget_set_size_request(ctgrem_text_box, -2,34);
  gtk_text_view_set_editable(GTK_TEXT_VIEW(ctgrem_text_box), FALSE);
  if(gdk_color_parse("RGB:22/8B/22", &color)){
    if(!gdk_colormap_alloc_color(gdk_colormap_get_system(), &color, FALSE, TRUE)){
      g_warning("Could not allocate color");
    }
  }


  buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(ctgrem_text_box));

  //BROKE
  tag = gtk_text_buffer_create_tag (buffer, NULL, "foreground", "blue", NULL);  

  gtk_text_buffer_get_end_iter(buffer,&it1);
  gtk_text_buffer_insert_with_tags(buffer, &it1,chr_str, -1,tag,NULL);
  gtk_text_buffer_get_end_iter(buffer,&it1);
  gtk_text_buffer_insert_with_tags(buffer, &it1,mess_str, -1,tag,NULL);


  if(gdk_color_parse("blue", &color)){
    if(!gdk_colormap_alloc_color(gdk_colormap_get_system(), &color, FALSE, TRUE)){
      g_warning("Could not allocate color");
    }
  }

  gtk_text_buffer_insert_at_cursor(buffer, user_str, -1);

  gtk_widget_show (ctgrem_text_box);
  gtk_box_pack_start (GTK_BOX (hbox2), ctgrem_text_box, TRUE, TRUE, 0);

  gtk_container_add(GTK_CONTAINER(ctg_rem_frame), hbox2);

  if(size_option==SIZE_NORMAL) /*Contig stats are shown in second column*/
    gtk_table_attach_defaults(GTK_TABLE(table), hbox, 0,1, 1,2);
  else if(size_option==SIZE_NO_STATS) /*No contig stats -- use both columns*/
    gtk_table_attach_defaults(GTK_TABLE(table), hbox, 0,2, 1,2);

  /* Contig stats*/
  frame = gtk_frame_new("Contig stats");
  if(size_option!=SIZE_NO_STATS)  /*Only attach stats if wanted.*/
    gtk_table_attach_defaults(GTK_TABLE(table), frame, 1,2, 0,2);
  gtk_widget_show(frame);
  vbox2=gtk_vbox_new(TRUE,0);
  gtk_container_set_border_width (GTK_CONTAINER (vbox2), 2);

  sprintf(clones_str, "Clones: %d (%d buried)", contigs[ctg].count, num_buried);
  clones_label = gtk_label_new(clones_str);
  gtk_box_pack_start(GTK_BOX(vbox2), clones_label, FALSE, FALSE, 0);

  sprintf(markers_str, "Markers: %d", contigs[ctg].markers);
  markers_label = gtk_label_new(markers_str);
  gtk_box_pack_start(GTK_BOX(vbox2), markers_label, FALSE, FALSE, 0);

  sprintf(sequenced_str, "Sequenced: %d", contigs[ctg].seq);
  sequenced_label = gtk_label_new(sequenced_str);
  gtk_box_pack_start(GTK_BOX(vbox2), sequenced_label, FALSE, FALSE, 0);

  sprintf(length_str, "Length: %d CB units",
          contigs[currentctg].right-contigs[currentctg].left+1);
  length_label = gtk_label_new(length_str);
  gtk_box_pack_start(GTK_BOX(vbox2), length_label, FALSE, FALSE, 0);

  gtk_container_add(GTK_CONTAINER(frame), vbox2);



  hbox=gtk_hbox_new(FALSE,0);
  spacer_hbox=gtk_hbox_new(FALSE,0);
  gtk_widget_set_size_request(spacer_hbox, SCROLLBAR_WIDTH, 0);
  gtk_box_pack_start(GTK_BOX(hbox), spacer_hbox, FALSE, FALSE, 0);

  fpcruler = gtk_hfpcruler_new();
  gtk_fpcruler_set_metric(GTK_FPC_RULER(fpcruler), display_zoom);
  gtk_signal_connect (GTK_OBJECT (main_hadj), "value-changed",
		      GTK_SIGNAL_FUNC (update_fpcruler), fpcruler);
//  gtk_signal_connect (GTK_OBJECT (main_hadj), "changed",
//		      GTK_SIGNAL_FUNC (update_fpcruler), fpcruler);
  gtk_signal_connect (GTK_OBJECT (fpcruler), "motion_notify_event",
		      GTK_SIGNAL_FUNC (update_region), NULL);

  gtk_box_pack_start(GTK_BOX(hbox), fpcruler, TRUE, TRUE, 0);
  gtk_box_pack_end (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
  gtk_widget_show (fpcruler);
#define EVENT_METHOD(i, x) GTK_WIDGET_GET_CLASS(i)->x
//  g_signal_connect_swapped (G_OBJECT (fpcruler), "drag_motion_event",
//                              G_CALLBACK( EVENT_METHOD (fpcruler, motion_notify_event)),
//                              G_OBJECT (fpcruler));
//  g_signal_connect_swapped (G_OBJECT (fpcruler), "button_press_event",
//                              G_CALLBACK (EVENT_METHOD (fpcruler, button_press_event)),
//                              G_OBJECT (fpcruler));
//  g_signal_connect_after (G_OBJECT (ctg_window), "expose_event",gtk_hfpcruler_expose,0);

/*
 g_signal_connect (G_OBJECT (ctg_window), "expose_event",
                             ctg_window_expose_event,0);
 g_signal_connect (G_OBJECT (ctg_window), "configure_event",
                             ctg_window_configure_event,0);
 g_signal_connect (G_OBJECT (ctg_window), "realize",
                             ctg_window_realize,0);
*/

  scrolled_window = gtk_scrolled_window_new(NULL,NULL);

  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window),
				 GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start(GTK_BOX(vbox), scrolled_window, TRUE, TRUE, 0);


  /*Load tracks*/
  tracks_vbox = gtk_vbox_new(FALSE,0);   /*Container for adding tracks to*/
  gtk_container_set_border_width (GTK_CONTAINER (tracks_vbox), 3);

  load_tracks(main_hadj);

  gtk_widget_show (tracks_vbox);

  /* Put fpcruler at bottom*/
  hbox=gtk_hbox_new(FALSE,0);
  spacer_hbox=gtk_hbox_new(FALSE,0);
  gtk_widget_set_size_request(spacer_hbox, SCROLLBAR_WIDTH - 15, 0);
  gtk_box_pack_start(GTK_BOX(hbox), spacer_hbox, FALSE, FALSE, 0);

  hscrollbar = gtk_hscrollbar_new (GTK_ADJUSTMENT (main_hadj));
  gtk_box_pack_start(GTK_BOX(hbox), hscrollbar, TRUE, TRUE, 0);

  spacer_hbox=gtk_hbox_new(FALSE,0);
  gtk_widget_set_size_request(spacer_hbox, 5, 0);
  gtk_box_pack_start(GTK_BOX(hbox), spacer_hbox, FALSE, FALSE, 0);

  gtk_box_pack_end (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
  gtk_widget_show (hscrollbar);

  gtk_scrolled_window_add_with_viewport (
		 GTK_SCROLLED_WINDOW (scrolled_window), tracks_vbox);

  /* Set buried state*/
  if(showburied==0){
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(yes_radiobutton), TRUE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(no_radiobutton), FALSE);
  }
  else{
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(yes_radiobutton), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(no_radiobutton), TRUE);
  }

  gtk_widget_show_all (ctg_window);
  gtk_widget_set_app_paintable(ctg_window,1);
  update_page_numbers();
  int left_offset = page_end_left * display_zoom * horiz_scale_factor;
  int left_start = GTK_ADJUSTMENT(main_hadj)->value + left_offset;
   gtk_fpcruler_set_range(GTK_FPC_RULER(fpcruler),left_start-1,
			left_start + GTK_ADJUSTMENT(main_hadj)->page_size+1,
			left_start-1,
			left_start + GTK_ADJUSTMENT(main_hadj)->page_size+1,
			GTK_ADJUSTMENT(main_hadj)->upper,
			1);
//  refresh_gtk_ctgdisplay();
    
  return 0;
}
