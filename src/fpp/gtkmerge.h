/****************************************************
 * File: gtkmerge.h
 *
 * Author: Fred Engler
 ****************************************************/

#include "gtktrack.h"

GtkWidget *merge_window1=NULL, *merge_window2=NULL;  /* The merge contig dialog windows*/

int mergeoffset;    /* Start in cb units of the newly added ctg*/
int mergespace;     /* Space between contigs (negative if overlap)*/

/* Function prototypes*/
void merge_display1();
