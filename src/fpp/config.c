/******************************************************************
                       config.c
written by IL
edited by CAS

3sept98 - Display PCR markers
30jul02 - Display REP markers  (fred)
*******************************************************************/
#include "fpp.h"
#include "clam.h"
#include <glib.h>
#include <sys/stat.h>
#include "mtp.h"
void defaultpick(int box,double x,double y);
void setdefaultflag(void);

extern void set_Zunc(), clear_Zunc(), read_filter();
extern void Read_unc(); /* clam/score.c - read uncertaity file */
extern int graphQueryOpen();

//extern void mtp_update_fp_class_pars(void);

static int variableArc =0 ;
static int eqArc =0, sulstonArc=0, sulstonfArc=0;
static int agaroseArc = 0, hicfArc = 0;

/*efriedr 3/15/02*/
static int gSizeBox;           /*Genome size stuff*/
static char gSizeText[12];
static int cSizeBox            /*Clone size ...*/;
static char cSizeText[12];
static int bSizeBox;           /*Band size ... */
static char bSizeText[12];

/*fred 9/30/02*/
static int gelLenBox;           /*Gel length stuff*/
static char gelLenText[12];

static int pageSizeBox;         /*Page size stuff*/
static char pageSizeText[12];

static void update_gSize();
static void update_cSize();
static void update_bSize();
static void update_gelLen();
static void update_pageSize();

void setunsomething()
{
  static char chdirname[DIR_BUFFER_SIZE],chfilename[FIL_BUFFER_SIZE];
  FILE *fpunsome=NULL;

  fpunsome = (FILE *)graphQueryOpen(chdirname,chfilename,"","r","Choose Tolerance file");
  if(fpunsome != NULL){
    sprintf(Proj.uncfile,"%s/%s",chdirname,chfilename);
    fclose(fpunsome);
 }
  Proj.variable=1;
  setdefaultflag();
  Read_unc();
}

void setfilter()
{
  static char chdirname[DIR_BUFFER_SIZE],chfilename[FIL_BUFFER_SIZE];
  FILE *fpunsome=NULL;

  fpunsome = (FILE *)graphQueryOpen(chdirname,chfilename,"","r","Choose Vector file");
  if(fpunsome != NULL){
    sprintf(Proj.filterfile,"%s/%s",chdirname,chfilename);
    fclose(fpunsome);
 }
  setdefaultflag();
  read_filter();
}


BOOL checkitout(char *temp)
{
  struct stat stbuf;

  if (temp[0]=='\0') {
      clear_Zunc();
      return TRUE;
  }
  if(stat(temp, &stbuf) == -1){
    printf("File %s does not exist.", temp);
    return FALSE;
  }
  strcpy(Proj.uncfile, temp);
  Proj.variable=TRUE;
  Read_unc();
  return TRUE;
}

void checkunfile(char *temp)
{
  BOOL junk;

  junk = checkitout(temp);
}
BOOL checkfilterout(char *temp)
{
  struct stat stbuf;

  if(stat(temp, &stbuf) == -1){
    if (Proj.filterfile[0] == '\0') {
      displaymess("No filter file, thersfore no filtering will be done.");
      return FALSE;
    }
    else displaymess("File does not exist.");
    return FALSE;
  }
  return TRUE;
}

void checkfilterfile(char *temp)
{
  BOOL junk;

  junk = checkfilterout(temp);
}

void configquit()
{
  char str1[FIL_BUFFER_SIZE+50];

  update_gSize(gSizeText);
  update_cSize(cSizeText);
  update_bSize(bSizeText);
  update_gelLen(gelLenText);
  update_pageSize(pageSizeText);
  if(strlen(Proj.uncfile)>0){
    if(!checkitout(Proj.uncfile)){
      sprintf(str1,"File %s does not exist do you still wish to quit?",Proj.uncfile);
	  /*  sness */
      /*if(graphQuery(str1)){ */
      if(messQuery(str1)){
	graphActivate(gconfig);
	graphDestroy();
	strcpy(Proj.uncfile,"");
      }
    }
    else
      graphDestroy();
  }
  else
    graphDestroy();
}

static void update_gSize(char *sizestr){   /*efriedr 3/15/02*/
   float size;
   long temp;

   temp=Proj.genomesize/1000;
   sscanf(sizestr,"%f",&size);
   Proj.genomesize=size*1000;   /*Convert kb to b*/
   if(temp!=(long)Proj.genomesize/1000)
      update=TRUE;
}

static void update_cSize(char *sizestr){   /*efriedr 3/22/02*/
   int size;
   int temp;

   temp=Proj.avginsertsize;
   sscanf(sizestr,"%d",&size);
   Proj.avginsertsize=size;
   if(temp!=Proj.avginsertsize)
      update=TRUE;
}

static void update_bSize(char *sizestr){   /*efriedr 3/22/02*/
   int size;
   int temp;

   temp=Proj.avgbandsize;
   sscanf(sizestr,"%d",&size);
   Proj.avgbandsize=size;
   if(temp!=Proj.avgbandsize)
      update=TRUE;
}

static void update_gelLen(char *sizestr){   /*fred 9/30/02*/
   int size;
   int temp;

   temp=Proj.gel_len;
   sscanf(sizestr,"%d",&size);
   Proj.gel_len=size;
   if(temp!=Proj.gel_len)
      update=TRUE;
}

static void update_pageSize(char *sizestr){   /*fred 7/24/03*/
   int size;
   int temp;

   temp=Proj.default_page_size;
   sscanf(sizestr,"%d",&size);
   Proj.default_page_size=size;
   if(temp!=Proj.default_page_size)
      update=TRUE;
}


void
update_fp_class_pars(void)
{
    switch (Pz.fp_class) {
    case AGAROSE:
        Pz.precompute = 0;
        break;
    case HICF:
        Pz.precompute = 1;
        break;
    default:
        g_assert_not_reached();
        break;
    }
    mtp_update_fp_class_pars();
}

void setdefaultflag()
{
  float row =1.5;
  int width, junk ;
  static MENUOPT quitmenu[] = {
  { (VoidRoutine)configquit, "Close"},
  { graphPrint, "Print Screen"},
  { 0, 0 } };

  if(graphActivate(gconfig)){
    graphClear();
    graphPop();
  }
  else{
    gconfig = graphCreate (TEXT_FIT,"Configure Display",.2,.2,.53,.34) ;
    graphMenu (quitmenu) ;
    graphRegister (PICK, defaultpick) ;
  }
  graphFitBounds (&width,&junk) ;

  //row+=1.5;
  graphButton("Tolerance File:",setunsomething,2.0,row);

  graphText("Variable Tolerance",28.0,row);

  variableArc = graphBoxStart();
  graphArc(26.0, row+0.5, 0.7, 0, 360);
  if (Proj.variable) graphFillArc(26.0, row+0.5, 0.4, 0, 360);
  graphBoxEnd();
  graphBoxDraw(variableArc, BLACK, TRANSPARENT);

  row+=1.5;
  graphTextEntry(Proj.uncfile,DIR_BUFFER_SIZE+FIL_BUFFER_SIZE,2.0,row,checkunfile);

  row+=2.0;
  graphText("Fast Sulston",4.0,row);
  sulstonfArc = graphBoxStart();
  graphArc(2.0, row+0.5, 0.7, 0, 360);
  if (Proj.eq2==0) graphFillArc(2.0, row+0.5, 0.4, 0, 360);
  graphBoxEnd();
  graphBoxDraw(variableArc, BLACK, TRANSPARENT);

  graphText("Pure Sulston",20.0,row);
  sulstonArc = graphBoxStart();
  graphArc(18.0, row+0.5, 0.7, 0, 360);
  if (Proj.eq2==2) graphFillArc(18.0, row+0.5, 0.4, 0, 360);
  graphBoxEnd();
  graphBoxDraw(variableArc, BLACK, TRANSPARENT);

  graphText("Equation 2",36.0,row);

  eqArc = graphBoxStart();
  graphArc(34.0, row+0.5, 0.7, 0, 360);
  if (Proj.eq2==1) graphFillArc(34.0, row+0.5, 0.4, 0, 360);
  graphBoxEnd();
  graphBoxDraw(eqArc, BLACK, TRANSPARENT);

  /*efriedr 3/15/02*/
  row+=2.0;
  sprintf(gSizeText,"%0.0f",Proj.genomesize/1000);
  graphText("Genome size",2.0,row);
  gSizeBox=graphTextEntry(gSizeText,8,15,row,update_gSize);
  graphText("kb",24,row);

  if(Proj.avginsertsize==0)
     sprintf(cSizeText,"150000");
  else
     sprintf(cSizeText,"%d",Proj.avginsertsize);
  graphText("Clone size",30,row);
  cSizeBox=graphTextEntry(cSizeText,7,41,row,update_cSize);
  graphText("b",49,row);
  row+=1.5;

  if(Proj.avgbandsize==0)
     sprintf(bSizeText,"4096");
  else
     sprintf(bSizeText,"%d",Proj.avgbandsize);
  graphText("Band size",20,row);
  bSizeBox=graphTextEntry(bSizeText,5,30,row,update_bSize);
  graphText("b",36,row);

  /*fred 9/30/02*/
  row+=2.0;
  sprintf(gelLenText,"%d",Proj.gel_len);
  graphText("Gel length",2.0,row);
  gelLenBox=graphTextEntry(gelLenText,8,15,row,update_gelLen);

  row+=2.0;
  sprintf(pageSizeText,"%d",Proj.default_page_size);
  graphText("Contig display page size",2.0,row);
  pageSizeBox=graphTextEntry(pageSizeText,8,29,row,update_pageSize);

  row += 2.0;
  graphText("Agarose", 4, row);
  agaroseArc = graphBoxStart();
  graphArc(2.0, row + 0.5, 0.7, 0, 360);
  if (Pz.fp_class == AGAROSE)
      graphFillArc(2.0, row + 0.5, 0.4, 0, 360);
  graphBoxEnd();
  graphBoxDraw(agaroseArc, BLACK, TRANSPARENT);

  graphText("HICF", 16, row);
  hicfArc = graphBoxStart();
  graphArc(14.0, row + 0.5, 0.7, 0, 360);
  if (Pz.fp_class == HICF)
      graphFillArc(14.0, row + 0.5, 0.4, 0, 360);
  graphBoxEnd();
  graphBoxDraw(hicfArc, BLACK, TRANSPARENT);

  row+=2.0;
  graphLine(0.0,row,width,row);
  row+=1.5;

  graphButton("Vector File:",setfilter,2.0,row);

  row+=1.5;
  graphTextEntry(Proj.filterfile,DIR_BUFFER_SIZE+FIL_BUFFER_SIZE,2.0,row,checkfilterfile);

  row+=2.0;
  graphButton("Close",configquit,21.0,row);

  graphRedraw();
}

void defaultpick(int box,double x,double y)
{
  if(box == 0)
    return;

  if(box == variableArc) {
    Proj.variable = !Proj.variable;
    if (Proj.variable) {
       if (Proj.uncfile[0] != '\0') Read_unc();
       else set_Zunc();
    }
    else fprintf(stdout,"Tolerance is %d \n", Pz.tol);
  }
  else if(box == eqArc) {
    Proj.eq2 = 1;
  }
  else if(box == sulstonArc) {
    Proj.eq2 = 2;
  }
  else if(box == sulstonfArc) {
    Proj.eq2 = 0;
  }
  else if (box == agaroseArc) {
      Pz.fp_class = AGAROSE;
      update_fp_class_pars();
  }
  else if (box == hicfArc) {
      Pz.fp_class = HICF;
      update_fp_class_pars();
  }
  else if(box == gSizeBox){
    gSizeBox=graphTextEntry(gSizeText,8,0,0,update_gSize);   /*efriedr 3/15/02*/
    return;
  }
  else if(box == cSizeBox){
    cSizeBox=graphTextEntry(cSizeText,7,0,0,update_cSize);   /*efriedr 3/22/02*/
    return;
  }
  else if(box == bSizeBox){
    bSizeBox=graphTextEntry(bSizeText,5,0,0,update_bSize);   /*efriedr 3/22/02*/
    return;
  }
  else if(box == gelLenBox){
    gelLenBox=graphTextEntry(gelLenText,8,0,0,update_gelLen);   /*fred 9/30/02*/
    return;
  }
  else if(box == pageSizeBox){
    pageSizeBox=graphTextEntry(pageSizeText,8,0,0,update_pageSize);
    return;
  }
  else{
    graphTextEntry(Proj.uncfile,DIR_BUFFER_SIZE+FIL_BUFFER_SIZE,0,0,checkunfile);
    return;
  }
  setdefaultflag();
}
