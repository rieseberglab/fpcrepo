/***********************************************************************
                       fpp.h
3sept98 - marker3Arc (and why is it here?)
30jul02 - marker5Arc (and why is it here?) (fred)
***********************************************************************/
#ifndef _FPP_H_
#define _FPP_H_
#include "fpshr.h"
#include "fpstruct.h"

/*#define MAP_TRACE yes*/
#define MALLOC_CHECK yes
#define NBANDS 2000
#define CLONE_MARKER_MAX 200

/* added for Linux */
/* WMN changed to 4-byte swap when unsigned shorts were changed to ints */
/* The effect should be to create a big-endian number */
/* Note x86 is little-endian, solaris is big */

#if defined(ALPHA) || defined(LINUX) || defined(X86)
#define SWAP_HALF(i) ( (((i) & 0xFF000000) >> 24) | (((i) & 0x00FF0000) >> 8) | (((i) & 0x0000FF00) << 8)  | (((i) & 0x000000FF) << 24))
#else
#define SWAP_HALF(i) (i)
#endif

#if defined(ALPHA) || defined(LINUX) || defined(X86)
#define SWAP_HALF16(i) ((((i) & 0xFF00) >> 8) | (((i) & 0x00FF) << 8))
#else
#define SWAP_HALF16(i) (i)
#endif

#define graphQuery messQuery

extern int showburied; /* int used to see if buried clones are displayed */

extern float ZOOM ; /* initialise zoom's to 1 and shifts to 0 */
extern float zoomfactor;
extern float NEWZOOM;
extern float YSHIFT;
extern float FPZOOM;
extern float FPYSHIFT;

extern int currentctg;
extern int currentclone;
extern int bandmax, bandmin;
extern float scale,fpheight;
extern int centre_pos;
extern int minx;
extern int lastbox;
extern float sizepercol;

#ifdef MYDEBUG
   extern BOOL scrcoor;
   extern BOOL info;
   extern BOOL fmap;
   extern BOOL regionflag;
   extern BOOL where;
#endif

extern float MAP_POS_X;
extern int gelmax;
extern int band,botscrollbox,gelscrollbar,gelcolstart;
extern int scaleband,keysetband,maxpage,upkey,downkey;
extern int MAP_BOX;
extern int fpcolstart;
extern BOOL fpmove;
extern BOOL deletefp;
extern BOOL fpflag;
extern BOOL displaybars;
extern BOOL markerReset;
extern BOOL update;     /* used to see if data has changed and wether to save before exiting */
extern Graph g1; /* vertical/horizontal map */
extern Graph g7; /* clone information */
extern Graph g2; /* list of contigs */
extern Graph g3; /* finger print maps */
extern Graph g4; /* mode (stop) display */
extern Graph g5; /* addclone display */
extern Graph gconfig; /* configure (global) display */
extern Graph gmessage; /* message display */
extern Graph gtest , gtrace ;
extern Graph gtextctg; /* contig displayed as text */
extern Graph gtextwhole; /* all of project displayed as text */
extern Graph gmarker;
extern Graph gselected;
extern Graph gselected2;
extern Graph gsearch;
extern Graph gcomment;
extern Graph gpace;
extern Graph gsubmit;
extern Graph gCtgChr;
extern Graph gCtgRmk;
extern float endpoints[MAXLEVEL][MAXCOL]; /* number of (sub colomns)levels, number of colomns allowed */

extern int colnum,vertconbox,hozconbox;
extern float colwidth;
extern char charmaxcol;
extern char reqctg[5];

extern REGION region[REGNUM];      /* can have up to 2 region bars */
extern int regdef;            /* index for region */
extern int regtop,regbot;     /* box numbers for region bar's */
extern int bar1,bar2;

extern int cloneindex[FPMAX]; /* clone index */
extern int clonefpindex[FPMAX]; /* clone's fp number  index */
extern int indexbox[FPMAX]; /* box for clone's fp */

/*extern Array acedata;*/

extern Array clonepos;

extern BOOL newctg;
extern BOOL newsettings;
extern BOOL preserve;
extern int HIGHLIGHT;
extern int FRIEND;
extern int SELECTED;
extern int PCRCOLOUR;
extern int BPYCOLOUR;
extern int MRKCOLOUR;

extern int lastctg;

extern int page; /* page number of ctg display 0 bieing the first */
extern int ctgmax;

extern struct contig *root;
extern struct contig *clhigh; /* pointer to the highlighted clone */
extern int highmark;
extern int highremark;  /*fred 4/22/03*/
extern FPC_SEQ* highseq;
extern FPC_SEQ* centreSequence;

extern int fphigh,tolerbox,fpcolourbox;
extern BOOL toler,fpcolour;
extern BOOL displaymap;
extern int classctg;
extern int main_classctg;
extern BOOL  dataloaded;
extern Graph gstart;
extern char projectname[80],chsearchglob[NAME_SZ],commentglob[COMMENT_SZ],rmcommentglob[COMMENT_SZ];
extern char chaddname[CLONE_SZ],chfpname[CLONE_SZ];
extern float hozheight;
extern int clonedisplayed;
extern struct list *listroot;
extern int listdisplay;

extern int geladdbox,fpflagbox;
extern BOOL geladd;
extern int gelcount, gelclone[GELMAX],gelcloneindex[GELMAX],gelhigh;
/* sness sep21/2000 */
extern int gel_trail[GELMAX];

extern int nYAC;
extern float yacwidth;
extern int anchorMiniContig;
extern int nProbe; /* nprobe number of rows for markers */
extern int nREM;

extern int Nmarkers;
extern BOOL minicontig;
/* idl end 4/5/95 */
extern int textSize;
extern int lastpicked,lastdbclicked;
extern TEXTHIGH texthigh;
extern int displaycloneindex;
/*  Last edited: Jun 14 09:54 1995 (il) */
extern int currentmarker;
extern struct markerdisplist *topmarkerlist, *popupmarkerlist;
extern BOOL merging;

/*il 14/8/95*/
extern BOOL trail;
extern BOOL barhighlighted,fpadd,zoominput;
extern int reqbox,zoombox;
extern char reqzoom[5];
extern int bar_active;
extern int fpmovebox,fpremovebox,fpaddbox;
extern BOOL togglejustchanged;
extern int trailbut;
extern int editmenustatus;
extern BOOL markeraddactive;
extern BOOL markerdelactive;
extern BOOL firstread;
/* edit stuff */

extern struct marker tempmarker;
extern MARKEREDITLIST *rootmarkerptr;
extern struct markerctgpos *temppos;
extern struct remark *tempremark,*editremarkptr;
extern struct remark *tempremarkfp;
extern CLONE clone2;
extern int contigbox,lasteditbox,xbox,ybox;
extern char chctg[4],chx[4],chy[4];
extern char chctgmark[4],chpos[4];
extern int active,lastremarkpicked;
extern Graph gmarkeredit2,gcloneedit,gdispmarkers,gmarkerpopup, gsequence;
extern EDIT *start;
extern BOOL newmarker;
extern int editmarkerstatus,editmarker,editclonestatus;
extern int parentbox;
extern char chparent[12];
/* end edit stuff */
extern int  mergecontig1,mergecontig2,startpt;
extern BOOL okaytowrite,writeaccess;
extern int searchtype;
extern char timestamp[255];
extern struct preslist *preservelist;
extern BOOL PRTMOVE;
extern int defaultflag;
extern int alloc_contig;
extern float boxstart;
extern char reqclone[CLONE_SZ]; /* FIX 30jun99 */
extern char req2ctg[30];
extern BOOL debug;
extern int num_of_bands;
extern int markerdisplayed;
extern FPC_SEQ* seqdisplayed;

int SHARED_OPTION;
int SIM_FLAG;
#endif
