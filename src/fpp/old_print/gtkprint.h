/****************************************************
 * File: gtkprint.h
 *
 * Author: Fred Engler
 ****************************************************/

#define MAX_PRINT_COMMAND_LEN 30   /* Maximum length of the print command*/

#ifndef NO_IMLIB
#include "gtktrack.h"

#include <gdk_imlib.h>


/* Function prototypes*/
void print_dialog();

#endif // NO_IMLIB
