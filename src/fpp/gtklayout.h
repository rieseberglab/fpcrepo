/****************************************************
 * File: gtklayout.h
 *
 * Author: Fred Engler
 ****************************************************/

#ifndef INC_LAYOUT_H
#define INC_LAYOUT_H

#include "gtktrack.h"

struct layout_ctg_info *ctg_layout=NULL;   /* The structure for holding the ctg layout
                                            * information read from the fpp file*/
int max_layout= -1;                        /* Memory allocation size*/

/*Function prototypes*/
void write_fpp_file();
void free_ctg_layout_mem();
void check_ctg_layout_mem();
void load_layout();
void store_single_ctg_layout(int ctg);

#endif /* __LAYOUT_H__ */
