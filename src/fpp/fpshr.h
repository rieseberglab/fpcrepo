/********************************************************
                     fpshr.h
5dec97 MARKER_SZ 16
3sept98 disppacmarkers
30jul02 disprepmarkers
*********************************************************/
#define WITH_SEQTRACK 

#include "a.h"
#include "graph.h"
#include "acedb.h"
#include "sysclass.h"
#include "key.h"
#include <limits.h>
#include "types.h"

#define MaX(a,b) (a > b ? a : b)
#define MiN(a,b) (a < b ? a : b)
           /* if change, grep for 12s to find print statements with size 12 */
                       /* increase size on 3/8/1 from 12 to 16 */
#define CLONE_SZ 20    /* maximum length of 15 clone name +1 if cancelled */
#define MARKER_SZ 30    /* maximum length of marker name */
#define COMMENT_SZ 41  
#define NAME_SZ 25     /* maximum of marker, clone and contig name */
#define GEL_SZ 16     /* gel number now is alphanumeric*/
#define CTGMSG_SZ 100  /* contig message size */
#define MSGSIZE 100   /* project message size, shown in project window */
#define ANCHOR_BIN_SZ 20       /* maximum size of anchor bins*/
#define MAX_SEQ_PER_CLONE 100  /* max number of sequences allowed to hit a clone */
#define SEQ_SZ  25
#include "proj.h" /* uses MARKER_SZ */

#define PARENT 1
#define PSPARENT 2
#define EXACT 4
#define APPROX 8
#define PSEUDO 16

/* used for status field in marker (most are obsolete) && ctgstat */
#define MANUAL 1      /* default ctgstat */
#define AUTO 0        /* if cbmap was run & ok'ed */
#define UPDATED 4
#define NEW 8
#define USERUPDATE 10
#define AUTOUPDATE 11

/* this is not used */
#define USER 2
#define SAM 1

/* frame type */
#define FRAME 1
#define PLACE 2

/* for the hash table to check duplicated clone names */
#define HASH_SIZE 262143
struct hash_list_element {
    int i;
    struct hash_list_element* next;
};

/* structures*/

struct remark {
  char message[COMMENT_SZ+1];
  int box;
  int colour;
  struct remark *next;
};

struct markerctgpos { 
  int ctg;
  int pos,status;
  struct markerctgpos *next;
};

struct markerclone {
  int cloneindex;
  int weak;
  struct markerclone *nextclone;
};  

struct mytime {
  int day,month,year,hour,minute;
};

typedef struct marker {
  char marker[MARKER_SZ+1]; 	/* name of the marker */
  int count;		/* number of clones this marker is in */
  int type; 	/* in fpp/types.h (Locus, Probe, STS, etc.) */

  /*fred 5/28/03 -- new framework method*/
  char anchor_bin[ANCHOR_BIN_SZ+1];/* chromosome/linkage group of marker */
  float anchor_pos;     /* position on chr or lg*/
  int frame_type;       /* either FRAME or PLACE*/
  BOOL anchor;        

  struct markerctgpos *pos;
  int box;
  int minicontigbox;
  int colour;
  int status;
  int textbox;
  int textbox2;
  int cloneindex; /* index of first clone */
  int weak;
  struct markerclone *nextclone;        
  struct remark *remark;
  struct mytime creation_date;                    
  struct mytime modified_date;  
} MARKER ;


typedef enum    { 
                HT_BSS,         // BSS hit to BES
                HT_BES,         // embedded BES
                HT_MRK          // embedded MRK (not used yet)
                } seq_hit_type;

typedef struct seqhit {
  struct sequence* seq;
  int clone_idx;
  char rf;  /* BES  r or f */
  int seq_start, seq_end;
  seq_hit_type type;
  struct seqhit* next;
} SEQHIT;  

typedef enum    {
                ST_DRAFT,   // draft contig or supercontig
                ST_CLONE    // sequenced clone (not used yet)
                } seq_type;



typedef struct sequence {
  char* name;           
  seq_type type; 	   
  int length;
  struct fpc_seq_list *ctgs;    /* seqctgs, if it is a supercontig */
  int pos;                      /* location in supercontig, if 
                                    it is a seqctg */
  struct sequence *parent;      /* parent superconting, if it is a seqctg */
  SEQHIT *hits;                 /* hits to clones */ 
  struct seqctgpos* ctgpos;    
  struct remark *remark;
} FPC_SEQ;

typedef struct fpc_seq_list {
    FPC_SEQ* seq;
    struct fpc_seq_list* next;
} FPC_SEQ_LIST;

struct seqctgpos { 
  int ctg;
  int pos;
  int ctg_start;
  int ctg_end;
  int seq_start;
  int seq_end;
  int seqctgs_hit;
  int clones_hit;
  int score;
  float corr;
  char LR;
  FPC_SEQ* pseq; // helpful in the analysis routines
  struct seqctgpos *next;
};

struct markertop {
  int markerindex; 
  int weak;
  int new; /* 0 - no, 1 - new, 2 - processed */
  char marker[MARKER_SZ+1];
  struct markertop *nextmarker;
} ;

struct markerlist2{
  int markerindex;
  char marker[MARKER_SZ+1];
  int ctg,midpt;
  struct markerlist2 *next;
}; 

struct markerlist {
  int markerindex;
  int midpt;
  int global;
  struct markerlist *next;
};

struct fpdata {
  char fpchar[CLONE_SZ+1];
  char gelname[GEL_SZ+1];
  int b1,b2;   /* bands are bands[b1-1] to bands[b1+b2-2] */
  struct fpdata *next;
};

typedef  struct clonedata {
  char clone[CLONE_SZ+1];       /* Clone name */
  struct fpdata *fp;    /* fp number as a char */ 
  char chctg[10];       /* FIX 18mar99 was [6] contig number as character */
  int ctg, oldctg;             /* contig number */
  int ibc;             /* 16sept for ibc */
  int x,y;             /* position of clone */
  char match[CLONE_SZ+1];       /* set to "      " if not a  Match */
  int mattype;         /* see defines  */
  int parent;          /* index of parent */
  int next;            /* index of next clone in this contig */
  int highcol;         /* highlight colour IDL 27 Apr 95 */
  int selected;       /* is the clone selected */
  int class;
  int seqstat, seqtype; /* ADD 18mar99 seqtype */
  int size_bp;          /* WMN to store size for MTP efficiency */
  struct markertop *marker;
  struct remark *remark;
  struct remark *fp_remark;    
  SEQHIT* seq_hits;
  struct mytime creation_date;
  struct mytime modified_date;
  
  int matrix_location; /* used to quickly refer to the matrix row this clone is */
} CLONE ;

typedef struct contigdata {
  int ctg;          /* contig number */
  int start;        /* index of first clone in this contig */
  int last;         /* index of last clone in contig */
  int count;        /* number of clones in contig */
  int left, right;  /* coordinates */
  int markers; /* number of markers per ctg */
  int seq;     /* number of clones sequened in ctg */
  char projmsg[MSGSIZE];   
  char ctgIBC;
  short approxQs;   /* 1 if approx, 0 if exact */
  int ctgQs;
  int ctgstat;      
  int draft;
  int high_score, avg_score, low_score; /* cari 11aug04 */
      /* CAS Feb03 - added three fields for 6.4 */
  char chr_msg[CTGMSG_SZ];
  short noedit_chr;
  short noedit_pos;
  float chr_pos;              /* set in  ctg2chr calc */
  char user_msg[CTGMSG_SZ];
  char trace_msg[CTGMSG_SZ];
  struct mytime ctgdate;      
} CONTIGDATA;

typedef struct filter {
  BOOL done;
  int min,max;
  int type;
  char subtype;
  char exclude[5];
} FILTER;

#ifdef MAIN
int FPP;                 /* set to 1 in fpp.c, 0 in non-graphic programs */
BOOL  Build_useFlag;
Array acedata=0;
Array bands=0;
Array bands_second=0;
Array markerdata = 0;
Array acedatasecond=0;
Array filterdata = 0;
int tot_clones=0, tot_bands=0, singleton=0;
int bandmax=0, bandmin=0, max_contig=0,max_contig2=0, *fparray=0,max_fp=0;
CONTIGDATA *contigs=NULL;
char fileName[FIL_BUFFER_SIZE] ;
char dirName[DIR_BUFFER_SIZE] ;
char mergedir[DIR_BUFFER_SIZE],mergefile[FIL_BUFFER_SIZE];
char secondfileName[FIL_BUFFER_SIZE],newfileName[FIL_BUFFER_SIZE],dirName3[DIR_BUFFER_SIZE];
char newdirName[DIR_BUFFER_SIZE];
BOOL fpnum,projmerge = FALSE;
struct markerlist *markerlistroot;
struct markerlist2 *markerlistroot2;
int Nmarkers,fpstatus=0;
FILE *fpsecond=NULL;
#else
extern int FPP,*fparray;
extern Array acedata;
extern Array bands;
extern Array bands_second;
extern Array markerdata;
extern Array acedatasecond,markerdatasecond;
extern Array filterdata;
extern int tot_clones, tot_bands, singleton;
extern int bandmax, bandmin, max_contig, max_contig2, *fparray, max_fp;
extern CONTIGDATA *contigs;
extern char fileName[FIL_BUFFER_SIZE] ;
extern char dirName[DIR_BUFFER_SIZE] ;
extern char mergedir[DIR_BUFFER_SIZE],mergefile[FIL_BUFFER_SIZE];
extern char secondfileName[FIL_BUFFER_SIZE],newfileName[FIL_BUFFER_SIZE],dirName3[DIR_BUFFER_SIZE];
extern char newdirName[DIR_BUFFER_SIZE];

extern BOOL fpnum,projmerge;
extern struct markerlist *markerlistroot;
extern struct markerlist2 *markerlistroot2;
extern int Nmarkers,fpstatus;
extern int currentctg;
extern FILE *fpsecond;
#endif


