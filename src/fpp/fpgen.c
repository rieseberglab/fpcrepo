/**********************************************************
                   fpgen.c
Functions for highlighting
contains highlightmarker, and clearall

written by IL
rewritten by CAS
edited by FE
**********************************************************/
#include "fpp.h"
#include <gtk/gtkwidget.h>

extern int centreClone;
extern int BPYCOLOUR;
extern int MRKCOLOUR;
extern GtkWidget *ctg_window;
extern int mergeoffset, mergecontig2;  /*fred 5/10/04*/
extern void clear_highlights();
extern void refresh_all_track_colors();

void trailZap(), gelhighlight();
void redrawfingerprints();
void highlightnodraw(BOOL highlight);
void highlightmarkernodraw(BOOL highlight);
void addclonetomarker(int cloneindex);
void displaymarker();
int getcolourbox(CLONE *clone);
static void dohighlightmarker();
static void dohighlightparent();
void highlightfriends();
void appendCloneToMarkerList(int cloneindex);  

int FpGelCBpicked=0;
int markerTrail=1, markerTrailColCnt=0, markerTrailCol=PURPLE;
/**********************************************************
                   cloneHigh
************************************************************/
int cloneHigh(struct contig *p, int pop)
{
CLONE *clp;

    if (p==NULL) return 0;
    if (!displaymap) {
       ctgdisplay(currentctg);
       return 0;
    }
    clp = arrp(acedata,p->next,CLONE);
    highlightmarkernodraw(FALSE);
    highlightnodraw(FALSE);
    centreClone = p->next;
    if(merging && clp->ctg==mergecontig2)   /*fred 5/10/04*/
      centre_pos = clp->x + mergeoffset;
    else
      centre_pos = clp->x;
    if(ctg_window!=NULL){
      if(!trail) clear_highlights();
      refresh_all_track_colors();
    }
    if (pop) graphPop(); 
return 1;
}

/************************************************************
                   clearall
************************************************************/
void clearall()
{
  struct markerlist *mp;
  struct marker *marker;
  struct contig *p;
  struct remark *rem;
  CLONE *clone;
 
  if(ctg_window==NULL)
    return;

  for (p=root; p!=NULL; p = p->new)
  {
    clone = arrp(acedata,p->next,CLONE);
    if(clone->selected==TRUE){
      clone->selected = FALSE;
      clone->highcol = WHITE;
    }
    else if(clone->highcol!=0){
      clone->highcol = 0;
    }
    rem =clone->remark;
    while(rem!=NULL){
      if (rem->colour != 0) {
         if (rem->box != 0)
            graphBoxDraw(rem->box,BLACK, WHITE);
         rem->colour = 0;
      }
      rem = rem->next;
    }
    rem =clone->fp_remark;
    while(rem!=NULL){
      if (rem->colour != 0) {
         if (rem->box != 0)
            graphBoxDraw(rem->box,BLACK, WHITE);
         rem->colour = 0;
      }
      rem = rem->next;
    }
  }
  mp = markerlistroot;
  while(mp!=NULL){
    marker = arrp(markerdata,mp->markerindex,MARKER);
    if (marker->colour != 0 && marker->colour!=PCRCOLOUR && 
       marker->colour!=BPYCOLOUR && marker->colour!=MRKCOLOUR) {
       if (marker->type!=markPCR || marker->type!=markBAC || 
	   marker->type!=markPAC || marker->type!=markYAC ||
	   marker->type!=markeBAC || marker->type!=markeMRK) 
		   marker->colour = 0;
       else  if (marker->type==markPCR) marker->colour = PCRCOLOUR;
       else  if (marker->type==markeMRK) marker->colour = MRKCOLOUR;
       else  marker->colour = BPYCOLOUR;
       if (marker->box != 0)
	  graphBoxDraw(marker->box,BLACK, marker->colour);
    }
    mp = mp->next;
  }
  clhigh = NULL;
  highmark = -1; 
  highremark = -1; 
  highseq = 0;
  fphigh = gelhigh = -1;

  trailZap();
  redrawfingerprints();
  gelhighlight();
  refresh_all_track_colors();   
}

void highlightnodraw(BOOL highlight) {
  return;
}

/***************************************************************
                  highlightfriends
***************************************************************/
void highlightfriends(BOOL high)
{
  CLONE *clone;
  struct contig *p = NULL;
  BOOL last;
  int col;

  if(clhigh ==NULL) return;
  clone = arrp(acedata,clhigh->next,CLONE);
  if(clone->mattype == 0) return;

  if(clone->mattype & PARENT || clone->mattype & PSPARENT  ){ /* i.e. parent */  
      for (p=root; p!=NULL; p = p->new)
      {
	clone = arrp(acedata,p->next, CLONE);
	if(clone->parent == clhigh->next){
	  if(!high){
	    col = clone->highcol = WHITE;
	    if(clone->selected) col = SELECTED;
	  } 
	  else col = clone->highcol = FRIEND;
	}
      } 
   }
   else{ /* its buried */
      for (last=FALSE, p=root; p!=NULL && !last; p = p->new)
      {
	 if(p->next == clone->parent){
	   clone = arrp(acedata,clone->parent,CLONE);
	   if(!high){
	      col = clone->highcol = WHITE;
	      if(clone->selected) col = SELECTED;
	    } 
	    else col = clone->highcol = FRIEND;
            last=TRUE;
	  }
       }
   }
}
/************************************************************
                   highlightparent
***********************************************************/
void highlightparent(int parent,BOOL high, int col)
{
     dohighlightparent(parent,high,TRUE, col);
}
void highlightparentnodraw(int parent,BOOL high, int col)
{
     dohighlightparent(parent,high,FALSE, col);
}
void dohighlightparent(int parent,BOOL high, BOOL draw, int col)
{
  int second;
  BOOL last,found;
  struct contig *temp;
  struct markerclone *newptr;
  MARKER *markerptr;

  if (high && markerTrail) second = col;
  else if(high) second = GREEN;
  else  if (arrp(acedata,parent,CLONE)->selected) second= SELECTED;
  else second= WHITE;
      
  markerptr = arrp(markerdata,highmark,MARKER);
  if(markerptr->cloneindex ==parent)
    return;
  newptr = markerptr->nextclone;
  found = FALSE;
  while(newptr!=NULL){
    if(newptr->cloneindex == parent)
      return;
    newptr=newptr->nextclone ;
  }  
  if(!found){
    if (!arrp(acedata,parent,CLONE)->selected)
           arrp(acedata,parent,CLONE)->highcol= second;
    temp=root;
    last = FALSE;
    while (!last){
      if(temp->next==parent && draw){
	if(temp->box !=0)
	  graphBoxDraw(temp->box, BLACK, second);
	last = TRUE;
      }
      if(temp->new!=NULL)
	temp = temp->new;
      else
	last = TRUE;
    } 
  }
}

/************************************************************
                   highlightmarker
***********************************************************/
void highlightmarker(BOOL high)
{
    dohighlightmarker(high, TRUE);
}
void highlightmarkernodraw(BOOL high)
{
    dohighlightmarker(high, 0);
}

static void dohighlightmarker(BOOL high, BOOL draw)
{
  int clcol,highcol,clcolweak,col,remcolour;   
  struct remark *remptr;   
  MARKER *markerptr;
  BOOL last;
  struct contig *temp;
  struct markerclone *newptr;
  int numcol=3;
  int colours[8] = {PALEBLUE, PALEMAGENTA, LIGHTGREEN}; 

  return;   /*FIXME fred 5/8/03*/
  if(highmark == -1) return;
  if(ctg_window==NULL)
    return;

  if(high){
    if (markerTrail) {
       col = clcolweak=clcol = highcol = colours[markerTrailColCnt];
       markerTrailColCnt++;
       if (markerTrailColCnt==numcol) markerTrailColCnt=0;
     }
     else {
       highcol = HIGHLIGHT;
       clcolweak = LIGHTRED;
       remcolour = LIGHTRED;   /*fred 11/6/02*/
       clcol = FRIEND;
       col = clcol;
     }
  }
  else{
    highcol = clcol = col = remcolour = WHITE;   /*fred 11/6/02 added remcolour*/
  }

  markerptr = arrp(markerdata,highmark,MARKER);
  markerptr->colour = highcol;
  if (markerptr->type==markPCR && highcol==WHITE) highcol = PCRCOLOUR; 
  if (markerptr->type==markeMRK && highcol==WHITE) highcol = MRKCOLOUR; 
  if ((markerptr->type==markBAC || markerptr->type==markPAC ||
      markerptr->type==markYAC || markerptr->type==markeBAC) && highcol==WHITE) highcol = BPYCOLOUR; 
  if(markerptr->box != 0 && draw==TRUE)
    graphBoxDraw(markerptr->box,BLACK,highcol); 

       /* process first clone of marker */
  for (temp=root; temp!=NULL; temp = temp->new) {
    if(temp->next == markerptr->cloneindex){		
       if(high){ /* 30sept98 */
          if (markerTrail && arrp(acedata,temp->next,CLONE)->highcol)
                              col = markerTrailCol;
          else if(markerptr->weak) col = clcolweak;
          else col = clcol;
       }
       else if(arrp(acedata,temp->next,CLONE)->selected && !high)
          col = SELECTED;
       else col = WHITE;

       if (col!=SELECTED) arrp(acedata,temp->next,CLONE)->highcol = col;
       if(temp->box!=0 && draw)
	 graphBoxDraw(temp->box, BLACK, col);
       else if(arrp(acedata,temp->next,CLONE)->parent != -1) {
	 if (draw) 
           highlightparent(arrp(acedata,temp->next,CLONE)->parent,high,col);
	   else highlightparentnodraw(arrp(acedata,temp->next,CLONE)->parent,
                             high,col);
       }
    }
  }
	   
       /* process rest of clones of marker */
  newptr = markerptr->nextclone;	    
  while(newptr!=NULL){
    for (temp=root; temp!=NULL; temp = temp->new) {
      if(temp->next==newptr->cloneindex){
	if(high){
          if (markerTrail && 
              arrp(acedata,temp->next,CLONE)->highcol &&
              arrp(acedata,temp->next,CLONE)->highcol!=clcol)
                           col = markerTrailCol;
	  else if(newptr->weak) col = clcolweak;
	  else col = clcol;
	}
	else if(arrp(acedata,temp->next,CLONE)->selected && !high)
	  col = SELECTED;
	else col = WHITE; /* 30sept98*/

	if (col!=SELECTED) arrp(acedata,temp->next,CLONE)->highcol = col;
	if(temp->box !=0 && draw)
	  graphBoxDraw(temp->box, BLACK, col);
	else if(arrp(acedata,temp->next,CLONE)->parent != -1) {
	  if (draw) 
            highlightparent(arrp(acedata,temp->next,CLONE)->parent,high,col);
	  else highlightparentnodraw(arrp(acedata,temp->next,CLONE)->parent,
                   high,col);
        }
	last = TRUE;
      }
    } 
    newptr=newptr->nextclone ;
  }  

  /*fred 11/6/02 -- add marker remark*/
  remptr = markerptr->remark;
  while(remptr!=NULL){
     remptr->colour = remcolour;
     remptr = remptr->next;
  }
  if(!high) highmark =-1;
  else currentclone = -1;
}
