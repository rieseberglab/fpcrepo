/************************************************************
                        DEF: contigs
written by IL
edited by CAS
30jul02 disprepmarkers (fred)
***********************************************************/
#include "fpp.h"

int gCtgRmk;
void ZCtgChrUpdate();
void initAnyContig(int i, struct contigdata contigArr []);
extern void show_help();

/************************************************************
                 DEF: initContig
*************************************************************/
void initContig(int i)
{
	initAnyContig(i, contigs);
}



/************************************************************
                 DEF: initContig
*************************************************************/
void initAnyContig(int i, struct contigdata contigArr [])
{
      contigArr[i].count = 0;
      contigArr[i].markers = contigArr[i].seq = 0;
      contigArr[i].last = contigArr[i].start = -1;
      contigArr[i].left  = contigArr[i].right = contigArr[i].ctg = contigArr[i].draft = 0;
      contigArr[i].chr_pos = 0.0;
      contigArr[i].projmsg[0] = '\0';
      contigArr[i].ctgQs = -1;
      contigArr[i].approxQs = -1;
      contigArr[i].ctgstat = ND;
      contigArr[i].noedit_chr=0;
      contigArr[i].noedit_pos=0;
      contigArr[i].user_msg[0]='\0';
      contigArr[i].chr_msg[0]='\0';
      contigArr[i].trace_msg[0]='\0';
      contigArr[i].ctgIBC = ' ';
      contigArr[i].ctgdate.day = contigArr[i].ctgdate.month = contigArr[i].ctgdate.year = 0;
      contigArr[i].ctgdate.hour = contigArr[i].ctgdate.minute = 0;
      contigArr[i].high_score = contigArr[i].avg_score = contigArr[i].low_score = 0;
}

/**************************************************************
                 DEF: contigsAlloc
**************************************************************/

void contigsAlloc(ctg)
int ctg;
{
  int i;
  void *malloc(), *realloc();

  max_contig = MaX(ctg, max_contig);
  if (contigs == NULL) {
    i = 0;
    if(alloc_contig < ctg )
      alloc_contig = ctg + 10;
    contigs = (struct contigdata *)
          malloc((sizeof(struct contigdata) * alloc_contig));
    if (contigs == NULL) messcrash("Cannot alloc memory for contigs.");
  }
  else if (ctg < alloc_contig) return;
  else {
    i = alloc_contig;
    alloc_contig = MaX(alloc_contig+100, ctg+10);
    contigs = (struct contigdata *)
        realloc(contigs, (sizeof(struct contigdata) * alloc_contig));
    if (contigs == NULL) messcrash("Cannot realloc memory for contigs.");
  }
  for (; i< alloc_contig; i++) initContig(i);
}


/*****************************************************8
                DEF: copyContigs
Makes a copy of a contigs array
*******************************************************/
struct contigdata * copyContigs(struct contigdata * origContigs)
{
	unsigned int totalCopyElements = max_contig + 10;
	unsigned int copyBytes = totalCopyElements * sizeof(struct contigdata);
	struct contigdata * contigsCpy = (struct contigdata *) malloc(copyBytes);
    if (contigsCpy == NULL)
	{
		messcrash("Cannot alloc memory for contigsCpy.");
	}
	int i = -1;
	for (i = 0; i < totalCopyElements; i++)
	{
		initAnyContig(i, contigsCpy);
	}
	// max_contig is the actual 0-based index of the last array element
	//	with non-zero clone count
	memcpy(contigsCpy, origContigs, (max_contig + 1) * sizeof(struct contigdata));
	return contigsCpy;
}

/*****************************************************8
                DEF: getcontigsmax
*******************************************************/
int getcontigsmax()
{
int i,k=0;

 for(i=1;i<=max_contig;i++){
   if(contigs[i].count != 0)
     k=i;
 }
 return(k);
}
/***************************************************************
                    DEF: contigRemark
CAS 2-2-3: add to edit contig remarks. Called from contig display
****************************************************************/
void contigRemark(), updateproj();
static int editctg;
static int chrbox, posbox, userbox;
static char chrStr[15], posStr[15];
int s1Arc, s2Arc, s3Arc, s4Arc, s5Arc, s6Arc;

void quitcontigRemark()
{
   if (graphActivate(gCtgRmk)) graphDestroy();
}
void CurrentCtgRemark() {
   quitcontigRemark();
   if (currentctg > 0 && currentctg <= max_contig) {
       editctg = currentctg; contigRemark();
   }
}
void thisCtgRemark(int i) {
   quitcontigRemark();
   editctg = i;
   contigRemark();
}

static void hitCR()
{
   if (editctg==currentctg) ctgdisplay(currentctg);
   updateproj();
   update = TRUE;
}
static void scanText()
{
  if(graphActivate(gCtgRmk)){
    if (chrStr[0]!='\0') {
       contigs[editctg].noedit_chr=1;
       if (strcasecmp(chrStr,"none")==0)
           sprintf(contigs[editctg].chr_msg,"None");
       else
           sprintf(contigs[editctg].chr_msg,"%s%s",Proj.label_abbrev,chrStr);
       ZCtgChrUpdate(editctg);
       update=TRUE; contigRemark(); updateproj();
       if (editctg==currentctg) ctgdisplay(currentctg);

    }
    if (posStr[0]!='\0') {
       contigs[editctg].noedit_pos=1;
       sscanf(posStr,"%f", &(contigs[editctg].chr_pos));
       update=TRUE; contigRemark(); updateproj();
    }
  }
}


static void pickg(int box,double x,double y)
{
    if (box == s1Arc) {
        contigs[editctg].ctgstat=ND;
        update=TRUE; contigRemark(); updateproj();
    }
    else if (box == s2Arc) {
        contigs[editctg].ctgstat=NOCB;
        update=TRUE; contigRemark(); updateproj();
    }
    else if (box == s3Arc) {
        contigs[editctg].ctgstat=AVOID;
        update=TRUE; contigRemark(); updateproj();
    }
    else if (box == s4Arc) {
        contigs[editctg].ctgstat=DEAD;
        update=TRUE; contigRemark(); updateproj();
    }
    else if (box == s5Arc) {
        contigs[editctg].noedit_chr= (!contigs[editctg].noedit_chr);
        if (contigs[editctg].noedit_chr==0) chrStr[0]='\0';
        else if (chrStr[0]=='\0') {
           strcpy(chrStr,"None");
           strcpy(contigs[editctg].chr_msg,"None");
        }
        update=TRUE; contigRemark(); updateproj();
    }
    else if (box == s6Arc) {
        contigs[editctg].noedit_pos= (!contigs[editctg].noedit_pos);
        if (contigs[editctg].noedit_pos==0) posStr[0]='\0';
        update=TRUE; contigRemark(); updateproj();
    }
}

/*********************************************************/
static void zhelp()
{
show_help("Edit Contig Remarks & Status","editctg");
}
/**********************************************************/
void contigRemark()
{
float row, col=3, width=50.0;
char msg[300];
static MENUOPT amenu[] = { {quitcontigRemark, "Close"},
    { 0, 0 } };

  if(graphActivate(gCtgRmk)){
    graphClear();
    graphPop();
  }
  else {
     gCtgRmk =
       graphCreate (TEXT_FIT,"Edit Contig Remarks & Status",.2,.2,.76,.45);
     graphRegister (PICK, pickg) ;
     chrStr[0]='\0';
     posStr[0]='\0';
  }
  graphMenu(amenu);

  row=1.0;
  if (contigs[editctg].ctgQs == -1)
    sprintf(msg,"Edit contig %d  (Qs unknown)",editctg);
  else
    sprintf(msg,"Edit contig %d  (Qs %d)",editctg, contigs[editctg].ctgQs);
  graphText(msg,2.0,row);

  row+=2.0;
  graphText("Status:",2.0,row);
  row+=2.0;
  s1Arc = graphBoxStart();
      graphArc(col, row+0.5, 0.7, 0, 360);
      if (contigs[editctg].ctgstat==ND) graphFillArc(col, row+0.5, 0.4, 0, 360);
      graphBoxEnd();
      graphBoxDraw(s1Arc, BLACK, TRANSPARENT);
  graphText("Ok (do everything)",4.0,row);
  row+=1.5;
  s2Arc = graphBoxStart();
      graphArc(col, row+0.5, 0.7, 0, 360);
      if (contigs[editctg].ctgstat==NOCB) graphFillArc(col, row+0.5, 0.4, 0, 360);
      graphBoxEnd();
      graphBoxDraw(s2Arc, BLACK, TRANSPARENT);
  graphText(
    "NoCB (For IBC, add clones, merge contigs, but do not reorder clones)",
     4.0,row);
  row+=1.5;
  s3Arc = graphBoxStart();
      graphArc(col, row+0.5, 0.7, 0, 360);
      if (contigs[editctg].ctgstat==AVOID) graphFillArc(col, row+0.5, 0.4, 0, 360);
      graphBoxEnd();
      graphBoxDraw(s2Arc, BLACK, TRANSPARENT);
  graphText("Avoid (Avoid on both Build and IBC)",4.0,row);
  row+=1.5;
  s4Arc = graphBoxStart();
      graphArc(col, row+0.5, 0.7, 0, 360);
      if (contigs[editctg].ctgstat==DEAD) graphFillArc(col, row+0.5, 0.4, 0, 360);
      graphBoxEnd();
      graphBoxDraw(s2Arc, BLACK, TRANSPARENT);
  graphText("Dead (No Summary,Builds,AceDump",4.0,row);
  row+=1.5;
  graphLine(0.0,row,width,row);
  row += 1.5;
  sprintf(msg,"%s Remark: %s",Proj.label_abbrev,contigs[editctg].chr_msg);
  graphText(msg,2.0,row);
  row += 1.5;
  sprintf(msg,"%s Position: %7.2f",Proj.label_abbrev,contigs[editctg].chr_pos);
  graphText(msg,2.0,row);
  row +=1.5;
  graphText("Chr: ",2.0,row);
  chrbox = graphTextEntry(chrStr,5,6.0,row,scanText);
  s5Arc = graphBoxStart();
      graphArc(17.0, row+0.5, 0.7, 7, 360);
      if (contigs[editctg].noedit_chr==1) graphFillArc(17.0, row+0.5, 0.4, 7, 360);
      graphBoxEnd();
      graphBoxDraw(s5Arc, BLACK, TRANSPARENT);
  graphText("No Auto update    (Type the word 'none' if no assignment)",19.0,row);
  row +=1.5;
  graphText("Pos: ",2.0,row);
  posbox = graphTextEntry(posStr,8,6.0,row,scanText);
  s6Arc = graphBoxStart();
      graphArc(17.0, row+0.5, 0.7, 0, 360);
      if (contigs[editctg].noedit_pos==1) graphFillArc(17.0, row+0.5, 0.4, 0, 360);
      graphBoxEnd();
      graphBoxDraw(s6Arc, BLACK, TRANSPARENT);
  graphText("No Auto update",19.0,row);
  row +=1.5;
  graphText("Hit <CR> after entering a value",2.0,row);
  row +=1.5;
  graphLine(0.0,row,width,row);

  row +=1.5;
  graphText("User Remark: ",2.0,row);
  userbox = graphTextEntry(contigs[editctg].user_msg,CTGMSG_SZ,16.0,row,hitCR);
  row +=1.5;
  graphText("Trace Remark: ",2.0,row);
  userbox = graphTextEntry(contigs[editctg].trace_msg,CTGMSG_SZ,16.0,row,hitCR);
  row +=1.5;
  graphText("Remark is changed as soon as you start typing.",2.0,row);
  row +=1.5;
  graphText("Trace Remark automatically updated on contig changes.",2.0,row);
  row += 2.0;
  graphButton("Close",quitcontigRemark,2.0,row);
  graphButton("Help",zhelp,10.0,row);
  graphRedraw();
}
