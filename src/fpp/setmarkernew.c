/*  Last edited: Jan 18 09:30 1996 (il) */
#include "fpp.h"
#include <fcntl.h>
#ifdef ALPHA
#include "sex.h"
#endif
#define SEEK_SET 0
void markersinctgcount(void);

void setmarkerposaftermerge()
{
  int l;
  struct markerclone *p;
  struct markerctgpos *pos;
  struct marker *marker;
  CLONE *clone;
  struct {
      int left, right, ctg;
  } pt[100], tmp;
  int stack[100], s;
  int k, depth=0, npt, i, j, ctg;
  int x, y, save_depth=0;
  BOOL proceedctg,found;


/* find all clones for marker */
  for(l=0;l<arrayMax(markerdata);l++)
  {
    marker = arrp(markerdata,l,MARKER);
    clone = arrp(acedata,marker->cloneindex,CLONE);
    p = marker->nextclone;
    npt=0;
    pt[npt].ctg = clone->ctg;
    pt[npt].left = clone->x;
    pt[npt].right = clone->y;
    npt++;
    while(p != NULL){
      clone = arrp(acedata,p->cloneindex,CLONE);
      if (npt == 100) {
	printf("Bloody hell - marker %s has more than 100 clones!\n",
	       marker->marker);
	exit(0);
      }
      pt[npt].ctg = clone->ctg;
      pt[npt].left = clone->x;
      pt[npt].right = clone->y;
      npt++;
      p = p->nextclone;
    }
/* sort by ctg and left end */
    for (i=0; i<npt-1; i++){ 
      for (j=i+1; j<npt; j++){ 
	if (pt[i].ctg > pt[j].ctg || 
	    (pt[i].ctg == pt[j].ctg && pt[i].left > pt[j].left)) {
	  tmp = pt[i];
	  pt[i] = pt[j];
	  pt[j] = tmp;
	}
      }
    }
/* for each ctg, position */ 
    ctg  = -1;
    proceedctg = FALSE;
    for (i=0; i<npt; i++) {     
      if (ctg != pt[i].ctg) {
	if ((depth > save_depth) && proceedctg) {
	  pos->pos = (x+y) >> 1;
	}
	pos= marker->pos;
	found = FALSE;
	proceedctg = TRUE;
	while(pos !=NULL && !found){
	  if(pos->ctg == pt[i].ctg)
	    found=TRUE;
	  if(!found)
	    pos = pos->next;
	}
	if(found){
	  if(pos->status == USER) 
	    proceedctg = FALSE;
	}
	else if(pt[i].ctg == 0)
	  proceedctg =FALSE;
	else{
	/*  printf("Marker %s with ctg %d not found\n",marker->marker,pt[i].ctg);*/
	  if(marker->pos == NULL){
	    marker->pos  = (struct markerctgpos *)
	      messalloc((sizeof(struct markerctgpos)));
	    pos = marker->pos;
	    pos->pos = -1;
	    pos->status = AUTO;
	    pos->next = NULL;
	  }
	  else{
	    pos= marker->pos;
	    while(pos->next !=NULL)
	      pos = pos->next;
	    pos->next = (struct markerctgpos *) 
	      messalloc((sizeof(struct markerctgpos)));
	    pos = pos->next;
	    pos->status = AUTO;
	    pos->next = NULL;
	  }
	}
	if(proceedctg){
	  x = pt[i].left;
	  y = pt[i].right;
	  stack[0] = y;
	  pos->pos = (x+y) >> 1;
	  ctg = pos->ctg = pt[i].ctg;
	  depth = save_depth = 1;
	}
      }
      else if (pt[i].left <= y && proceedctg) {
	if (depth == 100) { printf("impossible!\n"); exit(0);}
	stack[depth++] = pt[i].right;
	x = MaX(x, pt[i].left);
	y = MiN(y, pt[i].right);
      }
      else if (proceedctg){
	if (depth > save_depth) {
	  save_depth = depth;
	  pos->pos = x+y >> 1;
	}
        x = pt[i].left;
        y = pt[i].right;
        stack[depth++] = y;
        for (j=-1, k=s=0; s< depth; s++) 
	  {
	    if (stack[s] < x) {
              stack[s] = -1;
              if (j == -1) j = s;
           }
           else { 
              k++;
              y = MiN(y, stack[s]);
              if (stack[j]== -1) {
                 stack[j] = stack[s];
                 stack[s] = -1;
                 while (stack[j]!=-1) j++;
              } else j = s;
           }
        }
	depth=k;
      }
    }
    if (depth > save_depth && proceedctg) {
       pos->pos = x+y >> 1;
    }
  } /* end loop through markers */
  markersinctgcount();
}
