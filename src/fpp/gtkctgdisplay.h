/****************************************************
 * File: gtkctgdisplay.h
 *
 * Author: Fred Engler
 ****************************************************/

#ifndef _GTKCTGDISPLAY_H_
#define _GTKCTGDISPLAY_H_

#include "gtkfpcruler.h"
#include "gtkhfpcruler.h"
#include "gtktrack.h"

GtkWidget *tracks_vbox;             /* The vbox that holds the tracks in the contig display*/
GtkWidget *tracks[1000];            /* Array for holding all the track widgets*/
float display_zoom;                 /* Value of the current zoom.*/
char ctg_window_title[80];          /* Contig display window title.*/
float zoom_max;                /* WMN 9/19/06 max zoom value for gtk pixel limit */
float horiz_scale_factor;       /* WMN 9/20/06 to allow hicf/agarose scale diff */

GtkWidget *ctg_window=NULL;         /* The widget for the contig display window*/
GtkWidget *ctgrem_text_box;         /* Contig remarks text box*/
GtkWidget *clones_label;            /* Label for # of clones in contig stats box*/
GtkWidget *markers_label;           /* Label for # of markers in contig stats box*/
GtkWidget *sequenced_label;         /* Label for # of sequenced clones in contig stats box*/
GtkWidget *length_label;            /* Label for contig length in contig stats box*/
GtkWidget *ctg_rem_frame;           /* Frame around contig remarks giving ctg # and project name*/

/* Function prototypes*/
int handle_button_press(GtkWidget *widget, GdkEventButton *event, GtkTrack *track);
void reorder_tracks();
GtkWidget * add_track(GtkWidget *widget, GtkObject *adj);
void create_new_track(GtkWidget *widget, GtkObject *adj);
void close_ctg_window(GtkWidget *widget, gpointer data);
void remove_all_tracks();
void load_tracks(GtkObject *adj);
void update_zoom(GtkObject *widget, gpointer data);
void whole_callback(GtkWidget *widget, GtkObject *adj);
void update_fpcruler(GtkWidget *widget, GtkWidget *fpcruler);
gint update_region(GtkWidget *widget, GdkEventMotion *event, gpointer data);
int search_tracks(GtkWidget *widget, gpointer data);
void update_page_numbers();
void update_page_view(GtkWidget *widget, gpointer data);
void move_left_callback(GtkWidget *widget, gpointer data);
void move_right_callback(GtkWidget *widget, gpointer data);
void refresh_gtk_ctgdisplay();
int gtk_ctgdisplay(int ctg);

#endif
