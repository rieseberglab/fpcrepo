/*************************************************************
                    region.c
Called from Select Clones, Region
cari 20feb04 - select all children of parents, and do not
  select children if parents not selected
***********************************************************/ 
#include "fpp.h"
#include <gtk/gtkwidget.h>

static void addregion(void);

extern GtkWidget *ctg_window;
extern void refresh_all_track_colors();

static void addregion()
{
  struct contig *p, *q;
  CLONE *clone, *ischild;
  REGION tempted;
  int addbit,temp;

  if(region[1].scr < region[0].scr){ 
    tempted = region[0];
    region[0] = region[1];
    region[1] = tempted;
    temp = regtop;
    regtop = regbot;
    regbot = temp;
    temp = bar1;
    bar1 = bar2;
    bar2 = temp;
    bar_active = !bar_active;
  }
  printf("Select all parents and their children between %d and %d\n",
          region[0].band, region[1].band);         
  if(!merging){
    for(p=root; p->new != NULL; p=p->new){      /* go through each clone until all added */
      clone = arrp(acedata,p->next,CLONE);
      if((clone->y >= region[1].band && clone->y <= region[0].band) ||
	 (clone->x >= region[1].band && clone->x <= region[0].band) ||
	 (clone->x <= region[1].band && clone->y >= region[0].band) ){
            if (clone->match[0] != ' ') continue;
	    clone->selected = TRUE;
	    if(clone->mattype == PARENT || clone->mattype == PSPARENT) {
               for(q=root; q->new != NULL; q=q->new){  /* find children */
                    ischild = arrp(acedata,q->next,CLONE);
                    if (strcmp(ischild->match, clone->clone)==0) ischild->selected=1;
               }
            } 
	}
      }
   } 
  else{
    for(p=root; p->new != NULL; p=p->new){      /* go through each clone until all added */
      clone = arrp(acedata,p->next,CLONE);
      if(clone->ctg == mergecontig2)
	addbit = (contigs[mergecontig1].right-contigs[mergecontig2].left)+startpt;
      else
	addbit =0;
      if((clone->y+addbit >= region[1].band && clone->y+addbit <= region[0].band) ||
	 (clone->x+addbit >= region[1].band && clone->x+addbit <= region[0].band) ||
	 (clone->x+addbit <= region[1].band && clone->y+addbit >= region[0].band) ){
            if (clone->match[0] != ' ') continue;
	    clone->selected = TRUE;
	    if(clone->mattype == PARENT || clone->mattype == PSPARENT) {
               for(q=root; q->new != NULL; q=q->new){  /* find children */
                    ischild = arrp(acedata,q->next,CLONE);
                    if (strcmp(ischild->match, clone->clone)==0) ischild->selected=1;
               }
            } 
       }
    } 
  }
  refresh_all_track_colors();  
}

void fpRegion()
{
  regdef = 0;
  if(ctg_window != NULL)
    addregion();
  if(graphActivate(g3))
    drawfpdata(1);
}
